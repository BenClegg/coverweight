# CoverWeight

A prototype automated grading / coverage analysis tool, specifically for the use of investigating the impact of coverage on weighted tests in test suites.

This includes a reference implementation of line recoverage, a measure of how many times individual lines are covered. This implementation is available in [HitCounts.java](https://bitbucket.org/BenClegg/coverweight/src/master/src/main/java/coverweight/report/HitCounts.java). Further processing can be performed on this metric, such as determining the mean recoverage per test.

## Disclaimer

This is prototype research software; I do not recommend using it in production as is.
