package coverweight.runner.jacoco;

import coverweight.runner.TestRunner;
import coverweight.sourcefiles.ClassUnderTest;
import coverweight.sourcefiles.UnitTest;
import org.jacoco.core.analysis.*;
import org.jacoco.core.tools.ExecFileLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;

public class JacocoAnalyser
{
    // Based on https://github.com/CodeDefenders/CodeDefenders/blob/master/src/main/java/org/codedefenders/execution/LineCoverageGenerator.java

    private static final Logger logger = LoggerFactory.getLogger(JacocoAnalyser.class);


    private CoverageBuilder coverageBuilder = new CoverageBuilder();
    private Collection<ExecFileLoader> execFileLoaders = new ArrayList<>();
    private ClassUnderTest cut;


    /**
     * Merge then analyse JaCoCo executions for every specified test on a CUT
     * @param tests
     * @param classUnderTest
     */
    public JacocoAnalyser(Collection<UnitTest> tests, ClassUnderTest classUnderTest)
    {
        cut = classUnderTest;

        for (UnitTest test : tests)
            createExecFileLoader(test, cut);

        run();
    }

    /**
     * Analysis of the JaCoCo execution output of a single test / CUT pair
     * @param test
     * @param classUnderTest
     */
    public JacocoAnalyser(UnitTest test, ClassUnderTest classUnderTest)
    {
        cut = classUnderTest;

        createExecFileLoader(test, cut);

        run();
    }

    private void createExecFileLoader(UnitTest test, ClassUnderTest classUnderTest)
    {
        Path jacocoExec = test.getJacocoExecPath(classUnderTest);
        ExecFileLoader execFileLoader = new ExecFileLoader();
        try
        {
            execFileLoader.load(jacocoExec.toFile());
        } catch (IOException e)
        {
            logger.error(e.getMessage());
        }
        execFileLoaders.add(execFileLoader);
    }

    private void run()
    {
        if(execFileLoaders.isEmpty())
            logger.error("No execFileLoader is defined; specified jacocoExec(s) for CUT may not exist.");

        for(ExecFileLoader execFileLoader : execFileLoaders)
        {
            Analyzer analyzer = new Analyzer(execFileLoader.getExecutionDataStore(), coverageBuilder);
            //final String regex = cut.getBaseName() + ".class";
            //final Pattern innerClassPattern = Pattern.compile(regex);

            File classFile = cut.getClassFilePath().toFile();
            try
            {
                analyzer.analyzeClass(new FileInputStream(classFile), cut.getClassDir().toString());
            }
            catch (IOException e)
            {
                logger.error("Failed to analyze file: " + classFile, e);
            }
        }
    }

    public CoverageResult getCoverageResult()
    {
        CoverageResult coverageResult = new CoverageResult();
        for (IClassCoverage cc : coverageBuilder.getClasses())
        {
            // TODO should probably check classpath / package name here too
            if (cc.getName().endsWith(cut.getBaseName()))
            {
                coverageResult = new CoverageResult(cc);
            }
        }

        return coverageResult;
    }
}
