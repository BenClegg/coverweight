package coverweight.runner.jacoco;

import org.jacoco.core.analysis.IClassCoverage;
import org.jacoco.core.analysis.ICounter;
import org.jacoco.core.analysis.ILine;

import java.util.ArrayList;
import java.util.List;

public class CoverageResult
{
    private List<Integer> coveredLines = new ArrayList<>();
    private List<Integer> uncoveredLines = new ArrayList<>();

    private IClassCoverage classCoverage;

    public CoverageResult()
    {

    }

    public CoverageResult(IClassCoverage inputClassCoverage)
    {
        this.classCoverage = inputClassCoverage;

        // Covered / Uncovered Lines
        for (int i = classCoverage.getFirstLine(); i <= classCoverage.getLastLine(); i++)
        {
            final ILine line = classCoverage.getLine(i);
            final int status = line.getInstructionCounter().getStatus();
            // TODO change behaviour of partial coverage ?
            if (status == ICounter.FULLY_COVERED || status == ICounter.PARTLY_COVERED)
                coveredLines.add(i);
            else if (status == ICounter.NOT_COVERED)
                uncoveredLines.add(i);
        }
    }

    public List<Integer> getCoveredLines()
    {
        return coveredLines;
    }

    public List<Integer> getUncoveredLines()
    {
        return uncoveredLines;
    }

    public double getCoveredLineRatio()
    {
        if(classCoverage == null)
            return Double.NaN;
        return classCoverage.getLineCounter().getCoveredRatio();
    }

    public double getCoveredMethodRatio()
    {
        if(classCoverage == null)
            return Double.NaN;
        return classCoverage.getMethodCounter().getCoveredRatio();
    }

    public double getCoveredInstructionRatio()
    {
        if(classCoverage == null)
            return Double.NaN;
        return classCoverage.getInstructionCounter().getCoveredRatio();
    }

    public double getCoveredBranchRatio()
    {
        if(classCoverage == null)
            return Double.NaN;
        return classCoverage.getBranchCounter().getCoveredRatio();
    }
}
