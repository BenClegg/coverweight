package coverweight.runner;

import coverweight.mistakesim.checkresults.GradeerSolution;
import coverweight.runner.jacoco.CoverageResult;
import coverweight.sourcefiles.ClassUnderTest;
import coverweight.sourcefiles.UnitTest;

import java.util.Arrays;
import java.util.Collection;

public class TestResult
{
    private boolean passes;

    private UnitTestCUTPair unitTestCUTPair;
    private CoverageResult coverage;

    public TestResult(AntProcessResult antProcessResult,
                      CoverageResult coverageResult,
                      ClassUnderTest classUnderTest,
                      UnitTest unitTest)
    {
        coverage = coverageResult;
        passes = readTestPassStatus(antProcessResult);

        unitTestCUTPair = new UnitTestCUTPair(unitTest, classUnderTest);
    }

    public TestResult(ClassUnderTest psuedoCUT, UnitTest unitTest, boolean faultDetected)
    {
        this.unitTestCUTPair = new UnitTestCUTPair(unitTest, psuedoCUT);
        this.passes = !faultDetected;
        this.coverage = null;
    }

    private boolean readTestPassStatus(AntProcessResult antProcessResult)
    {
        String resultMessage = antProcessResult.getJUnitMessage();
        //String[] tokens = resultMessage.split(" ");

        /*
        if(getIntFromTokens(tokens, 7) > 0)
        {
            if((getIntFromTokens(tokens, 9) == 0) && (getIntFromTokens(tokens, 11) == 0))
                return true;
        }
        */
        // TODO properly handle errors
        if ((resultMessage.contains("Failures: 0")) && (resultMessage.contains("Errors: 0")))
            return true;

        return false;
        /*
        String[] split = resultMessage.split("Tests run: ");
        if (Integer.parseInt(split[1].substring(0,1)) > 0)
        {
            return true;
        }
        return false;
        */
    }

    private int getIntFromTokens(String[] tokens, int index)
    {
        String s = tokens[index].replaceAll(",","");
        return Integer.parseInt(s);
    }

    @Override
    public String toString()
    {
        return "TestResult{" +
                "passes=" + passes +
                ", coverageResult=" + coverage +
                ", unitTestCUTPair=" + unitTestCUTPair +
                '}';
    }

    public boolean isPasses()
    {
        return passes;
    }

    public CoverageResult getCoverage()
    {
        return coverage;
    }

    public ClassUnderTest getClassUnderTest()
    {
        return unitTestCUTPair.getClassUnderTest();
    }

    public UnitTest getTest()
    {
        return unitTestCUTPair.getUnitTest();
    }

    public UnitTestCUTPair getUnitTestCUTPair()
    {
        return unitTestCUTPair;
    }
}
