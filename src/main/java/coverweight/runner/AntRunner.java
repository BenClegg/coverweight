package coverweight.runner;

import coverweight.configuration.Global;
import coverweight.sourcefiles.ClassUnderTest;
import coverweight.sourcefiles.UnitTest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Based on https://github.com/CodeDefenders/CodeDefenders/blob/master/src/main/java/org/codedefenders/execution/AntRunner.java
 */
public class AntRunner
{
    private static final Logger logger = LoggerFactory.getLogger(AntRunner.class);


    public AntRunner()
    {
    }

    public AntProcessResult runTest(ClassUnderTest cut, UnitTest test)
    {
        //logger.info("Running test " + test.getJavaFile() + " on " + cut.getJavaFile());
        return runAntTarget("run-test", cut, test);
    }

    public AntProcessResult compileCut(ClassUnderTest cut)
    {
        return runAntTarget("compile-cut", cut, null);
    }

    public AntProcessResult compileTest(ClassUnderTest cut, UnitTest test)
    {
        return runAntTarget("compile-test", cut, test);
    }


    public AntProcessResult runAntTarget(String target, ClassUnderTest cut, UnitTest test)
    {
        ProcessBuilder pb = new ProcessBuilder();
        List<String> command = new ArrayList<>();

        // Classpath
        String localClasspath = "lib/hamcrest-all-1.3.jar"+File.pathSeparator+"lib/junit-4.12.jar";
        pb.environment().put("CLASSPATH", localClasspath);

        // Core configuration
        command.add(Global.ANT_HOME + "/bin/ant");
        command.add(target);
        command.add("-Dhome.dir=" + Global.HOME_DIR);
        command.add("-Dmvn.dir=" + Global.MVN_REPOSITORY_DIR);
        //command.add("-Dcut.dir=" + cut.getJavaFile().getParent().toString());
        if(cut != null)
        {
            command.add("-Dcut.dir=" + cut.getClassDir());
            //System.out.println(cut.getClassDir());
            command.add("-Dcut.file=" + cut.getJavaFile().getParent());
            if(test != null)
                command.add("-Djacoco.exec=" + test.getJacocoExecPath(cut));
        }
        if(test != null)
        {
            command.add("-DtestClassname=" + test.getFullyQualifiedClassName());
            command.add("-Dtest.dir=" + test.getClassDir());
            command.add("-Dtest.file=" + test.getJavaFile().getParent());
        }

        pb.command(command);
        pb.directory(new File(Global.HOME_DIR));
        pb.redirectErrorStream(true);

        // Run
        //logger.info("Executing Ant Command {} from directory {}", pb.command().toString(), Global.HOME_DIR);
        return runAntProcess(pb);
    }

    private AntProcessResult runAntProcess(ProcessBuilder pb)
    {
        AntProcessResult res = new AntProcessResult();
        try {
            Process p = pb.start();


            BufferedReader is = new BufferedReader(new InputStreamReader(p.getInputStream()));
            res.setInputStream(is);

            String line;

            BufferedReader es = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            StringBuilder esLog = new StringBuilder();
            while ((line = es.readLine()) != null) {
                esLog.append(line).append(System.lineSeparator());
            }
            res.setErrorStreamText(esLog.toString());

        } catch (Exception ex) {
            res.setExceptionText(String.format("Exception: %s%s", ex.toString(), System.lineSeparator()));
        }
        return res;
    }

}
