package coverweight.runner;

import coverweight.runner.jacoco.CoverageResult;
import coverweight.runner.jacoco.JacocoAnalyser;
import coverweight.sourcefiles.ClassUnderTest;
import coverweight.sourcefiles.UnitTest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestRunner implements Runnable
{

    /*
        NOTE stick to individual test files having individual tests;
                can always split suites later, since not required for experiment
    */

    // See https://www.jacoco.org/jacoco/trunk/doc/examples/java/CoreTutorial.java for example

    private static final Logger logger = LoggerFactory.getLogger(TestRunner.class);

    private ClassUnderTest cut;
    private UnitTest test;

    private TestResult testResult;

    public TestRunner(ClassUnderTest classUnderTest, UnitTest unitTest)
    {
        cut = classUnderTest;
        test = unitTest;
    }

    public void compileTest(boolean skipIfTestClassExists)
    {
        if(skipIfTestClassExists && test.isCompiled())
            return;
        // TODO skip compilation if compiled?
        // Run ant
        AntRunner antRunner = new AntRunner();
        AntProcessResult antResult = antRunner.compileTest(cut, test);
        //System.out.println(antResult);
    }

    public void run()
    {
        // Run ant
        AntRunner antRunner = new AntRunner();
        AntProcessResult antResult = antRunner.runTest(cut, test);
        //System.out.println(antResult);
        //System.err.println(antResult.getErrorMessage());

        JacocoAnalyser jacocoAnalyser = new JacocoAnalyser(test, cut);
        CoverageResult coverageResult = jacocoAnalyser.getCoverageResult();

        testResult = new TestResult(antResult, coverageResult, cut, test);
    }

    public TestResult getTestResult()
    {
        return testResult;
    }
}
