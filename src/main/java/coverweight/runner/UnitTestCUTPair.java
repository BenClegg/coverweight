package coverweight.runner;

import coverweight.sourcefiles.ClassUnderTest;
import coverweight.sourcefiles.UnitTest;

import java.util.Objects;

public class UnitTestCUTPair
{
    UnitTest unitTest;
    ClassUnderTest classUnderTest;

    public UnitTestCUTPair(UnitTest test, ClassUnderTest cut)
    {
        unitTest = test;
        classUnderTest = cut;
    }

    public UnitTest getUnitTest()
    {
        return unitTest;
    }

    public ClassUnderTest getClassUnderTest()
    {
        return classUnderTest;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UnitTestCUTPair that = (UnitTestCUTPair) o;
        return getUnitTest().equals(that.getUnitTest()) &&
                getClassUnderTest().equals(that.getClassUnderTest());
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(getUnitTest(), getClassUnderTest());
    }

    @Override
    public String toString()
    {
        return "UnitTestCUTPair{" +
                "unitTest=" + unitTest +
                ", classUnderTest=" + classUnderTest +
                '}';
    }
}
