package coverweight.runner;

import coverweight.configuration.Global;
import coverweight.sourcefiles.ClassUnderTest;
import coverweight.sourcefiles.UnitTest;
import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class TestBatch implements Runnable
{
    Logger logger = LoggerFactory.getLogger(this.getClass());

    private Collection<TestResult> testResults;
    private Collection<TestRunner> testRunners;

    private Collection<ClassUnderTest> cuts;
    private Collection<UnitTest> tests;

    boolean attemptCompilationSkip = true;

    public TestBatch(Collection<ClassUnderTest> classesUnderTest,
                     Collection<UnitTest> unitTests)
    {
        cuts = classesUnderTest;
        tests = unitTests;
        testRunners = new ArrayList<>(tests.size() * cuts.size());
        testResults = new ArrayList<>(tests.size() * cuts.size());

        for (UnitTest t : tests)
        {
            for (ClassUnderTest c : cuts)
            {
                TestRunner testRunner = new TestRunner(c, t);
                testRunners.add(testRunner);
            }
        }
    }

    public TestBatch(ClassUnderTest classUnderTest,
                     Collection<UnitTest> unitTests)
    {
        cuts = new ArrayList<>(1);
        cuts.add(classUnderTest);
        tests = unitTests;
        testRunners = new ArrayList<>(tests.size() * cuts.size());
        testResults = new ArrayList<>(tests.size() * cuts.size());

        for (UnitTest t : tests)
        {
            TestRunner testRunner = new TestRunner(classUnderTest, t);
            testRunners.add(testRunner);
        }
    }

    public TestBatch(ClassUnderTest classUnderTest, UnitTest unitTest)
    {
        testRunners = new ArrayList<>(1);
        testResults = new ArrayList<>(1);

        testRunners.add(new TestRunner(classUnderTest, unitTest));
    }

    public void compileTests()
    {
        for(TestRunner r : testRunners)
        {
            r.compileTest(attemptCompilationSkip);
        }
    }

    @Override
    public void run()
    {
        compileTests();

        StopWatch stopWatch = StopWatch.createStarted();

        // Run all test runners concurrently
        ExecutorService batchExec = Executors.newWorkStealingPool();

        for (TestRunner tr : testRunners)
        {
            batchExec.submit(tr::run);
        }

        batchExec.shutdown();
        try
        {
            while(!batchExec.awaitTermination(Global.PER_TEST_TIMEOUT * testRunners.size(), TimeUnit.SECONDS))
                batchExec.shutdownNow();
        } catch (InterruptedException e)
        {
            e.printStackTrace();
            System.err.println("Batch Compiler was interrupted.");
        }


        for (TestRunner tr : testRunners)
            testResults.add(tr.getTestResult());

        stopWatch.stop();
        logger.info(this.hashCode() + " Executed " + testRunners.size() + " tests in " + stopWatch.toString());
    }

    public Collection<TestResult> getTestResults()
    {
        return testResults;
    }

    public void setAttemptCompilationSkip(boolean attemptCompilationSkip)
    {
        this.attemptCompilationSkip = attemptCompilationSkip;
    }
}
