package coverweight;

import coverweight.configuration.*;
import coverweight.experiments.SimpleAnalysisExperiment;
import coverweight.report.HitCounts;
import coverweight.runner.*;
import coverweight.sourcefiles.ClassUnderTest;
import coverweight.sourcefiles.UnitTest;
import coverweight.weighting.WeightMap;
import coverweight.weighting.WeightedCoverageGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

public class CoverWeight
{
    private Logger logger = LoggerFactory.getLogger(getClass());

    private List<ClassUnderTest> modelCuts;
    private List<ClassUnderTest> cuts;
    private List<UnitTest> unitTests;

    private List<WeightMap> weightMaps;

    private ConcurrentMap<UnitTestCUTPair, TestResult> testResultMap;

    private ConcurrentMap<ClassUnderTest, ConcurrentMap<WeightMap, HitCounts>> hitCountsMap;

    public static boolean mute = true;

    public static void main(String[] args)
    {
        CLIReader cli = new CLIReader(args);
        CoverWeight coverWeight = new CoverWeight(cli, new HashSet<>());
        List<HitCounts> hcList = coverWeight.run();

        printInfo(hcList);

        SimpleAnalysisExperiment simpleAnalysisExperiment = new SimpleAnalysisExperiment(coverWeight);
        simpleAnalysisExperiment.run();

    }

    public List<ClassUnderTest> getModelCuts()
    {
        return modelCuts;
    }

    private static void printInfo(Collection<HitCounts> hitCountsCollection)
    {
        for (HitCounts hc : hitCountsCollection)
        {
            System.out.println(hc.toString());
            System.out.println(hc.weightedCoverageSummary());
        }
        for (HitCounts hc : hitCountsCollection)
        {
            System.out.println(hc.simpleStatistics());
        }
    }


    @Override
    public CoverWeight clone()
    {
        CoverWeight cloned = new CoverWeight(cuts, unitTests, weightMaps, testResultMap);
        if(!modelCuts.isEmpty())
        {
            cloned.setModelCuts(this.modelCuts);
        }
        return cloned;
    }

    public void setModelCuts(List<ClassUnderTest> modelCuts)
    {
        this.modelCuts = modelCuts;
    }

    public void setWeightMaps(List<WeightMap> weightMapCollection)
    {
        weightMaps = weightMapCollection;
        //weightMaps = new ArrayList<>();
        //addWeightMaps(weightMapCollection);
    }

    public void addWeightMaps(Collection<WeightMap> weightMapCollection)
    {
        weightMaps.addAll(weightMapCollection);
    }

    public CoverWeight(List<ClassUnderTest> classUnderTestList, List<UnitTest> unitTestList, List<WeightMap> weightMapList, ConcurrentMap<UnitTestCUTPair, TestResult> unitTestCUTPairTestResultMap)
    {
        cuts = classUnderTestList;
        unitTests = unitTestList;
        weightMaps = weightMapList;
        testResultMap = unitTestCUTPairTestResultMap;
        hitCountsMap = new ConcurrentHashMap<>();
    }

    public CoverWeight(CLIReader cli, Collection<ExecFlag> execFlags)
    {
        this(PathLoader.loadPaths(cli.getInputValue(OptionNames.CUT)),
                    PathLoader.loadPaths(cli.getInputValue(OptionNames.TESTS)),
                    PathLoader.loadFirstPath(cli.getInputValue(OptionNames.BUILT_CLASSES)),
                    PathLoader.loadFirstPath(cli.getInputValue(OptionNames.BUILT_TESTS)),
                    cli.optionIsSet(OptionNames.NESTED_CUTS),
                    cli.getInputValue(OptionNames.CUT_FILENAME));

        loadCsvPaths(cli);

        if(execFlags.contains(ExecFlag.ONLY_MODEL_CUTS))
            cuts = new ArrayList<>();

        // Attempt to load model CUTs
        try
        {
            loadModelCUTs(PathLoader.loadPaths(cli.getInputValue(OptionNames.MODEL_CUT_DIR)),
                    cli.getInputValue(OptionNames.MODEL_CUT_RELATIVE_FILE),
                    PathLoader.loadFirstPath(cli.getInputValue(OptionNames.BUILT_CLASSES)));
        }
        catch (IllegalArgumentException argEx)
        {
            argEx.printStackTrace();
            logger.warn("Model CUT(s) not defined, some results will be limited...");
        }

        // Replace weightMaps with normalized weightMaps
        weightMaps = generateNormalizedWeightMaps();
    }

    private void loadModelCUTs(Collection<Path> modelCutDirPathCollection,
                               String modelCutRelativeFileLoc,
                               Path builtCutDir)
    {
        modelCuts = new ArrayList<>();
        for (Path modelSrcRootDir : modelCutDirPathCollection)
        {
            Path path = Paths.get(modelSrcRootDir.toString(), modelCutRelativeFileLoc);
            boolean cutDetected = false;
            for (ClassUnderTest c : cuts)
            {
                if (c.getJavaFile().equals(path))
                {
                    cutDetected = true;
                    modelCuts.add(c);
                    break;
                }
            }

            if(!cutDetected)
            {
                // TODO this should use the root of the particular model solution instead
                // TODO define list of model directories in CLI, and a local path to the solution file
                //ClassUnderTest cut = initClassUnderTest(path, builtCutDir, modelSrcRootDir);
                ClassUnderTest cut = new ClassUnderTest(path, modelSrcRootDir);
                cut.init(builtCutDir);
                cuts.add(cut);
                modelCuts.add(cut);
            }
        }
    }

    public CoverWeight(Collection<Path> sutPathCollection,
                       Collection<Path> testDirPathCollection,
                       Path builtCutDir,
                       Path builtTestDir,
                       boolean nestedCutDirs,
                       String cutFilename)
    {
        Global.init();
        cuts = new ArrayList<>();
        unitTests = new ArrayList<>();
        weightMaps = new ArrayList<>();
        testResultMap = new ConcurrentHashMap<>();
        hitCountsMap = new ConcurrentHashMap<>();

        addClassesUnderTest(sutPathCollection, builtCutDir, nestedCutDirs, cutFilename);
        setupTests(testDirPathCollection, builtTestDir);
        addNaiveWeightMap();
    }

    /**
     * Initialise coverweight without any specific classes under test
     * @param testDirPathCollection
     * @param builtTestDir
     */
    public CoverWeight(Collection<Path> testDirPathCollection,
                       Path builtTestDir)
    {
        Global.init();
        unitTests = new ArrayList<>();
        weightMaps = new ArrayList<>();
        testResultMap = new ConcurrentHashMap<>();
        hitCountsMap = new ConcurrentHashMap<>();

        setupTests(testDirPathCollection, builtTestDir);
        addNaiveWeightMap();
    }

    public void addTest(Path testSourcePath, Path builtTestDirPath, Path sourceRootDir)
    {
        UnitTest test = new UnitTest(testSourcePath);
        test.setRelativeDir(constructRelativeDir(sourceRootDir, testSourcePath.getParent()));
        System.out.println("Relative: " + constructRelativeDir(sourceRootDir, testSourcePath));
        test.setClassDir(builtTestDirPath);
        unitTests.add(test);
    }

    public void addTestDir(Path testDirPath, Path builtTestDirPath)
    {
        // TODO implement (optional?) recursive test search here

        // Recursive
        List<Path> javaFiles = recursiveJavaFileSearch(testDirPath);
        for (Path p : javaFiles)
            // TODO must add relative file paths for build location
            addTest(p, builtTestDirPath, testDirPath);

        // Non recursive:
        /*
        try
        {
            Files.list(testDirPath).forEach(p -> {
                System.out.println(p.getFileName());
                if(p.getFileName().toString().endsWith(".java"))
                {
                    // TODO may need to do some deletion of common roots to maintain package structure for more complex cases
                    addTest(p, builtTestDirPath);
                }
            });
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        */
    }

    private String constructRelativeDir(Path rootDir, Path targetPath)
    {
        String result = targetPath.toString().replace(rootDir.toString(), "");
        if (result.isEmpty())
            return result;
        if(result.endsWith("/") || result.endsWith("\\"))
            result = result.substring(0, result.length() - 1);
        char first = result.charAt(0);
        if(first == '/' || first == '\\')
            result = result.substring(1);
        return result;
    }

    private List<Path> recursiveJavaFileSearch(Path dirToSearch)
    {
        List<Path> foundPaths = new ArrayList<>();
        try
        {
            Files.list(dirToSearch).forEach(p -> {
                if (p.toFile().isDirectory())
                    foundPaths.addAll(recursiveJavaFileSearch(p));
                else if (p.toString().endsWith(".java"))
                    foundPaths.add(p);
            });
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        return foundPaths;
    }

    private void setupTests(Collection<Path> testDirPathCollection, Path builtTestDir)
    {
        for (Path testDir : testDirPathCollection)
        {
            // Load individual test .java Paths
            addTestDir(testDir, builtTestDir);
        }
    }

    private void addNaiveWeightMap()
    {
        // Set naive weightmap
        weightMaps.add(new WeightMap(unitTests)); // Naive
    }

    private List<WeightMap> generateNormalizedWeightMaps()
    {
        return normalizeWeightMaps(weightMaps);
    }

    public static List<WeightMap> normalizeWeightMaps(List<WeightMap> toNormalize)
    {
        List<WeightMap> normalized = new ArrayList<>();

        for (WeightMap wm : toNormalize)
        {
            normalized.add(wm.generateNormalized());
        }
        return normalized;
    }

    public void addClassesUnderTest(Collection<Path> cutPathCollection, Path builtCutDir, boolean cutDirsAreNested, String cutFilename)
    {
        Collection<ClassUnderTest> cutsToInit = new ArrayList<>();

        for (Path c : cutPathCollection)
        {
            if(cutDirsAreNested)
            {

                for (File individualCutDir : c.toFile().listFiles(File::isDirectory))
                {

                    for (File f : recursivelyFindMatchingFiles(new ArrayList<>(), individualCutDir, cutFilename))
                    {
                        if(f.exists())
                            cutsToInit.add(new ClassUnderTest(f.toPath(), individualCutDir.toPath()));
                    }
                }

            }
            else
            {
                cutsToInit.add(new ClassUnderTest(c, c.getParent()));
            }
        }

        cutsToInit.parallelStream().forEach(c -> c.init(builtCutDir));
        cuts.addAll(cutsToInit);
        removeUncompiledCUTs();
    }

    private void removeUncompiledCUTs()
    {
        Collection<ClassUnderTest> cutsToRemove = new ArrayList<>();

        cutsToRemove.addAll(cuts.stream().filter(c -> !c.isCompiled()).collect(Collectors.toList()));

        cuts.removeAll(cutsToRemove);
    }

    private static List<File> recursivelyFindMatchingFiles(List<File> collected, File dir, String targetFilename)
    {
        if (!dir.exists())
            return collected;

        // Add matching java files at this level
        collected.addAll(Arrays.asList(dir.listFiles(f -> (!f.isDirectory() && f.getName().equals(targetFilename)))));

        // Descend
        for (File subDir : dir.listFiles(File::isDirectory))
        {
            collected.addAll(recursivelyFindMatchingFiles(new ArrayList<>(), subDir, targetFilename));
        }

        return collected;

    }

    public List<HitCounts> run()
    {
        return run(cuts);
    }

    public List<HitCounts> run(Collection<ClassUnderTest> classesUnderTest)
    {
        // TODO should collect classpath to improve compatibility for more complex tasks


        List<HitCounts> weightedResults = new ArrayList<>(classesUnderTest.size() * unitTests.size());

        int i = 0;
        for (ClassUnderTest c : classesUnderTest)
        {
            i++;
            if (!mute)
                logger.info("Running CUT " + c.getRelativeDir() + "(" + i + " / " + classesUnderTest.size() + ")");
            weightedResults.addAll(run(c));
        }

        return weightedResults;
    }

    public Collection<HitCounts> run(ClassUnderTest classUnderTest)
    {
        // Setup hitcounts storage
        ConcurrentMap<WeightMap, HitCounts> weightMapHitCountsMap = new ConcurrentHashMap<>();

        if (!mute)
            logger.info("Getting test info for " + classUnderTest.getIdentifer());

        List<UnitTestCUTPair> unitTestCUTPairs = new ArrayList<>(unitTests.size());
        for (UnitTest t : unitTests)
            unitTestCUTPairs.add(new UnitTestCUTPair(t, classUnderTest));

        // Find unit tests that do not have results for the current ClassUnderTest
        List<UnitTest> notExecuted = new ArrayList<>();
        for (UnitTestCUTPair p : unitTestCUTPairs)
        {
            if(!testResultMap.containsKey(p))
                notExecuted.add(p.getUnitTest());
        }

        // Run tests that haven't been executed yet
        if(!notExecuted.isEmpty())
        {
            logger.info("Running un-executed tests for CUT " + classUnderTest.getIdentifer() + "...");
            // Run tests
            TestBatch tb = new TestBatch(classUnderTest, notExecuted);
            tb.run();

            // Store results
            Collection<TestResult> testResults = tb.getTestResults();
            for (TestResult tr : testResults)
            {
                testResultMap.put(tr.getUnitTestCUTPair(), tr);
            }
        }

        // Get existing test results, should be as many as elements in unitTestCUTPairs
        List<TestResult> testResults = new ArrayList<>(unitTestCUTPairs.size());
        for(UnitTestCUTPair p : unitTestCUTPairs)
        {
            testResults.add(testResultMap.get(p));
        }

        List<WeightedCoverageGenerator> coverageGenerators = new ArrayList<>(weightMaps.size());
        for (WeightMap wm : weightMaps)
        {
            coverageGenerators.add(new WeightedCoverageGenerator(wm, testResults));
        }

        // Generate hit counts
        //Collection<HitCounts> weightedResults = new ArrayList<>(coverageGenerators.size());
        //coverageGenerators.parallelStream().forEach(covGen -> weightedResults.add(covGen.generateWeightedResults(classUnderTest)));
        Collection<HitCounts> weightedResults =
                coverageGenerators.parallelStream().
                        map(covGen -> covGen.generateWeightedResults(classUnderTest)).
                        collect(Collectors.toList());
        /*
        for (WeightedCoverageGenerator covGen : coverageGenerators)
            weightedResults.add(covGen.generateWeightedResults(classUnderTest));
            */

        for (HitCounts hc : weightedResults)
        {
            weightMapHitCountsMap.put(hc.getWeightMap(), hc);
        }

        hitCountsMap.put(classUnderTest, weightMapHitCountsMap);
        return weightedResults;
    }

    private void loadCsvPaths(CLIReader cli)
    {
        if (cli.optionIsSet(OptionNames.WEIGHT_CSVS))
        {
            List<Path> weightCsvDirs = PathLoader.loadPaths(cli.getInputValue(OptionNames.WEIGHT_CSVS));
            for (Path csvDir : weightCsvDirs)
            {
                System.out.println("Getting csv files in " + csvDir);
                try
                {
                    Files.list(csvDir).forEach(p -> {
                        if(p.getFileName().toString().endsWith(".csv"))
                        {
                            System.out.println("Adding csv file " + p);
                            weightMaps.add(new WeightMap(p, unitTests));
                        }
                    });
                } catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        }
    }

    public List<UnitTest> getUnitTests()
    {
        return unitTests;
    }

    public List<WeightMap> getWeightMaps()
    {
        return weightMaps;
    }

    @Override
    public String toString()
    {
        return "CoverWeight{" +
                "cuts=" + cuts +
                ", unitTests=" + unitTests +
                ", weightMaps=" + weightMaps +
                '}';
    }

    public List<ClassUnderTest> getCuts()
    {
        return cuts;
    }

    public ConcurrentMap<ClassUnderTest, ConcurrentMap<WeightMap, HitCounts>> getHitCountsMap()
    {
        return hitCountsMap;
    }

    public HitCounts getHitCounts(ClassUnderTest classUnderTest, WeightMap weightMap)
    {
        return hitCountsMap.get(classUnderTest).get(weightMap);
    }

    public Map<WeightMap, HitCounts> getWeightMapToHitCountsMap(ClassUnderTest classUnderTest)
    {
        return hitCountsMap.get(classUnderTest);
    }

    public ConcurrentMap<UnitTestCUTPair, TestResult> getTestResultMap()
    {
        return testResultMap;
    }

    public TestResult getTestResultForTestCUT(UnitTest test, ClassUnderTest cut)
    {
        UnitTestCUTPair pair = new UnitTestCUTPair(test, cut);
        return getTestResultMap().get(pair);
    }

    public void setCuts(List<ClassUnderTest> cuts)
    {
        this.cuts = cuts;
    }

    public void setUnitTests(List<UnitTest> unitTests)
    {
        this.unitTests = unitTests;
    }
}