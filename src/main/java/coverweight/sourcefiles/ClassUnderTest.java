package coverweight.sourcefiles;

import coverweight.mistakesim.checkresults.GradeerSolution;
import coverweight.runner.AntProcessResult;
import coverweight.runner.AntRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

public class ClassUnderTest extends Source
{
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private Path rootDirectory;
    private String relativePath;

    public ClassUnderTest(Path javaFilePath, Path rootDir)
    {
        super(javaFilePath);
        rootDirectory = rootDir;

        relativePath = javaFilePath.toString().replace(rootDirectory.toString(), "");
        setIdentifer(rootDir.getFileName().toString());
    }

    public ClassUnderTest(GradeerSolution gradeerSolution)
    {
        super(gradeerSolution);
        setIdentifer(gradeerSolution.getName());
    }

    @Override
    public void setClassDir(Path builtDir)
    {
        builtDir.toFile().mkdirs();
        File loc = new File(builtDir.toString() + File.separator + rootDirectory.toFile().getName());
        //System.out.println(loc);
        loc.mkdir();
        this.classDir = loc.toPath();
    }

    public Path getClassFilePath()
    {
        String javaLoc = getClassDir().toString() + relativePath;
        // Remove .java extension
        javaLoc = javaLoc.replace(".java", "");
        //javaLoc = javaLoc.substring(0, javaLoc.length() - 6);
        return Paths.get(javaLoc + ".class");
    }

    @Override
    public boolean isCompiled()
    {
        if(super.isCompiled())
            return true;

        Path classFile = Paths.get(classDir.toString() + relativePath.replace(".java", ".class"));
        return Files.exists(classFile);
    }

    @Override
    protected void defaultIdentifier()
    {
        setIdentifer(getRelativeDir());
    }

    public void init(Path builtCutDir)
    {
        logger.info("Initializing ClassUnderTest for " + getJavaFile());

        setClassDir(builtCutDir);

        // Check if original location has a matching .class file; copy if it does
        /*Path classFile = Paths.get(getJavaFile().toString().replace(".java", ".class"));
        if(Files.exists(classFile))
        {
            // Copy to built directory
        }*/

        if (!isCompiled())
        {
            AntRunner antRunner = new AntRunner();
            AntProcessResult r = antRunner.compileCut(this);
            if(!r.compiled())
                System.out.println(r);
        }
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ClassUnderTest that = (ClassUnderTest) o;
        return Objects.equals(rootDirectory, that.rootDirectory) &&
                Objects.equals(relativePath, that.relativePath);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(super.hashCode(), rootDirectory, relativePath);
    }
}
