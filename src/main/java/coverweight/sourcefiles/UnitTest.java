package coverweight.sourcefiles;

import coverweight.configuration.Global;
import coverweight.runner.AntProcessResult;
import coverweight.runner.AntRunner;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Pattern;

public class UnitTest extends Source
{
    public UnitTest(Path sourceFilePath)
    {
        super(sourceFilePath);
    }

    public Path getJacocoExecPath(ClassUnderTest cut)
    {
        /*
        return Paths.get(getClassDir() + File.separator
                + "jacoco-" + getBaseName() + ".exec");
                */
        return Paths.get(Global.HOME_DIR + File.separator +
                        "jacocoExecs" + File.separator +
                        cut.getRelativeDir().replaceAll("/", "_") + File.separator +
                        this.getRelativeDir().replaceAll("/", "_") + File.separator +
                        "jacoco-" + getBaseName() + ".exec");
    }

    @Override
    public boolean equals(Object o)
    {
        if(o == null)
            return false;
        if(!(o instanceof UnitTest))
            return false;
        return super.equals(o);
    }

    public String getFullyQualifiedClassName()
    {
        try
        {
            Optional<Path> foundPath = Files.walk(getClassDir()).filter(Files::isRegularFile)
                    .filter(p -> p.endsWith(getBaseName() + ".class")).findFirst();

            if(foundPath.isPresent())
            {
                // Convert to fully qualified name
                String path = foundPath.get().toString();
                path = path.replace(getClassDir().toString(), "")
                        .replace(".class", "")
                        .replaceAll(File.separator, ".");
                if(path.startsWith("."))
                    path = path.replaceFirst(".", "");
                return path;
            }
            return getBaseName();
        } catch (IOException e)
        {
            e.printStackTrace();
            // Generally won't be necessary, but this is the safest option in case of failure.
            return getBaseName();
        }
    }

    @Override
    protected void defaultIdentifier()
    {
        setIdentifer(getBaseName());
    }
}
