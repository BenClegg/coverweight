package coverweight.sourcefiles;

import coverweight.mistakesim.checkresults.GradeerSolution;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

public abstract class Source
{
    private String fileName;
    private String baseName;
    private Path javaFile;
    private String relativeDir; // In the form "relative/parent/directory/of/source"

    protected Path classDir;

    private String identifer;


    public Source(Path javaFilePath)
    {
        javaFile = javaFilePath;
        relativeDir = javaFile.getParent().toFile().getName();
        fileName = javaFile.getFileName().toString();
        baseName = fileName.replace(".java", "");
        defaultIdentifier();
    }

    public Source(Path javaFilePath, Path builtClassDirPath)
    {
        this(javaFilePath);
        setClassDir(builtClassDirPath);
    }

    protected Source(GradeerSolution gradeerSolution)
    {
        javaFile = Paths.get(gradeerSolution.getName());
        relativeDir = gradeerSolution.getName();
        fileName = gradeerSolution.getName();
        baseName = gradeerSolution.getName();
        defaultIdentifier();
    }


    public String getFileName()
    {
        return fileName;
    }

    public String getBaseName()
    {
        return baseName;
    }

    public Path getJavaFile()
    {
        return javaFile;
    }

    public Path getClassDir()
    {
        return classDir;
    }

    public void setClassDir(Path builtDir)
    {
        builtDir.toFile().mkdirs();
        this.classDir = builtDir;
        /*
        File loc = new File(builtDir.toString() + File.separator + relativeDir);
        System.out.println(loc);
        loc.mkdirs();
        this.classDir = loc.toPath();
        */
    }

    public boolean isCompiled()
    {
        String location = classDir.toString() + File.separator + getBaseName() + ".class";
        location = location.replace(File.separator + File.separator, File.separator);
        File c = new File(location);
        return c.exists();
    }

    public String getRelativeDir()
    {
        return relativeDir;
    }

    public String getIdentifer()
    {
        return identifer;
    }

    public void setIdentifer(String identifer)
    {
        this.identifer = identifer;
    }

    protected abstract void defaultIdentifier();

    public void setRelativeDir(String relativeDir)
    {
        this.relativeDir = relativeDir;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Source source = (Source) o;
        return Objects.equals(fileName, source.fileName) &&
                Objects.equals(baseName, source.baseName) &&
                Objects.equals(javaFile, source.javaFile) &&
                Objects.equals(relativeDir, source.relativeDir) &&
                Objects.equals(classDir, source.classDir) &&
                Objects.equals(identifer, source.identifer);
    }

    @Override
    public int hashCode()
    {
        return javaFile.hashCode();
    }
}
