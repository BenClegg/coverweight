package coverweight.mistakesim;

import coverweight.sourcefiles.UnitTest;
import coverweight.weighting.WeightMap;

import java.util.*;

public class WeightMapGenerator
{
    private static final int TARGET_NUMBER_SUITES = 80;
    //private static final int TARGET_NUMBER_SUITES = 400;
    private static final int SEARCH_BUDGET = TARGET_NUMBER_SUITES * 6;

    private static final int INITIAL_RANDOM_SEED = 1793;

    private static final double SUITE_SIZE_FACTOR = 0.5;

    private static final double MAX_SUITE_PROPORTION = 0.7; // % of tests enabled

    private Collection<UnitTest> tests;


    public WeightMapGenerator(Collection<UnitTest> tests)
    {
        this.tests = tests;
    }

    public Set<WeightMap> generateWeightMapSetFixedSize(int randomSeedOffset)
    {
        // Set random seed
        Random random = new Random(INITIAL_RANDOM_SEED + (1237 * randomSeedOffset));
        // Set suite size
        final int SUITE_SIZE = (int) Math.floor(this.tests.size() * SUITE_SIZE_FACTOR);

        // Generate
        Set<WeightMap> generated = new HashSet<>();
        int consumedSearchBudget = 0;
        while (generated.size() < TARGET_NUMBER_SUITES)
        {
            consumedSearchBudget++;
            if(consumedSearchBudget > SEARCH_BUDGET)
                break;
            generated.add(generateWeightMap(random, SUITE_SIZE));
        }
        return generated;
    }

    public Set<WeightMap> generateWeightMapSetSizeControlled(int randomSeedOffset)
    {
        // Set random seed
        Random random = new Random(INITIAL_RANDOM_SEED + (1237 * randomSeedOffset));

        // Target number of suites per size
        final int MAX_SIZE = (int) Math.ceil((double) tests.size() * MAX_SUITE_PROPORTION);
        final int NUM_DIFFERENT_SIZES = MAX_SIZE - 1;
        final int TARGET_TESTS_PER_SIZE = (int) Math.ceil((double)TARGET_NUMBER_SUITES / NUM_DIFFERENT_SIZES);
        final int BUDGET_PER_SIZE = TARGET_TESTS_PER_SIZE * 6;

        // Generate
        Set<WeightMap> allGenerated = new HashSet<>();

        for (int s = 2; s <= MAX_SIZE; s++)
        {
            // Per size
            Set<WeightMap> generatedForSize = new HashSet<>();
            int consumedSearchBudget = 0;

            while (generatedForSize.size() < TARGET_TESTS_PER_SIZE)
            {
                consumedSearchBudget++;
                if(consumedSearchBudget > BUDGET_PER_SIZE)
                    break;
                generatedForSize.add(generateWeightMap(random, s));
            }

            allGenerated.addAll(generatedForSize);
        }
        return allGenerated;
    }

    public Set<WeightMap> generateWeightMapSetRandomSize(int randomSeedOffset)
    {
        // Set random seed
        Random random = new Random(INITIAL_RANDOM_SEED + (971 * randomSeedOffset));

        // Generate
        Set<WeightMap> generated = new HashSet<>();
        int consumedSearchBudget = 0;
        while (generated.size() < TARGET_NUMBER_SUITES)
        {
            consumedSearchBudget++;
            if(consumedSearchBudget > SEARCH_BUDGET)
                break;

            // Set random suite size between MIN_SIZE and n - 1 (n = number of tests)
            final int MIN_SIZE = 2;
            int suiteSize = MIN_SIZE + random.nextInt(tests.size() - MIN_SIZE);

            generated.add(generateWeightMap(random, suiteSize));
        }
        return generated;

    }

    private WeightMap generateWeightMap(Random random, int suiteSize)
    {
        // Configure empty WeightMap
        WeightMap w = new WeightMap(tests);
        w.replaceAll((t, v) -> 0.0);

        // Copy of set of tests to select from
        List<UnitTest> pool = new ArrayList<>(tests);
        Set<UnitTest> chosen = new HashSet<>();

        while (chosen.size() < suiteSize)
        {
            if(pool.isEmpty())
                break;

            // Choose test in pool, move it to chosen
            Collections.shuffle(pool, random);
            UnitTest t = pool.get(0);

            chosen.add(t);
            pool.remove(t);
        }

        // Set weights of chosen tests to 1
        for (UnitTest t : chosen)
        {
            w.put(t, 1.0);
        }

        w.setName(String.valueOf(w.hashCode()));
        w.getTotalWeights();
        return w;
    }

}
