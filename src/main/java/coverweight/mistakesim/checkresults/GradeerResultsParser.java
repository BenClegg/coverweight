package coverweight.mistakesim.checkresults;

import coverweight.sourcefiles.UnitTest;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class GradeerResultsParser
{
    private Collection<UnitTest> unitTests;

    public GradeerResultsParser(Collection<UnitTest> unitTests)
    {

        this.unitTests = unitTests;
    }

    public Collection<TestResultsMap> parse(String resultsCSVRaw) throws IOException
    {
        Collection<Path> csvPaths = Arrays.stream(resultsCSVRaw.split(","))
                .map(Paths::get)
                .collect(Collectors.toList());

        if(csvPaths.isEmpty())
            throw new IOException("No CSVs defined");

        return parseAll(csvPaths);

    }

    public TestResultsMap parse(Path resultsCSV) throws IOException
    {
        // Load file lines
        List<String> lines = Files.readAllLines(resultsCSV);
        if(lines.isEmpty())
            throw new IOException("No lines read for " + resultsCSV);
        if(lines.size() < 2)
            throw new IOException("Read contents of " + resultsCSV + " are too short (<2 lines)");

        // Identify UnitTest names from first line
        String header = lines.remove(0);
        String[] columns = header.split(",");

        // Get Map of column numbers to identified tests
        Map<Integer, UnitTest> identifiedTests = new HashMap<>();
        for (int i = 0; i < columns.length; i++)
        {
            final String c = cleanCell(columns[i]);
            if(!c.toLowerCase().contains("solution"))
            {
                Optional<UnitTest> matchingTest = unitTests.stream().filter(t -> t.getIdentifer().equals(c)).findFirst();
                if(matchingTest.isPresent())
                    identifiedTests.put(i, matchingTest.get());
            }
        }

        // Populate TestResultsMap with each Solution line
        TestResultsMap testResultsMap = new TestResultsMap(
                resultsCSV.getParent().getFileName().toString()
        );

        for (String l : lines)
        {
            List<String> cells = Arrays.asList(l.split(","));

            // First cell is Solution
            GradeerSolution solution = new GradeerSolution(cleanCell(cells.get(0)));

            // Remaining are tests
            for (int i = 1; i < cells.size(); i++)
            {
                if(identifiedTests.containsKey(i))
                {
                    UnitTest t = identifiedTests.get(i);
                    boolean faultDetected = valueIsFaultDetected(cleanCell(cells.get(i)));
                    testResultsMap.addEntry(t, solution, faultDetected);
                }

            }
        }

        return testResultsMap;

    }

    public Collection<TestResultsMap> parseAllMerged(Collection<Path> resultCSVPaths) throws IOException
    {
        Collection<TestResultsMap> individuals = parseAll(resultCSVPaths);

        return TestResultsMap.generateAllPossibleMerges(individuals);
    }

    public Collection<TestResultsMap> parseAll(Collection<Path> resultCSVPaths) throws IOException
    {
        Collection<TestResultsMap> individuals = new ArrayList<>();
        for (Path p : resultCSVPaths)
            individuals.add(parse(p));

        return individuals;
    }

    static boolean valueIsFaultDetected(String value)
    {
        double d = Double.parseDouble(value);
        return d < 1.0;
    }
    private static String cleanCell(String cell)
    {
        return cell.replaceAll("\"", "")
                .replaceAll("TestSuiteCheck-", "");
    }
}
