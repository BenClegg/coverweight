package coverweight.mistakesim.checkresults;

import java.util.Objects;

public class GradeerSolution
{

    private String name;

    public GradeerSolution(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GradeerSolution that = (GradeerSolution) o;
        return Objects.equals(getName(), that.getName());
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(getName());
    }
}
