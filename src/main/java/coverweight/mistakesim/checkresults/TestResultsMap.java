package coverweight.mistakesim.checkresults;
import coverweight.sourcefiles.UnitTest;
import coverweight.weighting.WeightMap;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.stream.Collectors;

/**
 * The boolean value represents fault detection.
 * True = detected, False = not detected.
 */
public class TestResultsMap extends HashMap<UnitTest, Map<GradeerSolution, Boolean>>
{
    private String name;
    private Set<GradeerSolution> allGradeerSolutions = new HashSet<>();
    private Set<GradeerSolution> detectableSolutions = new HashSet<>();

    private static final boolean EXCLUDE_ALWAYS_DETECTED = false; // Disabled; equivalent mutants already removed
    private static final String[] NAME_COMMON = {"fa-FQs", "fa-DataLoader", "output-", "_allCheckResults"};

    public TestResultsMap(String name)
    {
        super();
        this.name = name;

        // TODO remove unnecessary parts of name by positions of underscores?
        for (String r : NAME_COMMON)
        {
            this.name = this.name.replaceAll(r, "");
        }
    }

    public TestResultsMap(TestResultsMap toCopy)
    {
        super();
        this.name = toCopy.getName();

        for (UnitTest t : toCopy.keySet())
        {
            for (GradeerSolution s : toCopy.getAllGradeerSolutions())
            {
                this.addEntry(t, s, toCopy.testDetectsSolution(t, s));
            }
        }
    }

    public void removeAlwaysFailing()
    {
        Set<GradeerSolution> alwaysFailing = allGradeerSolutions.stream()
                .filter(s -> this.keySet().stream().anyMatch(t -> !testDetectsSolution(t, s)))
                .collect(Collectors.toSet());

        // Remove individual entries
        for (UnitTest t : keySet())
        {
            // Remove undetectable entries
            for(GradeerSolution s : alwaysFailing)
            {
                get(t).remove(s);
            }
        }

        // Remove solutions that fail all tests from allGradeerSolutions
        allGradeerSolutions.removeAll(alwaysFailing);

    }

    public void removeUndetectable()
    {
        populateDetectableSolutions();
        Set<GradeerSolution> undetectable = allGradeerSolutions.stream()
                .filter(s -> !detectableSolutions.contains(s))
                .collect(Collectors.toSet());

        for (UnitTest t : keySet())
        {
            // Remove undetectable entries
            for(GradeerSolution s : undetectable)
            {
                get(t).remove(s);
            }
        }

        // Remove undetectable from allGradeerSolutions
        allGradeerSolutions.removeAll(undetectable);
    }

    /**
     * "Merge" mutants with identical test outcomes by removing all but one with the same signature
     */
    public void mergeIdentical()
    {
        // Get test signature for each solution
        Map<GradeerSolution, Map<UnitTest, Boolean>> testSignatures = new HashMap<>();
        for(GradeerSolution s : allGradeerSolutions)
        {
            testSignatures.put(s, testSignature(s));
        }

        // Construct a list of solutions to remove, by finding those that match another solution
        List<GradeerSolution> toRemove = new ArrayList<>();

        for (GradeerSolution solution : testSignatures.keySet())
        {
            // Skip those that have already been identified as removable
            if (toRemove.contains(solution))
                continue;

            Map<UnitTest, Boolean> solutionSig = testSignatures.get(solution);

            // Identify solutions with same test signature
            Collection<GradeerSolution> matching = new ArrayList<>();
            testSignatures.keySet().stream()
                    .filter(s -> !s.equals(solution))
                    .forEach(s ->
                    {
                        if (testSignatures.get(s).keySet().stream().allMatch( t ->
                                    testSignatures.get(s).get(t).equals(solutionSig.get(t))
                        ))
                        {
                            matching.add(s);
                        }
                    }
            );

            // Ensure that matching does not contain the solution being compared
            if(matching.contains(solution))
                matching.remove(solution);
            // Matching tests should be removed
            toRemove.addAll(matching);
        }

        // Remove every pending solution
        for (UnitTest t : keySet())
        {
            // Remove undetectable entries
            for(GradeerSolution s : toRemove)
            {
                get(t).remove(s);
            }
        }

        detectableSolutions.removeAll(toRemove);
        allGradeerSolutions.removeAll(toRemove);
    }

    private Map<UnitTest, Boolean> testSignature(GradeerSolution solution)
    {
        Map<UnitTest, Boolean> signature = new HashMap<>();

        for (UnitTest t : keySet())
        {
            signature.put(t, get(t).get(solution));
        }

        return signature;
    }


    public void addEntry(UnitTest test, GradeerSolution gradeerSolution, boolean faultDetected)
    {
        // Create if Check not yet present
        if(!containsKey(test))
            put(test, new HashMap<>());

        get(test).put(gradeerSolution, faultDetected);
        allGradeerSolutions.add(gradeerSolution);
    }

    public boolean testDetectsSolution(UnitTest test, GradeerSolution gradeerSolution)
    {
        return get(test).get(gradeerSolution);
    }

    private void populateDetectableSolutions()
    {
        for (GradeerSolution s : allGradeerSolutions)
        {
            if(solutionDetectedByTests(this.keySet(), s))
                detectableSolutions.add(s);
        }
    }

    public double faultDetectionRate(WeightMap weightMap, boolean detectableOnly)
    {
        Collection<GradeerSolution> detected = new ConcurrentLinkedDeque<>();

        Collection<GradeerSolution> solutionPool = new ConcurrentLinkedDeque<>(allGradeerSolutions);
        // Only use solutions that can be detected
        if (detectableOnly)
        {
            if(detectableSolutions.isEmpty())
                populateDetectableSolutions();
            solutionPool = detectableSolutions;
        }

        if (EXCLUDE_ALWAYS_DETECTED)
        {
            solutionPool = solutionPool.stream()
                    .filter(s -> this.keySet().stream().anyMatch(t -> !testDetectsSolution(t, s)))
                    .collect(Collectors.toSet());
        }

        detected = solutionPool.parallelStream()
                .filter(s -> solutionDetectedByTests(weightMap.getEnabledTests(), s))
                .collect(Collectors.toList());

        /*
        for (GradeerSolution s : solutionPool)
        {
            if(solutionDetectedByTests(weightMap.getEnabledTests(), s))
                detected.add(s);
        }
         */

        return (double) detected.size() / (double) solutionPool.size();
    }

    private boolean solutionDetectedByTests(Collection<UnitTest> tests, GradeerSolution solution)
    {
        for (UnitTest t : tests)
        {
            if(this.containsKey(t))
            {
                if(testDetectsSolution(t, solution))
                    return true;
            }
        }
        return false;
    }

    public boolean solutionDetectedByAnyTests(GradeerSolution solution)
    {
        return solutionDetectedByTests(this.keySet(), solution);
    }

    public Set<GradeerSolution> getAllGradeerSolutions()
    {
        return allGradeerSolutions;
    }

    public String getName()
    {
        return name;
    }

    public TestResultsMap merge(TestResultsMap other)
    {
        TestResultsMap merged = new TestResultsMap(this);
        merged.name = getName() + "_" + other.getName();
        merged.cleanName();

        for (UnitTest t : other.keySet())
        {
            for (GradeerSolution s : other.getAllGradeerSolutions())
            {
                merged.addEntry(t, s, other.testDetectsSolution(t, s));
            }
        }
        return merged;
    }

    public static Collection<TestResultsMap> generateAllPossibleMerges(Collection<TestResultsMap> toMerge)
    {
        Collection<TestResultsMap> found = new HashSet<>();

        for (TestResultsMap c : toMerge)
        {
            Collection<TestResultsMap> next = new HashSet<>(toMerge);
            next.remove(c);
            found.addAll(recursiveMerge(c, next));
        }
        return found;
    }

    private static Collection<TestResultsMap> recursiveMerge(TestResultsMap root, Collection<TestResultsMap> others)
    {
        Collection<TestResultsMap> found = new HashSet<>();

        found.add(root);

        if(others.size() > 0)
        {
            for(TestResultsMap c : others)
            {
                TestResultsMap merged = root.merge(c);
                found.add(merged);
                Collection<TestResultsMap> next = new HashSet<>(others);
                next.remove(c);
                found.addAll(recursiveMerge(merged, next));
            }
        }
        return found;
    }

    /**
     * Make name into a consistent format; sort sub-tokens alphabetically
     */
    public void cleanName()
    {
        List<String> tokens = Arrays.asList(this.getName().split("_"));
        Collections.sort(tokens);

        StringBuilder sb = new StringBuilder();
        Iterator<String> iter = tokens.iterator();
        while (iter.hasNext())
        {
            sb.append(iter.next());
            if (iter.hasNext())
                sb.append("_");
        }

        this.name = sb.toString();
    }

}

