package coverweight.mistakesim;

import coverweight.CoverWeight;
import coverweight.configuration.CLIReader;
import coverweight.configuration.OptionNames;
import coverweight.mistakesim.checkresults.GradeerResultsParser;
import coverweight.mistakesim.checkresults.TestResultsMap;
import coverweight.mistakesim.results.SuitesResultsCSV;
import coverweight.mistakesim.results.SuitesSummaryCSV;
import coverweight.report.HitCounts;
import coverweight.sourcefiles.UnitTest;
import coverweight.suitebias.GrowingWeightmapGenerator;
import coverweight.weighting.GrowthTarget;
import coverweight.weighting.WeightMap;
import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;

public class MistakeSimulationStudy
{
    private static final int REPETITIONS = 100;

    private Logger logger = LoggerFactory.getLogger(getClass());

    public static void main(String[] args)
    {
        CLIReader cli = new CLIReader(args);
        MistakeSimulationStudy mistakeSimulationStudy = new MistakeSimulationStudy(cli);
    }

    public MistakeSimulationStudy(CLIReader cli)
    {
        // CoverWeight instance for model & student solutions; has all possible suite WeightMaps
        CoverWeight cwModel = initialCoverWeight(cli);
        cwModel.setCuts(cwModel.getModelCuts());

        // Execute, get results for model solution
        cwModel.run();

        // Load Gradeer results for student & mutant solutions
        GradeerResultsParser gradeerResultsParser = new GradeerResultsParser(cwModel.getUnitTests());
        try
        {
            logger.info("Parsing Gradeer results...");
            TestResultsMap studentResults = gradeerResultsParser.parse(
                    Paths.get(cli.getInputValue(OptionNames.GRADEER_CSV_STUDENT)));
            Collection<TestResultsMap> mutantResults = gradeerResultsParser.parse(
                    cli.getInputValue(OptionNames.GRADEER_CSV_MUTANTS));

            // Run Probabilistic Coupling
            ProbabilisticCouplingStudy probabilisticCouplingStudy = new ProbabilisticCouplingStudy(
                    cwModel, studentResults, mutantResults
            );
            probabilisticCouplingStudy.run();

            // Basic setup completed , can now begin evaluating generated test suites
            logger.info("Finished execution stage, starting suite generation and result output");
            Collection<TestResultsMap> mutantResultsMerged = TestResultsMap.generateAllPossibleMerges(mutantResults);
            processRepetitions(cwModel, studentResults, mutantResultsMerged);


        } catch (IOException ioException) {
            ioException.printStackTrace();
            System.err.println("Could not load Gradeer solutions");
        }
    }

    private void processRepetitions(CoverWeight cwModel, TestResultsMap studentResults, Collection<TestResultsMap> mutantResults)
    {
        WeightMapGenerator weightMapGenerator = new WeightMapGenerator(cwModel.getUnitTests());

        // Suite growth
        logger.info("Running suite growth analysis...");
        for (int i = 1; i <= REPETITIONS; i++)
        {
            StopWatch stopWatch = StopWatch.createStarted();

            GrowingWeightmapGenerator growingWeightmapGenerator = new GrowingWeightmapGenerator(cwModel,studentResults.keySet(), mutantResults, i);
            Map<GrowthTarget, Collection<WeightMap>> growthTargetCollectionMap = growingWeightmapGenerator.generateForAllGrowthTargets();

            for (GrowthTarget growthTarget : growthTargetCollectionMap.keySet())
            {
                processRepetition("suiteGrowth/" + growthTarget.toString(),
                        growthTargetCollectionMap.get(growthTarget),
                        i, cwModel, studentResults, mutantResults);
            }
            logger.info("Finished suite growth analysis (" + i + ") in " + stopWatch.toString());
        }

        // Fixed size (70%)
        logger.info("Running controlled suite size analysis...");
        for(int i = 1; i <= REPETITIONS; i++)
        {
            StopWatch stopWatch = StopWatch.createStarted();

            // Generate suites
            Set<WeightMap> suites = weightMapGenerator.generateWeightMapSetSizeControlled(i);

            // Run analysis
            processRepetition("controlledSuiteSize", suites, i, cwModel, studentResults, mutantResults);

            logger.info("Finished analysis in " + stopWatch.toString());
        }

        logger.info("Running random suite size analysis...");
        // Random size (2 -> n-1)
        for(int r = 1; r <= REPETITIONS; r++)
        {
            StopWatch stopWatch = StopWatch.createStarted();

            // Generate suites
            Set<WeightMap> suites = weightMapGenerator.generateWeightMapSetRandomSize(r);

            // Run analysis
            processRepetition("randomSuiteSize", suites, r, cwModel, studentResults, mutantResults);

            logger.info("Finished analysis in " + stopWatch.toString());
        }


    }

    private void processRepetition(String exptGroupName, Collection<WeightMap> suites, int repetition, CoverWeight cwModel,
                                   TestResultsMap studentResults, Collection<TestResultsMap> mutantResults)
    {
        // Set suites
        List<WeightMap> suiteList = new ArrayList<>(suites);
        cwModel.setWeightMaps(suiteList);

        // Execute, get results
        Collection<HitCounts> modelHCs = cwModel.run();

        // Generate results of suite : coverage level, fault detection rate & mutation scores
        SuitesResultsCSV suitesResultsCSV = new SuitesResultsCSV(exptGroupName, suiteList, repetition, modelHCs,
                                                                    studentResults, mutantResults);
        suitesResultsCSV.write();
        // CSV of all weightmaps : Number of enabled tests, which tests enabled
        SuitesSummaryCSV suitesSummaryCSV = new SuitesSummaryCSV(exptGroupName, suiteList, repetition);
        suitesSummaryCSV.write();
    }


    /**
     * Recursive function to generate variants of a test suite.
     * Takes current WeightMap, then for each enabled test, recursively calls with that test disabled.
     * @param current Current WeightMap
     * @param collected WeightMaps collected so far for recursive branch
     * @return collected WeightMaps for sub branches
     */
    private Set<WeightMap> generateTestSuiteVariants(WeightMap current, Set<WeightMap> collected)
    {
        final double PROPORTION_ENABLED = 0.6;

        // Add current
        collected.add(current);

        // Base case: must contain at least one test
        if(current.getEnabledTests().size() <= 1)
            return collected;

        // Conditional case: must contain at least proportion of available tests
        if(current.getEnabledTests().size() <= Math.round(current.size() * PROPORTION_ENABLED))
            return collected;

        // Recursive step: remove a test
        for (UnitTest t : current.getEnabledTests())
        {
            WeightMap w = new WeightMap(current);
            w.put(t, 0.0);

            // Can skip branch if collected already contains the element; the branch has already been explored
            if(!collected.contains(w))
                collected.addAll(generateTestSuiteVariants(w, collected));
        }

        return collected;
    }

    private CoverWeight initialCoverWeight(CLIReader cli)
    {
        // Load a CoverWeight instance with standard args
        CoverWeight coverWeight = new CoverWeight(cli, new HashSet<>());

        // If in quick-run mode, reduce number of tests and CUTs now.
        //applyQuickRunMode(coverWeight, cli);

        // Make default WeightMap not normalized
        WeightMap wm = new WeightMap(coverWeight.getUnitTests());
        List<WeightMap> wmList = new ArrayList<>();
        wmList.add(wm);
        coverWeight.setWeightMaps(wmList);


        return coverWeight;
    }
}
