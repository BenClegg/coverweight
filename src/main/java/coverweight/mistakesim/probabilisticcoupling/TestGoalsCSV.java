package coverweight.mistakesim.probabilisticcoupling;

import coverweight.mistakesim.checkresults.GradeerSolution;
import coverweight.mistakesim.checkresults.TestResultsMap;
import coverweight.output.csv.CSVMaker;
import coverweight.report.HitCounts;
import coverweight.sourcefiles.UnitTest;
import coverweight.weighting.WeightMap;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class TestGoalsCSV extends CSVMaker
{
    private List<HitCounts> modelHitCounts;
    private Collection<WeightMap> individualTestWeightMaps;
    private Collection<UnitTest> tests;
    private final Collection<Integer> lineCoverageGoals;
    private Collection<TestResultsMap> perToolMutantResults;

    public TestGoalsCSV(List<HitCounts> modelHitCounts,
                        Collection<WeightMap> individualTestWeightMaps,
                        Collection<Integer> lineCoverageGoals,
                        Collection<TestResultsMap> perToolMutantResults)
    {
        super("probabilisticCoupling", "achievedTestGoals");
        this.modelHitCounts = modelHitCounts;
        this.individualTestWeightMaps = individualTestWeightMaps;
        this.lineCoverageGoals = lineCoverageGoals;
        this.perToolMutantResults = perToolMutantResults;

        this.tests = individualTestWeightMaps.stream()
                .filter(w -> w.getEnabledTests().size() == 1)
                .map(w -> w.getEnabledTests().iterator().next())
                .collect(Collectors.toSet());

        setHeader();
        generateLines();
    }

    @Override
    protected void setHeader()
    {

        List<String> tokens = new ArrayList<>();

        // Test Goal
        tokens.add("TestGoal");
        // Each test
        for (UnitTest t : tests)
            tokens.add(t.getIdentifer());

        lines.add(generateLine(tokens));
    }

    @Override
    public void generateLines()
    {
        // Line Coverage Goals
        for (int l : lineCoverageGoals)
        {

            List<String> tokens = new ArrayList<>();

            // Test Goal
            tokens.add("CoveredLine_" + l);
            // Each test
            for (UnitTest t : tests)
                tokens.add(modelHitCounts.stream()
                    .filter(hc -> individualTestWeightMaps.contains(hc.getWeightMap()))
                    .filter(hc -> hc.getWeightMap().getEnabledTests().contains(t))
                    .map(hc -> Boolean.toString(hc.getCoveredEnabledLines().contains(l)))
                    .findFirst().orElse("Error")
                );

            lines.add(generateLine(tokens));

        }

        // Mutant Goals
        for (TestResultsMap singleToolMutantResults : perToolMutantResults)
        {
            for (GradeerSolution m : singleToolMutantResults.getAllGradeerSolutions())
            {
                List<String> tokens = new ArrayList<>();
                // Test Goal
                tokens.add(singleToolMutantResults.getName() + "_" + m.getName());
                // Each test
                for (UnitTest t : tests)
                    tokens.add(Boolean.toString(singleToolMutantResults.testDetectsSolution(t, m)));

                lines.add(generateLine(tokens));

            }
        }
    }

}
