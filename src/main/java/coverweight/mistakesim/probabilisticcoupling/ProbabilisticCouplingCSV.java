package coverweight.mistakesim.probabilisticcoupling;

import coverweight.mistakesim.checkresults.GradeerSolution;
import coverweight.mistakesim.checkresults.TestResultsMap;
import coverweight.output.csv.CSVMaker;
import coverweight.report.HitCounts;
import coverweight.sourcefiles.ClassUnderTest;
import coverweight.sourcefiles.UnitTest;
import coverweight.weighting.WeightMap;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class ProbabilisticCouplingCSV extends CSVMaker
{
    private List<HitCounts> modelHitCounts;
    private Collection<WeightMap> individualTestWeightMaps;
    private final Collection<Integer> lineCoverageGoals;
    private Collection<TestResultsMap> perToolMutantResults;
    private TestResultsMap faultySolutionResults;

    public ProbabilisticCouplingCSV(List<HitCounts> modelHitCounts,
                                    Collection<WeightMap> individualTestWeightMaps,
                                    Collection<Integer> lineCoverageGoals,
                                    Collection<TestResultsMap> perToolMutantResults,
                                    TestResultsMap faultySolutionResults)
    {
        super("probabilisticCoupling", "probCoupResults");
        this.modelHitCounts = modelHitCounts;
        this.individualTestWeightMaps = individualTestWeightMaps;
        this.lineCoverageGoals = lineCoverageGoals;
        this.perToolMutantResults = perToolMutantResults;
        this.faultySolutionResults = faultySolutionResults;

        // Create file
        setHeader();
        generateLines();
    }

    @Override
    protected void setHeader()
    {
        List<String> tokens = new ArrayList<>();

        // Test Goal
        tokens.add("TestGoal");
        // Real Fault (i.e. student's solution)
        tokens.add("FaultySolution");
        // Probability
        tokens.add("DetectionProbability");

        lines.add(generateLine(tokens));
    }

    @Override
    public void generateLines()
    {
        Collection<GradeerSolution> faultySolutions = faultySolutionResults.getAllGradeerSolutions().stream()
                .filter(s -> faultySolutionResults.solutionDetectedByAnyTests(s))
                .collect(Collectors.toSet());

        // Line Coverage Goals
        for (int l : lineCoverageGoals)
        {
            for (GradeerSolution s : faultySolutions)
            {
                List<String> tokens = new ArrayList<>();

                // Test Goal
                tokens.add("CoveredLine_" + l);
                // Real Fault (i.e. student's solution)
                tokens.add(s.getName());
                // Probability
                tokens.add(String.valueOf(detectionProbabilityLineCoverage(s, l)));

                lines.add(generateLine(tokens));
            }
        }

        // TODO Mutant Goals
        for (TestResultsMap singleToolMutantResults : perToolMutantResults)
        {
            for (GradeerSolution m : singleToolMutantResults.getAllGradeerSolutions())
            {
                for (GradeerSolution s : faultySolutions)
                {
                    List<String> tokens = new ArrayList<>();

                    // Test Goal
                    tokens.add(singleToolMutantResults.getName() + "_" + m.getName());
                    // Real Fault (i.e. student's solution)
                    tokens.add(s.getName());
                    // Probability
                    tokens.add(String.valueOf(detectionProbabilityMutantDetection(s, m, singleToolMutantResults)));

                    lines.add(generateLine(tokens));
                }
            }
        }


    }

    private double detectionProbabilityMutantDetection(GradeerSolution faultySolution, GradeerSolution mutant, TestResultsMap mutantResults)
    {
        Collection<UnitTest> tests = individualTestWeightMaps.stream()
                .filter(w -> w.getEnabledTests().size() == 1)
                .map(w -> w.getEnabledTests().iterator().next())
                .filter(t -> mutantResults.testDetectsSolution(t, mutant)) // Only use tests that detect mutant
                .collect(Collectors.toSet());

        // No tests that detect / cover the test goal; probability is 0
        if(tests.isEmpty())
            return 0.0;

        long detectingTests = tests.stream()
                .map(t -> faultySolutionResults.get(t))
                .filter(map -> map.get(faultySolution))
                .count();

        return (double) detectingTests / tests.size();
    }

    private double detectionProbabilityLineCoverage(GradeerSolution faultySolution, int line)
    {
        Collection<WeightMap> applicableWeightMaps = modelHitCounts.stream()
                .filter(hc -> individualTestWeightMaps.contains(hc.getWeightMap()))
                .filter(hc -> hc.getCoveredEnabledLines().contains(line)) // Only use tests that cover the line
                .map(HitCounts::getWeightMap)
                .collect(Collectors.toSet());

        Collection<UnitTest> tests = applicableWeightMaps.stream()
                .filter(w -> w.getEnabledTests().size() == 1)
                .map(w -> w.getEnabledTests().iterator().next())
                .collect(Collectors.toSet());

        // No tests that detect / cover the test goal; probability is 0
        if(tests.isEmpty())
            return 0.0;

        long detectingTests = tests.stream()
                .map(t -> faultySolutionResults.get(t))
                .filter(m -> m.get(faultySolution))
                .count();

        return (double) detectingTests / tests.size();
    }
}
