package coverweight.mistakesim.results;

import coverweight.output.csv.CSVMaker;
import coverweight.report.HitCounts;
import coverweight.sourcefiles.UnitTest;
import coverweight.weighting.WeightMap;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

public class SuitesSummaryCSV extends CSVMaker
{

    private final Collection<WeightMap> suites;
    private final Collection<UnitTest> tests;


    public SuitesSummaryCSV(String exptGroupName,
                            Collection<WeightMap> suites,
                            int repetition)
    {
        super("suiteAnalysis" + "-" + exptGroupName + File.separator + repetition,
                "suitesSummary");
        this.suites = suites;
        this.tests = suites.stream().findFirst().get().keySet();

        setHeader();
        generateLines();
    }


    @Override
    protected void setHeader()
    {
        List<String> tokens = new ArrayList<>();

        // Name
        tokens.add("WeightMap");
        // Number Tests
        tokens.add("Number Enabled Tests");
        // Enabled Tests
        for (UnitTest t : tests)
            tokens.add(t.getBaseName());

        lines.add(generateLine(tokens));
    }

    @Override
    public void generateLines()
    {
        for (WeightMap w : suites)
        {
            List<String> tokens = new ArrayList<>();

            // Name
            tokens.add(w.getName());
            // Number Tests
            tokens.add(String.valueOf(w.getEnabledTests().size()));
            // Enabled Tests
            for (UnitTest t : tests)
                tokens.add(String.valueOf(w.get(t)));

            lines.add(generateLine(tokens));

        }
    }


}

