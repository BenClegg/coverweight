package coverweight.mistakesim.results;

import coverweight.mistakesim.checkresults.TestResultsMap;
import coverweight.output.csv.CSVMaker;
import coverweight.report.HitCounts;
import coverweight.weighting.WeightMap;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

public class SuitesResultsCSV extends CSVMaker
{

    private final String exptGroupName;
    private final Collection<WeightMap> suites;
    private final int repetition;
    private final Collection<HitCounts> modelHCs;
    private final TestResultsMap studentResults;
    private final Collection<TestResultsMap> mutantResults;

    public SuitesResultsCSV(String exptGroupName,
                            Collection<WeightMap> suites,
                            int repetition,
                            Collection<HitCounts> modelHCs,
                            TestResultsMap studentResults,
                            Collection<TestResultsMap> mutantResults)
    {
        super("suiteAnalysis" + "-" + exptGroupName + File.separator + repetition,
                "suitesResults");
        this.exptGroupName = exptGroupName;
        this.suites = suites;
        this.repetition = repetition;
        this.modelHCs = modelHCs;
        this.studentResults = studentResults;
        this.mutantResults = mutantResults;

        setHeader();
        generateLines();
    }


    @Override
    protected void setHeader()
    {
        List<String> tokens = new ArrayList<>();

        // Name
        tokens.add("WeightMap");
        // Number Tests
        tokens.add("Number Enabled Tests");
        // Proportion Enabled Tests
        tokens.add("Proportion Enabled Tests");
        // Coverage Level
        tokens.add("Line Coverage Ratio (Model)");
        // Fault Detection Rate (Students)
        tokens.add("Real Fault Detection Rate");
        // Mutation Score (Per Tool)
        for (TestResultsMap mrm : mutantResults)
            tokens.add(mrm.getName());

        lines.add(generateLine(tokens));
    }

    @Override
    public void generateLines()
    {
        for (WeightMap w : suites)
        {

            List<String> tokens = new ArrayList<>();

            // Name
            tokens.add(w.getName());

            // Number Tests
            tokens.add(String.valueOf(w.getEnabledTests().size()));

            // Proportion Enabled Tests
            tokens.add(String.valueOf((double) w.getEnabledTests().size() / (double) mutantResults.stream().findFirst().get().keySet().size()));

            // Coverage Level for model
            Optional<HitCounts> modelHC = modelHCs.stream().filter(hc -> hc.getWeightMap().equals(w)).findFirst();
            if(modelHC.isPresent())
                tokens.add(String.valueOf(modelHC.get().coveredLineRatio()));
            else
                tokens.add("NULL");

            // Fault Detection Rate (Students)
            tokens.add(String.valueOf(studentResults.faultDetectionRate(w, true)));

            // Mutation Score (Per Tool)
            for (TestResultsMap mrm : mutantResults)
                tokens.add(String.valueOf(mrm.faultDetectionRate(w, false)));

            // Store line
            lines.add(generateLine(tokens));
        }
    }

    private double faultDetectionRate(Collection<HitCounts> hitCountsCollection, WeightMap weightMap)
    {
        Collection<HitCounts> matching = hitCountsCollection.stream()
                .filter(hc -> hc.getWeightMap().equals(weightMap))
                .collect(Collectors.toSet());
        return matching.stream()
                .filter(hc -> !hc.allEnabledTestsPass())
                .count()
                / (double) matching.size();
    }

}

