package coverweight.mistakesim;

import coverweight.CoverWeight;
import coverweight.mistakesim.checkresults.GradeerSolution;
import coverweight.mistakesim.checkresults.TestResultsMap;
import coverweight.mistakesim.probabilisticcoupling.ProbabilisticCouplingCSV;
import coverweight.mistakesim.probabilisticcoupling.TestGoalsCSV;
import coverweight.report.HitCounts;
import coverweight.sourcefiles.ClassUnderTest;
import coverweight.weighting.WeightMap;

import java.util.*;
import java.util.stream.Collectors;

public class ProbabilisticCouplingStudy
{
    CoverWeight cwModel;
    TestResultsMap studentResults;
    Collection<TestResultsMap> perToolMutantResults;
    WeightMap allTestsWeightMap;
    Collection<WeightMap> individualTestWeightMaps = new HashSet<>();

    public ProbabilisticCouplingStudy(CoverWeight cwModel,
                                      TestResultsMap studentResults,
                                      Collection<TestResultsMap> perToolMutantResults)
    {
        this.cwModel = cwModel;
        this.studentResults = studentResults;
        this.perToolMutantResults = perToolMutantResults;

        // Make WeightMaps
        this.allTestsWeightMap = new WeightMap(cwModel.getUnitTests());
        this.individualTestWeightMaps.addAll(
                cwModel.getUnitTests().stream()
                        .map(test ->
                        {
                            WeightMap w = new WeightMap(cwModel.getUnitTests());
                            w.replaceAll((t,v) -> 0.0);
                            w.put(test, 1.0);
                            return w;
                        })
                        .collect(Collectors.toSet())
        );
    }

    public void run()
    {
        // Populate HitCounts
        List<WeightMap> allWeightMaps = new ArrayList<>();
        allWeightMaps.add(this.allTestsWeightMap);
        allWeightMaps.addAll(this.individualTestWeightMaps);
        cwModel.setWeightMaps(allWeightMaps);
        List<HitCounts> modelHitCounts = cwModel.run();

        // Construct test goals - all lines
        Collection<Integer> lineCoverageGoals = cwModel.getHitCounts(cwModel.getModelCuts().get(0), allTestsWeightMap)
                .keySet();

        //Collection<GradeerSolution> mutantGoals = new HashSet<>();
        //for (TestResultsMap trm : perToolMutantResults)
            //mutantGoals.addAll(trm.getAllGradeerSolutions());

        // Store test goal results for debugging
        TestGoalsCSV testGoalsCSV = new TestGoalsCSV(
                modelHitCounts,
                individualTestWeightMaps,
                lineCoverageGoals,
                perToolMutantResults
        );
        testGoalsCSV.write();

        // Perform probabilistic coupling analysis (use file writing class)
        ProbabilisticCouplingCSV probabilisticCouplingCSV = new ProbabilisticCouplingCSV(
                modelHitCounts,
                individualTestWeightMaps,
                lineCoverageGoals,
                perToolMutantResults,
                studentResults
        );
        probabilisticCouplingCSV.write();
    }

}
