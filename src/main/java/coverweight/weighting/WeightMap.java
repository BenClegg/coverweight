package coverweight.weighting;

import coverweight.sourcefiles.UnitTest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class WeightMap extends ConcurrentHashMap<UnitTest, Double>
{
    private static final Logger logger = LoggerFactory.getLogger(WeightMap.class);
    private double totalWeights = Double.NaN;

    private GrowthTarget growthTarget = null;

    private String name;

    /*
    public WeightMap(Map<UnitTest, Double> unitTestToWeightMap)
    {
        super();
        StringBuilder n = new StringBuilder();

        Iterator<UnitTest> testIterator = unitTestToWeightMap.keySet().iterator();

        while (testIterator.hasNext())
        {
            UnitTest t = testIterator.next();
            this.put(t, unitTestToWeightMap.get(t));
        }
        //name = n.toString();
        name = String.valueOf(this.hashCode());
        update();
    }
    */

    public WeightMap(WeightMap existing)
    {
        super();
        StringBuilder n = new StringBuilder();

        Iterator<UnitTest> testIterator = existing.keySet().iterator();

        while (testIterator.hasNext())
        {
            UnitTest t = testIterator.next();
            this.put(t, existing.get(t));
        }
        //name = n.toString();
        name = String.valueOf(this.hashCode());

        this.totalWeights = existing.totalWeights;
        this.growthTarget = existing.growthTarget;
        update();

    }

    /**
     * Generate a map of unit tests to weights from a provided .csv
     * @param weightsCsv
     * @param availableTests
     */
    public WeightMap(Path weightsCsv, Collection<UnitTest> availableTests)
    {
        super();
        name = weightsCsv.getFileName().toString();

        List<String> lines;

        // Load .csv
        try
        {
            lines = Files.readAllLines(weightsCsv);
        } catch (IOException e)
        {
            logger.error(e.getMessage());
            return;
        }

        // Load weights
        for (String l : lines)
        {
            String[] split = l.split(",");
            // Should be 2 elements, exit if there isn't
            if(split.length != 2)
                return;

            availableTests.forEach(t ->
            {
                if (t.getBaseName().equals(split[0]))
                {
                    double w = Double.parseDouble(split[1]);
                    put(t, w);
                }
            });
        }

        // Ensure every test is at least present in the map, but with a weight of 0
        for (UnitTest t : availableTests)
        {
            if (!containsKey(t))
                put(t, 0.0);
        }
        update();
    }

    /**
     * Create a naive weight map, where all weights are equivalent (i.e. weightless)
     * @param availableTests
     */
    public WeightMap(Collection<UnitTest> availableTests)
    {
        super();
        name = "NaiveAuto";
        for (UnitTest t : availableTests)
            put(t, 1.0);
        update();
    }

    /**
     * Create an unbalanced weightmap, all weights except for that of the test to inflate are 1.
     * The test to inflate has a weight of factor
     * Note: this will likely still need normalizing
     * @param availableTests
     * @param testToInflate
     * @param factor
     */
    public WeightMap(Collection<UnitTest> availableTests, UnitTest testToInflate, int factor, double totalFactors)
    {
        super();
        name = "Unbalanced-" + testToInflate.getBaseName() + String.format("%06d", factor);
        for (UnitTest t : availableTests)
        {
            if(t.equals(testToInflate))
                put(t, (double) factor);
            else
                put(t, totalFactors / 2);
        }
    }

    public void enableTest(UnitTest toEnable)
    {
        put(toEnable, 1.0);
        update();
    }

    public void disableAllTests()
    {
        this.replaceAll((t, v) -> 0.0);
        update();
    }

    public void onlyEnableTests(Collection<UnitTest> toEnable)
    {
        this.replaceAll((t, v) -> 0.0);
        toEnable.forEach(t -> put(t, 1.0));
        update();
    }

    public void update()
    {
        this.setName(String.valueOf(hashCode()));
        this.updateTotalWeights();// Required to initially store total weights
    }

    public void setGrowthTarget(GrowthTarget growthTarget)
    {
        this.growthTarget = growthTarget;
    }

    public String getGrowthTargetDetails()
    {
        if(growthTarget == null)
            return "NA";

        return growthTarget.toString();
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();

        for (UnitTest t : keySet())
        {
            sb.append(t.getBaseName() + "=" + get(t));
            sb.append(", ");
        }
        if(sb.length() > 2)
            sb.replace(sb.length() - 2, sb.length(), "");

        return "WeightMap-" + name + "{" + sb.toString() + "}";
    }

    public WeightMap(String mapName)
    {
        super();
        name = mapName;
    }

    /**
     * Generate a normalized copy of this weight set
     * This is performed by dividing all weights by the greatest weight
     * @return normalised WeightMap
     */
    public WeightMap generateNormalized()
    {
        double greatest = 0;
        for (UnitTest t : this.keySet())
        {
            double w = get(t);
            greatest += w;
            /*
            if(w > greatest)
                greatest = w;
                */
        }

        if(greatest == 0)
        {
            return this;
        }

        WeightMap normalized = new WeightMap("Normalized-" + this.getName());
        for (UnitTest t : this.keySet())
        {
            double w = get(t) / greatest;
            normalized.put(t, w);
        }

        return normalized;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * Check if all of the weights can be used
     * @return false if any weight is NaN or Infinity, true otherwise
     */
    public boolean allWeightsValid()
    {
        for (double w : values())
        {
            if(!Double.isFinite(w))
                return false;
        }
        return true;
    }

    public boolean allWeightsGreaterThanZero()
    {
        for (double w : values())
        {
            if(w <= 0)
                return false;
        }
        return true;
    }

    private void updateTotalWeights()
    {
        totalWeights = values().stream().mapToDouble(v -> v).sum();
    }

    public double getTotalWeights()
    {
        if(Double.isNaN(totalWeights))
            updateTotalWeights();
        return totalWeights;
    }

    public Collection<UnitTest> getEnabledTests()
    {
        return this.keySet().stream().filter(t -> this.get(t) > 0).collect(Collectors.toSet());
    }

    public double getProportionTestsEnabled()
    {
        return (double) getEnabledTests().size() / (double) this.keySet().size();
    }

    /*
    @Override
    public int hashCode()
    {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object o)
    {
        if(o == null)
            return false;
        if (!super.equals(o))
            return false;

        WeightMap other = (WeightMap) o;
        for (UnitTest t : this.keySet())
        {
            if(!other.containsKey(t))
                return false;
            if(!other.get(t).equals(this.get(t)))
                return false;
        }
        return true;
    }
    */
}
