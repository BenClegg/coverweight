package coverweight.weighting;

import coverweight.report.HitCounts;
import coverweight.runner.TestResult;
import coverweight.sourcefiles.ClassUnderTest;

import java.util.Collection;

public class WeightedCoverageGenerator
{
    private Collection<TestResult> testResults;
    private WeightMap weightMap;

    public WeightedCoverageGenerator(WeightMap weightMapInstance, Collection<TestResult> allTestResults)
    {
        testResults = allTestResults;
        weightMap = weightMapInstance;
    }

    public HitCounts generateWeightedResults(ClassUnderTest classUnderTest)
    {
        HitCounts hitCounts = new HitCounts(classUnderTest, weightMap, testResults);

        return hitCounts;
    }
}
