package coverweight.weighting;

import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;

public class GrowthTarget
{
    public enum Criterion
    {
        COVERAGE,
        MUTATION,
        RANDOM,
        RANDOM_PRESET
    }

    public enum PostSaturationMode
    {
        NONE,
        STACK,
        SWITCH_CRITERION
    }

    private final Criterion primaryCriterion;
    private final PostSaturationMode postSaturationMode;
    private final String mutationTool;
    private Collection<WeightMap> adequateSuites;

    public static final String NO_MUTANTS = "NO_MUTANTS";

    public GrowthTarget(Criterion primaryCriterion, PostSaturationMode postSaturationMode, String mutationTool)
    {
        this.primaryCriterion = primaryCriterion;
        this.postSaturationMode = postSaturationMode;
        this.mutationTool = mutationTool;
        this.adequateSuites = new HashSet<>();
    }

    public Criterion getCurrentCriterion()
    {
        if (adequateSuites.isEmpty())
            return primaryCriterion;

        if (postSaturationMode.equals(PostSaturationMode.SWITCH_CRITERION))
        {
            if (primaryCriterion.equals(Criterion.COVERAGE))
                return Criterion.MUTATION;
            return Criterion.COVERAGE;
        }

        // No criterion switching, return the standard criterion
        return primaryCriterion;
    }

    public boolean growthComplete()
    {
        switch (postSaturationMode)
        {
            case NONE:
                return !getAdequateSuites().isEmpty();
            case SWITCH_CRITERION:
                return getAdequateSuites().size() > 1; // Criterion switch exhausts both criteria
            case STACK:
                return false; // Only ends when no tests remain
        }
        return false;
    }

    public void addAdequateSuite(WeightMap adequateSuite)
    {
        adequateSuites.add(adequateSuite);
    }

    public Collection<WeightMap> getAdequateSuites()
    {
        return adequateSuites;
    }

    public Criterion getPrimaryCriterion()
    {
        return primaryCriterion;
    }

    public PostSaturationMode getPostSaturationMode()
    {
        return postSaturationMode;
    }

    public String getMutationTool()
    {
        return mutationTool;
    }

    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append(getPrimaryCriterion().name());

        if(!getPostSaturationMode().equals(PostSaturationMode.NONE))
        {
            sb.append("_");
            sb.append(getPostSaturationMode().name());
        }

        if(!getMutationTool().equals(NO_MUTANTS))
        {
            sb.append("_");
            sb.append(getMutationTool());
        }

        return sb.toString();
    }


    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GrowthTarget that = (GrowthTarget) o;
        return primaryCriterion == that.primaryCriterion && getPostSaturationMode() == that.getPostSaturationMode() && Objects.equals(getMutationTool(), that.getMutationTool());
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(primaryCriterion, getPostSaturationMode(), getMutationTool());
    }
}
