package coverweight.experiments.output;

import coverweight.CoverWeight;
import coverweight.output.csv.CSVMaker;
import coverweight.report.HitCounts;
import coverweight.sourcefiles.ClassUnderTest;
import coverweight.weighting.WeightMap;

import java.util.*;

public abstract class HitCountsVisitingCSV extends CSVMaker
{
    List<HitCounts> hitCountsList;
    CoverWeight cw;
    List<ClassUnderTest> cuts;

    public HitCountsVisitingCSV(String exptName,
                                String fileName,
                                List<HitCounts> hitCounts,
                                CoverWeight coverWeight)
    {
        super(exptName, fileName);
        hitCountsList = hitCounts;
        cw = coverWeight;
        cuts = cw.getCuts();
        System.out.println(cw);
        System.out.println(cw.getCuts());
        System.out.println(cuts);
        //cuts = new LinkedList<>(cw.getCuts());
        // TODO may need to change this to be a general identifier
        //cuts.sort(Comparator.comparing(Source::getRelativeDir));

        setHeader();
    }

    @Override
    protected void setHeader()
    {
        List<String> columns = new ArrayList<>();
        columns.add("WeightMap");
        for (ClassUnderTest c : cuts)
        {
            columns.add(c.getIdentifer());
        }
        lines.add(generateLine(columns));
    }

    @Override
    public void generateLines()
    {
        LinkedList<WeightMap> sortedWeightMaps = new LinkedList<>(cw.getWeightMaps());
        sortedWeightMaps.sort(Comparator.comparing(WeightMap::getName));

        for (WeightMap w : sortedWeightMaps)
        {
            List<String> cells = new ArrayList<>();
            cells.add(w.getName());

            // TODO optimize for speed with 3d hashmap?
            for (ClassUnderTest c : cuts)
            {
                try
                {
                    HitCounts hitCounts = hitCountsList.stream().filter(h ->
                            (h.getWeightMap().equals(w) && h.getClassUnderTest().equals(c))
                    ).findFirst().get();
                    cells.add(cellValue(hitCounts));
                }
                catch (NoSuchElementException e)
                {
                    e.printStackTrace();
                    cells.add("noHitCountMatches");
                }
            }

            lines.add(generateLine(cells));
        }
    }

    public abstract String cellValue(HitCounts hc);
}