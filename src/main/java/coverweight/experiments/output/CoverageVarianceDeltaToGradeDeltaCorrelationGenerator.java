package coverweight.experiments.output;

import coverweight.CoverWeight;
import coverweight.experiments.WeightModificationExperiment;
import coverweight.output.csv.CSVMaker;
import coverweight.output.csv.ManualCSVMaker;
import coverweight.report.HitCounts;
import coverweight.sourcefiles.ClassUnderTest;
import coverweight.weighting.WeightMap;
import org.apache.commons.math3.stat.correlation.SpearmansCorrelation;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class CoverageVarianceDeltaToGradeDeltaCorrelationGenerator
{
    private CSVMaker covVarCorrelation;

    private CoverWeight cw;
    private CoverWeight balanced;

    public CoverageVarianceDeltaToGradeDeltaCorrelationGenerator(String exptName,
                                                                 String inflatedTestName,
                                                                 CoverWeight coverWeight,
                                                                 CoverWeight balancedCoverWeight)
    {

        cw = coverWeight;
        balanced = balancedCoverWeight;

        String sharedDir = exptName + File.separator;

        covVarCorrelation = new ManualCSVMaker(sharedDir + "CovVarDeltaToGradeDeltaCorrelation", inflatedTestName)
        {

            @Override
            protected void setHeader()
            {
                List<String> columns = new ArrayList<>();
                columns.add("ClassUnderTest");
                columns.add("Correlation (Cov. Var. Difference of All Lines to Grade Difference)");
                columns.add("Correlation (Cov. Var. Difference of Covered Lines to Grade Difference)");
                addLine(generateLine(columns));
            }
        };

        run();
    }

    @SuppressWarnings("Duplicates")
    private void run()
    {
        for (ClassUnderTest c : cw.getCuts())
        {
            double[] covVarDeltasAll = new double[cw.getWeightMaps().size()];
            double[] covVarDeltasCovered = new double[cw.getWeightMaps().size()];
            double[] gradeDeltas = new double[cw.getWeightMaps().size()];

            int i = 0;
            for (WeightMap w : cw.getWeightMaps())
            {
                HitCounts hitCounts = cw.getHitCounts(c, w);

                // Get matching HitCounts from balanced coverweight instance
                // Should only be one weightmap in balanced (normalized naive)
                HitCounts balancedHC = balanced.getWeightMapToHitCountsMap(c).values().iterator().next();

                gradeDeltas[i] = delta(hitCounts.generateGrade(), balancedHC.generateGrade());

                if(WeightModificationExperiment.COV_BIAS_MODEL_ONLY)
                {
                    HitCounts goldenHC = cw.getHitCounts(cw.getModelCuts().get(0), w);
                    HitCounts balancedGoldenHC = balanced.getWeightMapToHitCountsMap(balanced.getModelCuts().get(0)).values().iterator().next();
                    covVarDeltasAll[i] = delta(goldenHC.getVarianceAll(), balancedGoldenHC.getVarianceAll());
                    covVarDeltasCovered[i] = delta(goldenHC.getVarianceCoveredLines(), balancedGoldenHC.getVarianceCoveredLines());
                }

                i++;
            }

            SpearmansCorrelation spearmansCorrelation = new SpearmansCorrelation();
            double allCorrelation = spearmansCorrelation.correlation(covVarDeltasAll, gradeDeltas);
            double coveredCorrelation = spearmansCorrelation.correlation(covVarDeltasCovered, gradeDeltas);

            List<String> cells = new ArrayList<>(3);
            cells.add(c.getIdentifer());
            cells.add(String.valueOf(allCorrelation));
            cells.add(String.valueOf(coveredCorrelation));

            covVarCorrelation.addLine(ManualCSVMaker.generateLine(cells));
        }
    }

    public void write()
    {
        covVarCorrelation.write();
    }

    private double delta(double a, double b)
    {
        if (WeightModificationExperiment.ABSOLUTE_DELTA)
            return Math.abs(a - b);
        return a - b;
    }
}
