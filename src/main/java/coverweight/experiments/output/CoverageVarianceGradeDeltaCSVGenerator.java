package coverweight.experiments.output;

import coverweight.CoverWeight;
import coverweight.experiments.WeightModificationExperiment;
import coverweight.output.csv.CSVMaker;
import coverweight.output.csv.ManualCSVMaker;
import coverweight.report.HitCounts;
import coverweight.sourcefiles.ClassUnderTest;
import coverweight.weighting.WeightMap;
import org.apache.commons.math3.stat.correlation.SpearmansCorrelation;

import java.io.File;
import java.util.*;

public class CoverageVarianceGradeDeltaCSVGenerator
{
    private CSVMaker gradeDeltaCSV;
    private CSVMaker generatedGradesCSV;
    private CSVMaker coveredCovVarCSV;
    private CSVMaker allCovVarCSV;
    private CSVMaker correlationCSV;
    private CSVMaker covVarCoveredToGradeDeltaCSV;
    private CSVMaker covVarAllToGradeDeltaCSV;
    private CSVMaker covVarCoveredDeltaCSV;
    private CSVMaker covVarAllDeltaCSV;

    private CoverWeight cw;
    private CoverWeight balancedCW;

    public CoverageVarianceGradeDeltaCSVGenerator(String exptName,
                                                  String inflatedTestName,
                                                  CoverWeight coverWeight,
                                                  CoverWeight balancedCoverWeight)
    {
        cw = coverWeight;
        balancedCW = balancedCoverWeight;

        String sharedDir = exptName + File.separator + inflatedTestName;

        gradeDeltaCSV = new ManualCSVMaker(sharedDir, "gradeDelta")
        {
            @Override
            protected void setHeader()
            {
                List<String> columns = new ArrayList<>();
                columns.add("WeightMap");
                columns.add("SumOfWeights");
                for (ClassUnderTest c : coverWeight.getCuts())
                {
                    columns.add(c.getIdentifer());
                }
                addLine(generateLine(columns));
            }
        };
        generatedGradesCSV = new ManualCSVMaker(sharedDir, "generatedGrades")
        {
            @Override
            protected void setHeader()
            {
                List<String> columns = new ArrayList<>();
                columns.add("WeightMap");
                columns.add("SumOfWeights");
                for (ClassUnderTest c : coverWeight.getCuts())
                {
                    columns.add(c.getIdentifer());
                }
                addLine(generateLine(columns));
            }
        };

        coveredCovVarCSV = new ManualCSVMaker(sharedDir, "coveredLineCoverageVariance")
        {
            @Override
            protected void setHeader()
            {
                List<String> columns = new ArrayList<>();
                columns.add("WeightMap");
                columns.add("SumOfWeights");
                for (ClassUnderTest c : coverWeight.getCuts())
                {
                    columns.add(c.getIdentifer());
                }
                addLine(generateLine(columns));
            }
        };

        allCovVarCSV = new ManualCSVMaker(sharedDir, "allLineCoverageVariance")
        {
            @Override
            protected void setHeader()
            {
                List<String> columns = new ArrayList<>();
                columns.add("WeightMap");
                columns.add("SumOfWeights");
                for (ClassUnderTest c : coverWeight.getCuts())
                {
                    columns.add(c.getIdentifer());
                }
                addLine(generateLine(columns));
            }
        };
        covVarCoveredDeltaCSV = new ManualCSVMaker(sharedDir, "coveredCovVarDelta")
        {
            @Override
            protected void setHeader()
            {
                List<String> columns = new ArrayList<>();
                columns.add("WeightMap");
                columns.add("SumOfWeights");
                for (ClassUnderTest c : coverWeight.getCuts())
                {
                    columns.add(c.getIdentifer());
                }
                addLine(generateLine(columns));
            }
        };
        covVarAllDeltaCSV = new ManualCSVMaker(sharedDir, "allCovVarDelta")
        {
            @Override
            protected void setHeader()
            {
                List<String> columns = new ArrayList<>();
                columns.add("WeightMap");
                columns.add("SumOfWeights");
                for (ClassUnderTest c : coverWeight.getCuts())
                {
                    columns.add(c.getIdentifer());
                }
                addLine(generateLine(columns));
            }
        };

        correlationCSV = new ManualCSVMaker(sharedDir, "correlation")
        {
            @Override
            protected void setHeader()
            {
                List<String> columns = new ArrayList<>();
                columns.add("WeightMap");
                columns.add("SumOfWeights");
                columns.add("Correlation (Grade Delta to Line Coverage Variance)");
                addLine(generateLine(columns));
            }
        };

        covVarCoveredToGradeDeltaCSV = new ManualCSVMaker(sharedDir, "coveredCovVarToGradeDeltaComparison")
        {
            @Override
            protected void setHeader()
            {
                List<String> columns = new ArrayList<>();
                columns.add("Line Coverage Variance (Covered Lines)");
                columns.add("Grade Delta");
                addLine(generateLine(columns));
            }
        };

        covVarAllToGradeDeltaCSV = new ManualCSVMaker(sharedDir, "allCovVarToGradeDeltaComparison")
        {
            @Override
            protected void setHeader()
            {
                List<String> columns = new ArrayList<>();
                columns.add("Line Coverage Variance (All Lines)");
                columns.add("Grade Delta");
                addLine(generateLine(columns));
            }
        };



        run();
    }

    public void write()
    {
        gradeDeltaCSV.write();
        generatedGradesCSV.write();
        coveredCovVarCSV.write();
        allCovVarCSV.write();
        correlationCSV.write();

        covVarCoveredToGradeDeltaCSV.write();
        covVarAllToGradeDeltaCSV.write();

        covVarCoveredDeltaCSV.write();
        covVarAllDeltaCSV.write();
    }

    private void run()
    {
        LinkedList<WeightMap> sortedWeightMaps = new LinkedList<>(cw.getWeightMaps());
        sortedWeightMaps.sort(Comparator.comparing(WeightMap::getName));

        for (WeightMap w : sortedWeightMaps)
        {
            processWeightmap(w);
        }
    }

    private void processWeightmap(WeightMap weightMap)
    {
        // Init cells of lines for each output
        List<String> gradeDeltaCells = new ArrayList<>(cw.getCuts().size());
        List<String> generatedGradeCells = new ArrayList<>(cw.getCuts().size());
        List<String> covVarAllDeltaCells = new ArrayList<>(cw.getCuts().size());
        List<String> covVarCoveredDeltaCells = new ArrayList<>(cw.getCuts().size());
        List<String> correlationCells = new ArrayList<>(2);

        gradeDeltaCells.add(weightMap.getName());
        generatedGradeCells.add(weightMap.getName());
        covVarAllDeltaCells.add(weightMap.getName());
        covVarCoveredDeltaCells.add(weightMap.getName());
        correlationCells.add(weightMap.getName());

        gradeDeltaCells.add(String.valueOf(weightMap.getTotalWeights()));
        generatedGradeCells.add(String.valueOf(weightMap.getTotalWeights()));
        covVarAllDeltaCells.add(String.valueOf(weightMap.getTotalWeights()));
        covVarCoveredDeltaCells.add(String.valueOf(weightMap.getTotalWeights()));
        correlationCells.add(String.valueOf(weightMap.getTotalWeights()));

        List<String> coveredCovVarCells = (List<String>)((ArrayList<String>) gradeDeltaCells).clone();
        List<String> allCovVarCells = (List<String>)((ArrayList<String>) gradeDeltaCells).clone();

        double[] gradeDeltas = new double[cw.getCuts().size()];
        double[] coveredCoverageVariances = new double[cw.getCuts().size()];
        double[] allCoverageVariances = new double[cw.getCuts().size()];

        int i = 0;

        for (ClassUnderTest c :cw.getCuts())
        {
            try
            {
                HitCounts hitCounts = cw.getHitCounts(c, weightMap);

                // Get matching HitCounts from balanced coverweight instance
                // Should only be one weightmap in balanced (normalized naive)
                HitCounts balanced = balancedCW.getWeightMapToHitCountsMap(c).values().iterator().next();

                gradeDeltas[i] = WeightModificationExperiment.gradeDelta(hitCounts, balanced);

                if(WeightModificationExperiment.COV_BIAS_MODEL_ONLY)
                {
                    HitCounts goldenHC = cw.getHitCounts(cw.getModelCuts().get(0), weightMap);
                    coveredCoverageVariances[i] = hitCounts.getVarianceCoveredLines();
                    allCoverageVariances[i] = hitCounts.getVarianceAll();
                }

                gradeDeltaCells.add(String.valueOf(gradeDeltas[i]));
                generatedGradeCells.add(String.valueOf(hitCounts.generateGrade()));
                coveredCovVarCells.add(String.valueOf(coveredCoverageVariances[i]));
                allCovVarCells.add(String.valueOf(coveredCoverageVariances[i]));

                covVarCoveredToGradeDeltaCSV.addLine(coveredCoverageVariances[i] + ", " + gradeDeltas[i]);
                covVarAllToGradeDeltaCSV.addLine(allCoverageVariances[i] + ", " + gradeDeltas[i]);

                // Coverage variance deltas
                if(WeightModificationExperiment.COV_BIAS_MODEL_ONLY)
                {
                    HitCounts goldenHC = cw.getHitCounts(cw.getModelCuts().get(0), weightMap);
                    HitCounts balancedGoldenHC = balancedCW.getHitCounts(balancedCW.getModelCuts().get(0),
                            balancedCW.getWeightMaps().get(0));
                    covVarAllDeltaCells.add(String.valueOf(delta(goldenHC.getVarianceAll(), balancedGoldenHC.getVarianceAll())));
                    covVarCoveredDeltaCells.add(String.valueOf(delta(goldenHC.getVarianceCoveredLines(), balancedGoldenHC.getVarianceCoveredLines())));

                }
                else
                {
                    covVarAllDeltaCells.add(String.valueOf(delta(hitCounts.getVarianceAll(), balanced.getVarianceAll())));
                    covVarCoveredDeltaCells.add(String.valueOf(delta(hitCounts.getVarianceCoveredLines(), balanced.getVarianceCoveredLines())));
                }
            }
            catch (NoSuchElementException e)
            {
                e.printStackTrace();
                gradeDeltaCells.add("noHitCountMatches");
                generatedGradeCells.add("noHitCountMatches");
                coveredCovVarCells.add("noHitCountMatches");
                allCovVarCells.add("noHitCountMatches");
                covVarCoveredToGradeDeltaCSV.addLine("noHitCountMatches,noHitCountMatches");
                covVarAllToGradeDeltaCSV.addLine("noHitCountMatches,noHitCountMatches");

                gradeDeltas[i] = Double.NaN;
                coveredCoverageVariances[i] = Double.NaN;
            }
            i++;
        }

        SpearmansCorrelation spearmansCorrelation = new SpearmansCorrelation();
        double correlation = spearmansCorrelation.correlation(gradeDeltas, coveredCoverageVariances);
        correlationCells.add(String.valueOf(correlation));

        gradeDeltaCSV.addLine(CSVMaker.generateLine(gradeDeltaCells));
        generatedGradesCSV.addLine(CSVMaker.generateLine(generatedGradeCells));
        coveredCovVarCSV.addLine(CSVMaker.generateLine(coveredCovVarCells));
        allCovVarCSV.addLine(CSVMaker.generateLine(allCovVarCells));
        correlationCSV.addLine(CSVMaker.generateLine(correlationCells));

        covVarAllDeltaCSV.addLine(CSVMaker.generateLine(covVarAllDeltaCells));
        covVarCoveredDeltaCSV.addLine(CSVMaker.generateLine(covVarCoveredDeltaCells));
    }

    private double delta(double a, double b)
    {
        if (WeightModificationExperiment.ABSOLUTE_DELTA)
            return Math.abs(a - b);
        return a - b;
    }
}
