package coverweight.experiments.output;

import coverweight.CoverWeight;
import coverweight.experiments.WeightModificationExperiment;
import coverweight.output.csv.ManualCSVMaker;
import coverweight.report.HitCounts;
import coverweight.sourcefiles.ClassUnderTest;
import coverweight.weighting.WeightMap;
import org.apache.commons.math3.stat.correlation.SpearmansCorrelation;

import java.io.File;
import java.util.*;

public class CovVarGradeDeltaCorrelationAllPermutationsVisitor
{
    private CoverWeight balanced;
    private ManualCSVMaker correlationCSV;

    private Map<ClassUnderTest, List<Double>> covVarsAllMap = new HashMap<>();
    private Map<ClassUnderTest, List<Double>> covVarsCoveredMap = new HashMap<>();
    private Map<ClassUnderTest, List<Double>> gradeDeltasMap = new HashMap<>();

    public CovVarGradeDeltaCorrelationAllPermutationsVisitor(String exptName,
                                                             CoverWeight balancedCoverWeight)
    {
        balanced = balancedCoverWeight;

        String sharedDir = exptName + File.separator;
        correlationCSV = new ManualCSVMaker(sharedDir + "CoverageVarianceToGradeDeltaCorrelation", "_allTests")
        {
            @Override
            protected void setHeader()
            {
                List<String> columns = new ArrayList<>();
                columns.add("ClassUnderTest");
                columns.add("Correlation (Cov. Var. All Lines to Grade Difference)");
                columns.add("Correlation (Cov. Var. Covered Lines to Grade Difference)");
                addLine(generateLine(columns));
            }
        };
    }

    public void visit(CoverWeight unbalancedCoverWeight)
    {
        for (ClassUnderTest c : balanced.getCuts())
        {
            List<Double> gradeDeltas = new ArrayList<>();
            List<Double> covVarsAll = new ArrayList<>();
            List<Double> covVarsCovered = new ArrayList<>();

            for (WeightMap w : unbalancedCoverWeight.getWeightMaps())
            {
                HitCounts hitCounts = unbalancedCoverWeight.getHitCounts(c, w);

                // Get matching HitCounts from balanced coverweight instance
                // Should only be one weightmap in balanced (normalized naive)
                HitCounts balancedHC = balanced.getWeightMapToHitCountsMap(c).values().iterator().next();

                gradeDeltas.add(WeightModificationExperiment.gradeDelta(hitCounts, balancedHC));
                covVarsAll.add(hitCounts.getVarianceAll());
                covVarsCovered.add(hitCounts.getVarianceCoveredLines());

                gradeDeltasMap.put(c, gradeDeltas);
                covVarsAllMap.put(c, covVarsAll);
                covVarsCoveredMap.put(c, covVarsCovered);
            }


        }
    }

    public void write()
    {
        for (ClassUnderTest c : balanced.getCuts())
        {

            double[] gradeDeltas = new double[gradeDeltasMap.get(c).size()];
            double[] covVarsAll = new double[covVarsAllMap.get(c).size()];
            double[] covVarsCovered = new double[covVarsCoveredMap.get(c).size()];

            for (int i = 0; i < gradeDeltas.length; i++)
            {
                gradeDeltas[i] = gradeDeltasMap.get(c).get(i);
            }
            for (int i = 0; i < covVarsAll.length; i++)
            {
                covVarsAll[i] = covVarsCoveredMap.get(c).get(i);
            }
            for (int i = 0; i < covVarsCovered.length; i++)
            {
                covVarsCovered[i] = covVarsCoveredMap.get(c).get(i);
            }


            SpearmansCorrelation spearmansCorrelation = new SpearmansCorrelation();
            double allCorrelation = spearmansCorrelation.correlation(covVarsAll, gradeDeltas);
            double coveredCorrelation = spearmansCorrelation.correlation(covVarsCovered, gradeDeltas);

            List<String> cells = new ArrayList<>();
            cells.add(c.getIdentifer());
            cells.add(String.valueOf(allCorrelation));
            cells.add(String.valueOf(coveredCorrelation));

            correlationCSV.addLine(ManualCSVMaker.generateLine(cells));
        }
        correlationCSV.write();
    }
}
