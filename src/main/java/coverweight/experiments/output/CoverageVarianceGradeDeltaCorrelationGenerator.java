package coverweight.experiments.output;

import coverweight.CoverWeight;
import coverweight.experiments.WeightModificationExperiment;
import coverweight.output.csv.CSVMaker;
import coverweight.output.csv.ManualCSVMaker;
import coverweight.report.HitCounts;
import coverweight.sourcefiles.ClassUnderTest;
import coverweight.weighting.WeightMap;
import org.apache.commons.math3.exception.NotANumberException;
import org.apache.commons.math3.stat.correlation.SpearmansCorrelation;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CoverageVarianceGradeDeltaCorrelationGenerator
{
    private CSVMaker covVarCorrelation;

    private CoverWeight cw;
    private CoverWeight balanced;

    public CoverageVarianceGradeDeltaCorrelationGenerator(String exptName,
                                                          String inflatedTestName,
                                                          CoverWeight coverWeight,
                                                          CoverWeight balancedCoverWeight)
    {

        cw = coverWeight;
        balanced = balancedCoverWeight;

        String sharedDir = exptName + File.separator;

        covVarCorrelation = new ManualCSVMaker(sharedDir + "CoverageVarianceToGradeDeltaCorrelation", inflatedTestName)
        {

            @Override
            protected void setHeader()
            {
                List<String> columns = new ArrayList<>();
                columns.add("ClassUnderTest");
                columns.add("Correlation (Cov. Var. All Lines to Grade Difference)");
                columns.add("Correlation (Cov. Var. Covered Lines to Grade Difference)");
                addLine(generateLine(columns));
            }
        };

        run();
    }

    @SuppressWarnings("Duplicates")
    private void run()
    {
        for (ClassUnderTest c : cw.getCuts())
        {
            double[] covVarsAll = new double[cw.getWeightMaps().size()];
            double[] covVarsCovered = new double[cw.getWeightMaps().size()];
            double[] gradeDeltas = new double[cw.getWeightMaps().size()];

            boolean nanFound = false;
            int i = 0;
            for (WeightMap w : cw.getWeightMaps())
            {
                HitCounts hitCounts = cw.getHitCounts(c, w);

                // Get matching HitCounts from balanced coverweight instance
                // Should only be one weightmap in balanced (normalized naive)
                HitCounts balancedHC = balanced.getWeightMapToHitCountsMap(c).values().iterator().next();

                gradeDeltas[i] = WeightModificationExperiment.gradeDelta(hitCounts, balancedHC);
                covVarsAll[i] = hitCounts.getVarianceAll();
                covVarsCovered[i] = hitCounts.getVarianceCoveredLines();

                i++;
            }

            //System.out.println(Arrays.toString(covVarsCovered));
            SpearmansCorrelation spearmansCorrelation = new SpearmansCorrelation();
            double allCorrelation = spearmansCorrelation.correlation(covVarsAll, gradeDeltas);
            double coveredCorrelation = Double.NaN;
            try
            {
                coveredCorrelation = spearmansCorrelation.correlation(covVarsCovered, gradeDeltas);
            }
            catch (NotANumberException nanEx)
            {
                System.out.println("NaN cov bias correlation for " + c.getIdentifer());
                System.out.println("This is likely due to there being no covered lines");
            }

            List<String> cells = new ArrayList<>(3);
            cells.add(c.getIdentifer());
            cells.add(String.valueOf(allCorrelation));
            cells.add(String.valueOf(coveredCorrelation));

            covVarCorrelation.addLine(ManualCSVMaker.generateLine(cells));
        }
    }

    public void write()
    {
        covVarCorrelation.write();
    }
}
