package coverweight.experiments;

import coverweight.CoverWeight;
import coverweight.configuration.CLIReader;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

public class ExperimentRunner
{
    private CoverWeight originalCoverWeight;

    /**
     * Runs experiments
     * Note: this requires a significant amount of memory to be assigned to the JVM
     * -Xms2g -Xmx8g should work
     * @param args arguments as specified in CLIReader
     */
    public static void main(String[] args)
    {
        ExperimentRunner experimentRunner = new ExperimentRunner(args);
        System.out.println("Running experiments...");
        experimentRunner.runExperiments();
    }

    public ExperimentRunner(String[] args)
    {
        CLIReader cli = new CLIReader(args);
        originalCoverWeight = new CoverWeight(cli, new HashSet<>());
        // run CoverWeight to populate TestResults map, should reduce overhead
        // note: instances inside experiment must be clones
        originalCoverWeight.run();

        System.out.println("Configuring experiments...");
    }

    private void runExperiments()
    {
        // Get all generated grades for all defined WeightMaps
        WeightMapSolutionAnalysisExperiment weightMapSolutionAnalysisExperiment = new WeightMapSolutionAnalysisExperiment(originalCoverWeight);
        weightMapSolutionAnalysisExperiment.run();

        // Test pass/fail results
        TestResultsExperiment testResultsExperiment = new TestResultsExperiment(originalCoverWeight);
        testResultsExperiment.run();

        // Weighting technique results
        WeightTechniqueAnalysisExperiment weightTechniqueAnalysisExperiment = new WeightTechniqueAnalysisExperiment(originalCoverWeight);
        weightTechniqueAnalysisExperiment.run();

        // Basic statistical analysis
        SimpleAnalysisExperiment simpleAnalysisExperiment = new SimpleAnalysisExperiment(originalCoverWeight);
        simpleAnalysisExperiment.run();

        // Randomly generated weights to study correlation to grade difference
        RandomWeightModificationExperiment randomWeightModificationExperiment = new RandomWeightModificationExperiment(originalCoverWeight);
        randomWeightModificationExperiment.run();

        // Should only normalize
        WeightModificationExperiment normalizedWeightModification = new WeightModificationExperiment(originalCoverWeight);
        normalizedWeightModification.setIdentifier("NormalizedWeightModification");
        normalizedWeightModification.run();
    }
}
