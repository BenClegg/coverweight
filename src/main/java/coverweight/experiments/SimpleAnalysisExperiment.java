package coverweight.experiments;

import coverweight.CoverWeight;
import coverweight.output.HitCountsSummary;
import coverweight.output.csv.*;
import coverweight.report.HitCounts;
import coverweight.sourcefiles.ClassUnderTest;
import coverweight.weighting.WeightMap;

import java.io.File;
import java.util.*;

public class SimpleAnalysisExperiment extends Experiment
{

    public SimpleAnalysisExperiment(CoverWeight coverWeightInstance)
    {
        super(coverWeightInstance.clone(), "SimpleAnalysis");

    }

    @Override
    public void run()
    {

        Map<ClassUnderTest, Collection<HitCounts>> cutHitCountsMap = new HashMap<>();

        for (ClassUnderTest c : coverWeight.getCuts())
        {
            Collection<HitCounts> hitCountsCollection = coverWeight.run(c);
            cutHitCountsMap.put(c, hitCountsCollection);

            analyseWeights(c, hitCountsCollection);
            hitCountsCollection.forEach(hc -> {
                HitCountsSummary hcSummary = new HitCountsSummary(hc);
                hcSummary.write();
            });
        }

        for (ClassUnderTest cut : cutHitCountsMap.keySet())
        {
            cutHitCountsMap.get(cut).forEach(h -> System.out.println(h.simpleStatistics()));
            System.out.println(cutHitCountsMap.get(cut));

        }


        if(coverWeight.getModelCuts() != null && !coverWeight.getModelCuts().isEmpty())
        {
            for(ClassUnderTest modelCut : coverWeight.getModelCuts())
            {
                crossAnalyseResults(modelCut, cutHitCountsMap, coverWeight.getWeightMaps());
                System.out.println(modelCut);
            }
        }

        if (cutHitCountsMap.keySet().size() > 1 && coverWeight.getWeightMaps().size() > 1)
        {
            PerWeightCorrelationCSV perWeightCorrelationCSV =
                    new PerWeightCorrelationCSV(cutHitCountsMap, coverWeight.getWeightMaps());
            perWeightCorrelationCSV.write();

            BiasToGeneratedGradeCorrelationCSV biasToGeneratedGradeCorrelationCSV =
                    new BiasToGeneratedGradeCorrelationCSV(coverWeight.getCuts(), cutHitCountsMap, coverWeight.getWeightMaps());
            biasToGeneratedGradeCorrelationCSV.write();

            WeightResultStatsCSV coverageVarianceCoveredLinesStatsCSV
                    = new WeightResultStatsCSV("coverageVarianceCoveredLines", coverWeight)
            {
                @Override
                public double getValue(HitCounts hitCounts)
                {
                    return hitCounts.getVarianceCoveredLines();
                }
            };
            WeightResultStatsCSV coverageVarianceAllLinesStatsCSV
                    = new WeightResultStatsCSV("coverageVarianceAllLines", coverWeight)
            {
                @Override
                public double getValue(HitCounts hitCounts)
                {
                    return hitCounts.getVarianceAll();
                }
            };
            WeightResultStatsCSV gradeStatsCSV
                    = new WeightResultStatsCSV("generatedGrades", coverWeight)
            {
                @Override
                public double getValue(HitCounts hitCounts)
                {
                    return hitCounts.generateGrade();
                }
            };
            coverageVarianceCoveredLinesStatsCSV.write();
            coverageVarianceAllLinesStatsCSV.write();
            gradeStatsCSV.write();

        }

        if (coverWeight.getWeightMaps().size() > 1)
        {
            PerSolutionCorrelationCSV perSolutionLineCovVarCorrelationCSV = new PerSolutionCorrelationCSV(cutHitCountsMap, coverWeight.getWeightMaps()){
                @Override
                public double getValue(HitCounts hitCounts) {
                    return hitCounts.getVarianceCoveredLines();
                }
            };
            perSolutionLineCovVarCorrelationCSV.setLocation("PerSolutionCorrelation" + File.separator +
                    "perSolutionLineCovVarCorrelationCSV.csv");
            perSolutionLineCovVarCorrelationCSV.write();
        }
    }

    private void analyseWeights(ClassUnderTest cut, Collection<HitCounts> hitCountsCollection)
    {
        LinkedList<HitCounts> sortedHitCounts = sortHitCounts(hitCountsCollection);
        PerSolutionAnalysisCSV perSolutionAnalysisCSV = new PerSolutionAnalysisCSV(cut, sortedHitCounts);
        perSolutionAnalysisCSV.write();
    }

    private void crossAnalyseResults(ClassUnderTest modelCut,
                                            Map<ClassUnderTest, Collection<HitCounts>> cutToHitCountsMap,
                                            Collection<WeightMap> weightMaps)
    {
        for(WeightMap wm : weightMaps)
        {
            ModelSolutionComparisonCSV modelSolutionComparisonCSV
                    = new ModelSolutionComparisonCSV(modelCut,
                    cutToHitCountsMap,
                    wm);
            modelSolutionComparisonCSV.write();
        }
    }

    private static LinkedList<HitCounts> sortHitCounts(Collection<HitCounts> hitCounts)
    {
        LinkedList<HitCounts> sortedHitCounts = new LinkedList<>(hitCounts);
        // Remove remaining NaN variances; all weights are 0 for these
        sortedHitCounts.removeIf(hc -> !Double.isFinite(hc.getVarianceCoveredLines()));
        sortedHitCounts.sort(new Comparator<HitCounts>()
        {
            @Override
            public int compare(HitCounts a, HitCounts b)
            {
                double varA = a.getVarianceCoveredLines();
                double varB = b.getVarianceCoveredLines();

                if(varA == varB)
                    return 0;
                if(varA > varB)
                    return -1;
                return 1;
            }
        });
        return sortedHitCounts;
    }

}
