package coverweight.experiments;

import coverweight.CoverWeight;
import coverweight.output.csv.CSVMaker;
import coverweight.report.HitCounts;
import coverweight.sourcefiles.ClassUnderTest;
import coverweight.weighting.WeightMap;

import java.util.ArrayList;

public class WeightTechniqueAnalysisExperiment extends Experiment
{
    public WeightTechniqueAnalysisExperiment(CoverWeight coverWeightInstance)
    {
        super(coverWeightInstance, "WeightTechniqueAnalysis");
    }

    @Override
    public void run()
    {
        PerWeightAnalysisCSV coveredCovVarCSV = new PerWeightAnalysisCSV(identifier, "coverageVarianceCovered", coverWeight){
            @Override
            protected String getValueOfHitCounts(HitCounts hitCounts)
            {
                return String.valueOf(hitCounts.getVarianceCoveredLines());
            }
        };

        PerWeightAnalysisCSV generatedGradesCSV = new PerWeightAnalysisCSV(identifier, "generatedGrades", coverWeight){
            @Override
            protected String getValueOfHitCounts(HitCounts hitCounts)
            {
                return String.valueOf(hitCounts.getVarianceCoveredLines());
            }
        };

        coveredCovVarCSV.write();
        generatedGradesCSV.write();
    }
}

abstract class PerWeightAnalysisCSV extends CSVMaker
{
    protected CoverWeight cw;

    public PerWeightAnalysisCSV(String outputDirName, String baseFileName, CoverWeight coverWeight)
    {
        super(outputDirName, baseFileName);
        cw = coverWeight;
        setHeader();
        generateLines();
    }

    @Override
    protected void setHeader()
    {
        ArrayList<String> columnNames = new ArrayList<>();
        columnNames.add("ClassUnderTest");
        for(WeightMap w : cw.getWeightMaps())
        {
            columnNames.add(w.getName());
        }
        lines.add(generateLine(columnNames));
    }

    @Override
    public void generateLines()
    {
        for(ClassUnderTest c : cw.getCuts())
        {
            ArrayList<String> row = new ArrayList<>();
            row.add(c.getIdentifer());
            for(WeightMap w : cw.getWeightMaps())
            {
                HitCounts h = cw.getHitCounts(c,w);
                row.add(getValueOfHitCounts(h));
            }
            addLine(generateLine(row));
        }
    }

    protected abstract String getValueOfHitCounts(HitCounts hitCounts);
}