package coverweight.experiments;

import coverweight.CoverWeight;

public abstract class Experiment implements Runnable
{
    CoverWeight coverWeight;
    String identifier;

    public Experiment(CoverWeight coverWeightInstance, String experimentName)
    {
        coverWeight = coverWeightInstance;
        identifier = experimentName;
    }

    public String getIdentifier()
    {
        return identifier;
    }

    public void setIdentifier(String identifier)
    {
        this.identifier = identifier;
    }

    public CoverWeight getCoverWeight()
    {
        return coverWeight;
    }
}
