package coverweight.experiments;

import coverweight.CoverWeight;
import coverweight.output.csv.CSVMaker;
import coverweight.output.csv.ManualCSVMaker;
import coverweight.report.HitCounts;
import coverweight.sourcefiles.ClassUnderTest;
import coverweight.weighting.WeightMap;

import java.util.ArrayList;

public class WeightMapSolutionAnalysisExperiment extends Experiment
{
    public WeightMapSolutionAnalysisExperiment(CoverWeight coverWeightInstance)
    {
        super(coverWeightInstance.clone(), "WeightMapSolutionAnalysis");
    }


    @Override
    public void run()
    {
        coverWeight.run();

        ManualCSVMaker generatedGradesCSV = new ManualCSVMaker(identifier, "generatedGrades")
        {
            @Override
            protected void setHeader()
            {
                ArrayList<String> columnNames = new ArrayList<>();
                columnNames.add("WeightMap");
                for (ClassUnderTest c : coverWeight.getCuts())
                {
                    columnNames.add(c.getIdentifer());
                }
                addLine(generateLine(columnNames));
            }

        };

        for (WeightMap w : coverWeight.getWeightMaps())
        {
            ArrayList<String> line = new ArrayList<>();
            line.add(w.getName());

            for (ClassUnderTest c : coverWeight.getCuts())
            {
                System.out.println(c);
                System.out.println(w);

                HitCounts h = coverWeight.getHitCounts(c, w);
                line.add(String.valueOf(h.generateGrade()));
            }
            generatedGradesCSV.addLine(CSVMaker.generateLine(line));
        }
        generatedGradesCSV.write();
    }
}
