package coverweight.experiments;

import coverweight.CoverWeight;
import coverweight.experiments.output.CoverageVarianceDeltaToGradeDeltaCorrelationGenerator;
import coverweight.experiments.output.CoverageVarianceGradeDeltaCSVGenerator;
import coverweight.experiments.output.CoverageVarianceGradeDeltaCorrelationGenerator;
import coverweight.sourcefiles.UnitTest;
import coverweight.weighting.WeightMap;
import org.apache.commons.math3.random.RandomGenerator;
import org.apache.commons.math3.random.RandomGeneratorFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Gets change of grades from inflating weights of each test in turn
 */
public class RandomWeightModificationExperiment extends WeightModificationExperiment
{
    private Logger logger = LoggerFactory.getLogger(getClass());

    private static final int NUMBER_VARIANTS = 5000;
    private static final int MIN_WEIGHT = 0;
    private static final int MAX_WEIGHT = 50000;

    public RandomWeightModificationExperiment(CoverWeight coverWeightInstance)
    {
        super(coverWeightInstance.clone());
        setIdentifier("RandomWeightModification");
    }

    @SuppressWarnings("Duplicates")
    @Override
    public void run()
    {
        // Setup balanced baseline system
        CoverWeight balanced = coverWeight.clone();
        List<WeightMap> naiveWeightMap = new ArrayList<>();
        naiveWeightMap.add(new WeightMap(coverWeight.getUnitTests()));
        balanced.setWeightMaps(CoverWeight.normalizeWeightMaps(naiveWeightMap));
        logger.info("Running balanced version");
        balanced.run();

        // Make unbalanced variant with random weights
        CoverWeight unbalanced = coverWeight.clone();
        unbalanced.setWeightMaps(generateUnbalancedWeightMaps(coverWeight.getUnitTests()));
        unbalanced.run();

        storeResults(unbalanced, balanced);
    }

    private List<WeightMap> generateUnbalancedWeightMaps(List<UnitTest> unitTests)
    {
        List<WeightMap> unbalanced = new ArrayList<>();

        // Generate random WeightMaps
        for (int i = 0; i < NUMBER_VARIANTS; i ++)
        {
            WeightMap currentMap = new WeightMap(unitTests);

            for (UnitTest t : unitTests)
            {
                currentMap.put(t, generateRandomWeight());
            }

            unbalanced.add(new WeightMap(currentMap));
        }

        return CoverWeight.normalizeWeightMaps(unbalanced);
    }

    private double generateRandomWeight()
    {
        RandomGenerator randomGenerator = RandomGeneratorFactory.createRandomGenerator(new Random());
        return MIN_WEIGHT + ((MAX_WEIGHT - MIN_WEIGHT) * randomGenerator.nextDouble());
    }

    private void storeResults(CoverWeight unbalancedCW, CoverWeight balancedCW)
    {
        CoverageVarianceGradeDeltaCorrelationGenerator correlationGenerator = new CoverageVarianceGradeDeltaCorrelationGenerator(identifier, "randomWeights", unbalancedCW, balancedCW);
        correlationGenerator.write();

        CoverageVarianceDeltaToGradeDeltaCorrelationGenerator deltaCorrelationGenerator = new CoverageVarianceDeltaToGradeDeltaCorrelationGenerator(identifier, "randomWeights", unbalancedCW, balancedCW);
        deltaCorrelationGenerator.write();

        CoverageVarianceGradeDeltaCSVGenerator generator = new CoverageVarianceGradeDeltaCSVGenerator(identifier, "randomWeights", unbalancedCW, balancedCW);
        generator.write();
    }
}

