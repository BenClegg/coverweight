package coverweight.experiments;

import coverweight.CoverWeight;
import coverweight.experiments.output.CovVarGradeDeltaCorrelationAllPermutationsVisitor;
import coverweight.experiments.output.CoverageVarianceDeltaToGradeDeltaCorrelationGenerator;
import coverweight.experiments.output.CoverageVarianceGradeDeltaCSVGenerator;
import coverweight.experiments.output.CoverageVarianceGradeDeltaCorrelationGenerator;
import coverweight.report.HitCounts;
import coverweight.sourcefiles.UnitTest;
import coverweight.weighting.WeightMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Gets change of grades from inflating weights of each test in turn
 */
public class WeightModificationExperiment extends Experiment
{
    private Logger logger = LoggerFactory.getLogger(getClass());

    private static final int FACTOR_LIMIT = 1000;
    private static final int FACTOR_STEP = 1;

    public static final boolean COV_BIAS_MODEL_ONLY = true;
    public static final boolean ABSOLUTE_DELTA = false;

    public WeightModificationExperiment(CoverWeight coverWeightInstance)
    {
        super(coverWeightInstance.clone(), "WeightModification");
    }

    @Override
    public void run()
    {
        // Setup balanced baseline system
        CoverWeight balanced = coverWeight.clone();
        List<WeightMap> naiveWeightMap = new ArrayList<>();
        naiveWeightMap.add(new WeightMap(coverWeight.getUnitTests()));
        balanced.setWeightMaps(CoverWeight.normalizeWeightMaps(naiveWeightMap));

        logger.info("Running balanced version");
        balanced.run();

        logger.info("Creating unbalanced variants...");
        // Make unbalanced variants

        CovVarGradeDeltaCorrelationAllPermutationsVisitor allTestsCorrelationVisitor =
                new CovVarGradeDeltaCorrelationAllPermutationsVisitor(identifier, balanced);

        for (UnitTest t : balanced.getUnitTests())
        {
            CoverWeight modified = balanced.clone();
            modified.setWeightMaps(generateUnbalancedWeightMaps(
                    balanced.getUnitTests(),
                    t
            ));
            modified.run();
            storeResults(t.getBaseName(), modified, balanced);
            allTestsCorrelationVisitor.visit(modified);
        }

        // Store
        allTestsCorrelationVisitor.write();
    }

    private void storeResults(String inflatedTestName, CoverWeight cwInstance, CoverWeight balancedCW)
    {
        logger.info(cwInstance.getCuts().toString());

        CoverageVarianceGradeDeltaCorrelationGenerator correlationGenerator = new CoverageVarianceGradeDeltaCorrelationGenerator(identifier, inflatedTestName, cwInstance, balancedCW);
        correlationGenerator.write();

        CoverageVarianceDeltaToGradeDeltaCorrelationGenerator deltaCorrelationGenerator = new CoverageVarianceDeltaToGradeDeltaCorrelationGenerator(identifier, inflatedTestName, cwInstance, balancedCW);
        deltaCorrelationGenerator.write();

        CoverageVarianceGradeDeltaCSVGenerator generator = new CoverageVarianceGradeDeltaCSVGenerator(identifier, inflatedTestName, cwInstance, balancedCW);
        generator.write();
        /*
        WeightMapsToGradeDeltaCSV gradesCSV = new WeightMapsToGradeDeltaCSV(identifier, inflatedTestName, hitCountsList, cwInstance, balancedHitCounts);
        WeightMapsToLineCoverageVarianceCoveredCSV lineCoverageVarianceCoveredCSV = new WeightMapsToLineCoverageVarianceCoveredCSV(identifier, inflatedTestName, hitCountsList, cwInstance);

        gradesCSV.write();
        lineCoverageVarianceCoveredCSV.write();
        */
    }

    private List<HitCounts> sortHitCounts(List<HitCounts> toSort)
    {
        // TODO reset to actually sorting if it makes any difference
        return toSort;
        /*
        LinkedList<HitCounts> sorted = new LinkedList<>(toSort);
        sorted.sort(Comparator.comparing(h -> h.getWeightMap().getName()));
        return sorted;
        */
    }

    private List<WeightMap> generateUnbalancedWeightMaps(List<UnitTest> unitTests, UnitTest testToModify)
    {
        List<WeightMap> unbalanced = new ArrayList<>();

        for (int i = 0; i < FACTOR_LIMIT; i += FACTOR_STEP)
        {
            // The weights generated are such that the modified test is set to a range of 0 to FACTOR_LIMIT
            // The weights of the other tests will be FACTOR_LIMIT / 2
            // This should get a spread of negative and positive influence
            unbalanced.add(new WeightMap(unitTests, testToModify, i, FACTOR_LIMIT));
        }

        return CoverWeight.normalizeWeightMaps(unbalanced);
    }

    public static double gradeDelta(HitCounts toCheck, HitCounts balanced)
    {
        if(ABSOLUTE_DELTA)
            return Math.abs(balanced.generateGrade() - toCheck.generateGrade());
        return balanced.generateGrade() - toCheck.generateGrade();
    }
}

