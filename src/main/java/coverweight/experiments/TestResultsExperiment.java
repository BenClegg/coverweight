package coverweight.experiments;

import coverweight.CoverWeight;
import coverweight.output.csv.ManualCSVMaker;
import coverweight.runner.TestResult;
import coverweight.runner.UnitTestCUTPair;
import coverweight.sourcefiles.ClassUnderTest;
import coverweight.sourcefiles.UnitTest;

import java.util.ArrayList;
import java.util.List;

public class TestResultsExperiment extends Experiment
{
    public TestResultsExperiment(CoverWeight coverWeightInstance)
    {
        super(coverWeightInstance.clone(), "TestResultsOutput");
    }

    @Override
    public void run()
    {
        // Setup csv
        ManualCSVMaker resultsCSV = new ManualCSVMaker(identifier, "testResults")
        {
            @Override
            protected void setHeader()
            {
                List<String> columns = new ArrayList<>();
                columns.add("ClassUnderTest");
                for (UnitTest t : coverWeight.getUnitTests())
                {
                    columns.add(t.getIdentifer());
                }
                addLine(generateLine(columns));
            }
        };

        // Fill content
        for (ClassUnderTest c : coverWeight.getCuts())
        {
            ArrayList<String> cells = new ArrayList<>();
            cells.add(c.getIdentifer());

            for (UnitTest t : coverWeight.getUnitTests())
            {
                UnitTestCUTPair testCUTPair = new UnitTestCUTPair(t,c);
                TestResult testResult = coverWeight.getTestResultMap().get(testCUTPair);
                if(testResult.isPasses())
                    cells.add("passes");
                else
                    cells.add("fails");
            }

            resultsCSV.addLine(ManualCSVMaker.generateLine(cells));
        }

        // Store output
        resultsCSV.write();
    }
}