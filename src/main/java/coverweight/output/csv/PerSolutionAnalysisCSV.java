package coverweight.output.csv;

import coverweight.report.HitCounts;
import coverweight.sourcefiles.ClassUnderTest;

import java.util.Collection;

public class PerSolutionAnalysisCSV extends CSVMaker
{
    private ClassUnderTest cut;
    private Collection<HitCounts> hitCountsCollection;


    public PerSolutionAnalysisCSV(ClassUnderTest classUnderTest, Collection<HitCounts> cutHitCounts)
    {
        super("perSolutionAnalysis", classUnderTest.getRelativeDir());
        cut = classUnderTest;
        hitCountsCollection = cutHitCounts;

        setHeader();
        generateLines();
    }

    @Override
    protected void setHeader()
    {
        lines.add("WeightMap" + ", " +
                "Variance (All)" + ", " +
                "Variance (Covered)" + ", " +
                "Generated Grade" + ", " +
                "All weights > 0?");
    }

    @Override
    public void generateLines()
    {
        for (HitCounts hc : hitCountsCollection)
        {
            lines.add(hc.getWeightMap().getName() + ", " +
                    hc.getVarianceAll() + ", " +
                    hc.getVarianceCoveredLines() + ", " +
                    hc.generateGrade() + ", " +
                    hc.getWeightMap().allWeightsGreaterThanZero());
        }
    }
}
