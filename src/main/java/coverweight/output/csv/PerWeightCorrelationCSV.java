package coverweight.output.csv;

import coverweight.report.HitCounts;
import coverweight.sourcefiles.ClassUnderTest;
import coverweight.weighting.WeightMap;
import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;

import java.util.*;

public class PerWeightCorrelationCSV extends CSVMaker
{
    Map<ClassUnderTest, Collection<HitCounts>> cutToHitCountsMap;
    Collection<WeightMap> weightMaps;

    public PerWeightCorrelationCSV(Map<ClassUnderTest, Collection<HitCounts>> cutToHitCountsCollectionMap,
                                   Collection<WeightMap> weightMapInstances)
    {
        super("perWeightCorrelation", "correlations");

        cutToHitCountsMap = cutToHitCountsCollectionMap;
        weightMaps = new ArrayList<>(weightMapInstances);

        setHeader();
        generateLines();
    }

    @Override
    protected void setHeader()
    {
        lines.add("WeightMap A" + ", " +
                "WeightMap B" + ", " +
                "Correlation");
    }

    @Override
    public void generateLines()
    {
        List<ClassUnderTest> cuts = new ArrayList<>(cutToHitCountsMap.keySet());

        for (WeightMap wmA : weightMaps)
        {
            handleWeightmap(wmA, cuts);
        }
    }

    // TODO allow for ability to change extracted value; pass as class constructor parameter
    private double getValue(HitCounts hitCounts)
    {
        return hitCounts.getVarianceCoveredLines();
    }

    private void handleWeightmap(WeightMap wmA, List<ClassUnderTest> cuts)
    {
        // Get statistics of both weightmaps for all cuts
        double[] valuesA = new double[cuts.size()];

        int i = 0;
        for(ClassUnderTest c : cuts)
        {
            try
            {
                HitCounts hitCounts = cutToHitCountsMap.get(c).stream().filter(hc -> hc.getWeightMap().equals(wmA)).findFirst().get();
                valuesA[i] = getValue(hitCounts);
                i ++;
            }
            catch (NoSuchElementException e)
            {
                e.printStackTrace();
                lines.add(wmA.getName() + ", ?" + ", " + "error");
                return;
            }
        }

        for (WeightMap wmB : weightMaps)
        {
            double[] valuesB = new double[cuts.size()];
            int j = 0;
            for(ClassUnderTest c : cuts)
            {
                try
                {
                    HitCounts hitCounts = cutToHitCountsMap.get(c).stream().filter(hc -> hc.getWeightMap().equals(wmB)).findFirst().get();
                    valuesB[j] = getValue(hitCounts);
                    j ++;
                } catch (NoSuchElementException e)
                {
                    e.printStackTrace();
                    lines.add(wmA.getName() + wmB.getName() + ", " + "error");
                    return;
                }
            }

            // Determine correlation between groups of values
            PearsonsCorrelation pearsonsCorrelation = new PearsonsCorrelation();
            double correl = pearsonsCorrelation.correlation(valuesA, valuesB);

            // Add to CSV
            lines.add(wmA.getName() + ", " +
                        wmB.getName() + ", " +
                        correl);
        }

        // TODO try to present in matrix form
    }
}
