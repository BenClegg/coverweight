package coverweight.output.csv;

import coverweight.CoverWeight;
import coverweight.report.HitCounts;
import coverweight.sourcefiles.ClassUnderTest;
import coverweight.weighting.WeightMap;
import org.apache.commons.math3.stat.correlation.Covariance;
import org.apache.commons.math3.stat.descriptive.moment.GeometricMean;
import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;
import org.apache.commons.math3.stat.descriptive.rank.Percentile;

import java.util.*;

public abstract class WeightResultStatsCSV extends CSVMaker
{
    List<ClassUnderTest> cuts;
    List<WeightMap> weightMaps;
    List<HitCounts> hitCountsList;

    public WeightResultStatsCSV(String baseFileName,
                                CoverWeight coverWeight)
    {
        super("WeightResultStatistics", baseFileName);
        cuts = coverWeight.getCuts();
        weightMaps = coverWeight.getWeightMaps();
        hitCountsList = coverWeight.run();

        setHeader();
        generateLines();
    }

    public abstract double getValue(HitCounts hitCounts);


    @Override
    protected void setHeader()
    {
        ArrayList<String> cells = new ArrayList<>();
        cells.add("WeightMap");
        cells.add("Mean");
        cells.add("Std Dev");
        cells.add("Q1");
        cells.add("Q3");
        cells.add("MinInRange");
        cells.add("MaxInRange");
        cells.add("Outliers");
        addLine(generateLine(cells));
    }

    @Override
    public void generateLines()
    {
        // Each weightmap is a row
        for (WeightMap w : weightMaps)
        {
            List<String> cells = new ArrayList<>();
            cells.add(w.getName());

            // Get stats on data
            double[] values = new double[cuts.size()];
            int i = 0;
            for (ClassUnderTest c : cuts)
            {
                try
                {
                    HitCounts hc = hitCountsList.stream().filter(h ->
                            (h.getWeightMap().equals(w) && h.getClassUnderTest().equals(c)))
                            .findFirst().get();
                    values[i] = getValue(hc);
                }
                catch (NoSuchElementException e)
                {
                    e.printStackTrace();
                    return;
                }
                i++;
            }

            // Mean
            Mean mean = new Mean();
            double m = mean.evaluate(values, 0, values.length);
            cells.add(String.valueOf(m));

            // Std Dev
            StandardDeviation standardDeviation = new StandardDeviation();
            double sd = standardDeviation.evaluate(values);
            cells.add(String.valueOf(sd));

            // Quartiles
            Percentile percentile = new Percentile();
            // Q1
            double qOne = percentile.evaluate(values, 25);
            cells.add(String.valueOf(qOne));

            // Q3
            double qThree = percentile.evaluate(values, 75);
            cells.add(String.valueOf(qThree));

            // Range
            // TODO change to percentiles
            double iqr = qThree - qOne;
            double lowerThreshold = qOne - (1.5 * iqr);
            double upperThreshold = qThree + (1.5 * iqr);
            double minInRange = lowestInRange(values, lowerThreshold);
            double maxInRange = highestInRange(values, upperThreshold);
            cells.add(String.valueOf(minInRange));
            cells.add(String.valueOf(maxInRange));

            // Outliers

            // Add line for weightmap
            addLine(generateLine(cells));
        }
    }

    private String generateOutliersCell(List<Double> outliers)
    {
        if(outliers.isEmpty())
        {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        Iterator<Double> outlierIter = outliers.iterator();
        while (outlierIter.hasNext())
        {
            sb.append("<");
            sb.append(outlierIter.next());
            sb.append(">");
        }
        return sb.toString();
    }

    private List<Double> outliers(double[] values, double lowerThreshold, double upperThreshold)
    {
        List<Double> outliers = new ArrayList<>();
        for (int i = 0; i < values.length; i++)
        {
            if(values[i] < lowerThreshold)
                outliers.add(values[i]);
            else if (values[i] > upperThreshold)
                outliers.add(values[i]);
            i++;
        }
        return outliers;
    }

    @SuppressWarnings("Duplicates")
    private double lowestInRange(double[] values, double lowerThreshold)
    {
        Mean mean = new Mean();
        double current = mean.evaluate(values, 0, values.length);

        for (int i = 0; i < values.length; i++)
        {
            if(values[i] >= lowerThreshold)
            {
                if(values[i] < current)
                {
                    current = values[i];
                }
            }
            i++;
        }
        return current;
    }

    @SuppressWarnings("Duplicates")
    private double highestInRange(double[] values, double upperThreshold)
    {
        Mean mean = new Mean();
        double current = mean.evaluate(values, 0, values.length);

        for (int i = 0; i < values.length; i++)
        {
            if(values[i] <= upperThreshold)
            {
                if(values[i] > current)
                {
                    current = values[i];
                }
            }
            i++;
        }
        return current;
    }
}
