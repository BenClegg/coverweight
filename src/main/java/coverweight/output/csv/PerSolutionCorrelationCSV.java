package coverweight.output.csv;

import coverweight.report.HitCounts;
import coverweight.sourcefiles.ClassUnderTest;
import coverweight.weighting.WeightMap;
import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;

import java.util.*;

public class PerSolutionCorrelationCSV extends CSVMaker
{
    Map<ClassUnderTest, Collection<HitCounts>> cutToHitCountsMap;
    Collection<WeightMap> weightMaps;
    Collection<ClassUnderTest> modelCuts;

    public PerSolutionCorrelationCSV(Map<ClassUnderTest, Collection<HitCounts>> cutToHitCountsCollectionMap,
                                     Collection<WeightMap> weightMapInstances)
    {
        super("perSolutionCorrelation", "correlations");

        cutToHitCountsMap = cutToHitCountsCollectionMap;
        weightMaps = new ArrayList<>(weightMapInstances);

        setHeader();
        generateLines();
    }

    @Override
    protected void setHeader()
    {
        lines.add("ClassUnderTest A" + ", " +
                "ClassUnderTest B" + ", " +
                "Correlation");
    }

    @Override
    public void generateLines()
    {
        List<ClassUnderTest> cuts = new ArrayList<>(cutToHitCountsMap.keySet());

        for (ClassUnderTest cA : cuts)
        {
            handleCUT(cA, cuts);
        }
    }

    // TODO allow for ability to change extracted value; pass as class constructor parameter
    public double getValue(HitCounts hitCounts)
    {
        return hitCounts.getVarianceCoveredLines();
    }

    private void handleCUT(ClassUnderTest cutA, List<ClassUnderTest> cuts)
    {
        // Get statistics of both weightmaps for all cuts
        double[] valuesA = new double[weightMaps.size()];

        int i = 0;
        for(WeightMap w : weightMaps)
        {
            try
            {
                HitCounts hitCounts = cutToHitCountsMap.get(cutA).stream().filter(hc -> hc.getWeightMap().equals(w)).findFirst().get();
                valuesA[i] = getValue(hitCounts);
                if(Double.isNaN(valuesA[i]))
                    System.out.println("NaN: " + w.getName() + " " + cutA.getRelativeDir());
                i ++;
            }
            catch (NoSuchElementException e)
            {
                e.printStackTrace();
                lines.add(cutA.getRelativeDir() + ", ?" + ", " + "error");
                return;
            }
        }

        for (ClassUnderTest cutB : cuts)
        {
            double[] valuesB = new double[weightMaps.size()];
            int j = 0;
            for(WeightMap w : weightMaps)
            {
                try
                {
                    HitCounts hitCounts = cutToHitCountsMap.get(cutB).stream().filter(hc -> hc.getWeightMap().equals(w)).findFirst().get();
                    valuesB[j] = getValue(hitCounts);
                    j ++;
                } catch (NoSuchElementException e)
                {
                    e.printStackTrace();
                    lines.add(cutA.getRelativeDir() + cutB.getRelativeDir() + ", " + "error");
                    if(Double.isNaN(valuesB[i]))
                        System.out.println("NaN: " + w.getName() + " " + cutB.getRelativeDir());
                    return;
                }
            }

            // Determine correlation between groups of values
            PearsonsCorrelation pearsonsCorrelation = new PearsonsCorrelation();
            double correl = pearsonsCorrelation.correlation(valuesA, valuesB);

            // Add to CSV
            lines.add(cutA.getRelativeDir() + ", " +
                        cutB.getRelativeDir() + ", " +
                        correl);
        }

        // TODO try to present in matrix form
    }
}
