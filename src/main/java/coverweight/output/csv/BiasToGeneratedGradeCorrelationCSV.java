package coverweight.output.csv;

import coverweight.report.HitCounts;
import coverweight.sourcefiles.ClassUnderTest;
import coverweight.weighting.WeightMap;
import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;

import java.util.Collection;
import java.util.Map;
import java.util.NoSuchElementException;

public class BiasToGeneratedGradeCorrelationCSV extends CSVMaker
{
    private Collection<ClassUnderTest> cuts;
    private Map<ClassUnderTest, Collection<HitCounts>> cutToHitCounts;
    private Collection<WeightMap> weightMaps;

    public BiasToGeneratedGradeCorrelationCSV(Collection<ClassUnderTest> classUnderTests,
                                              Map<ClassUnderTest, Collection<HitCounts>> cutToHitCountsMap,
                                              Collection<WeightMap> weightMapCollection)
    {
        super("coverageBiasGeneratedGradesCorrelation",
                "correlation");
        cuts = classUnderTests;
        cutToHitCounts = cutToHitCountsMap;
        weightMaps = weightMapCollection;

        setHeader();
        generateLines();
    }


    @Override
    protected void setHeader()
    {
        lines.add("WeightMap, Correlation (Coverage Bias & Grade)");
    }

    @Override
    public void generateLines()
    {
        for (WeightMap w : weightMaps)
        {
            handleWeightMap(w);
        }
    }

    private void handleWeightMap(WeightMap w)
    {
        double[] biasMeasures = new double[cuts.size()];
        double[] grades = new double[cuts.size()];
        int i = 0;

        for (ClassUnderTest cut : cuts)
        {
            try
            {
                HitCounts hc = cutToHitCounts.get(cut).stream().filter(h -> h.getWeightMap().equals(w)).findFirst().get();
                biasMeasures[i] = getCoverageBiasMetric(hc);
                grades[i] = hc.generateGrade();
            }
            catch (NoSuchElementException e)
            {
                e.printStackTrace();
                lines.add(w.getName() + ", " + "Error");
                return;
            }

            i++;
        }

        // Calculate correlation and write
        PearsonsCorrelation pearsonsCorrelation = new PearsonsCorrelation();
        double correl = pearsonsCorrelation.correlation(biasMeasures, grades);
        lines.add(w.getName() + ", " + correl);
    }

    private double getCoverageBiasMetric(HitCounts hc)
    {
        return hc.getVarianceCoveredLines();
    }
}
