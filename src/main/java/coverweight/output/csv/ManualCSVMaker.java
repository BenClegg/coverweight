package coverweight.output.csv;

public abstract class ManualCSVMaker extends CSVMaker
{

    public ManualCSVMaker(String outputDirName, String baseFileName)
    {
        super(outputDirName, baseFileName);
        setHeader();
    }

    @Override
    public void generateLines()
    {
        // Do nothing
    }
}
