package coverweight.output.csv;


import coverweight.output.OutputFile;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.Iterator;
import java.util.List;

public abstract class CSVMaker extends OutputFile
{

    public CSVMaker(String outputDirName, String baseFileName)
    {
        super(outputDirName, baseFileName, ".csv");
    }

    protected abstract void setHeader();

    public abstract void generateLines();

    public static String generateLine(List<String> tokens)
    {
        StringBuilder sb = new StringBuilder();

        Iterator<String> iterator = tokens.iterator();
        while (iterator.hasNext())
        {
            sb.append(iterator.next());
            if(iterator.hasNext())
                sb.append(",");
        }
        return sb.toString();
    }
}
