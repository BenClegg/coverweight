package coverweight.output.csv;

import java.util.ArrayList;

public abstract class BufferedCSVMaker extends CSVMaker
{
    private static final int DEFAULT_BUFFER_SIZE = 300;
    private int bufferSize;

    public BufferedCSVMaker(String outputDirName, String baseFileName)
    {
        this(outputDirName, baseFileName, DEFAULT_BUFFER_SIZE);
    }
    public BufferedCSVMaker(String outputDirName, String baseFileName, int lineBufferSize)
    {
        super(outputDirName, baseFileName);
        bufferSize = lineBufferSize;
        lines = new ArrayList<>(bufferSize);
    }

    @Override
    public void addLine(String line)
    {
        lines.add(line);
        if(lines.size() >= bufferSize)
        {
            // Append to file
            write();
            // Clear lines
            lines.clear();
        }
    }
}
