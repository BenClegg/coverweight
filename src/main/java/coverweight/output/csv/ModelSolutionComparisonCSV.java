package coverweight.output.csv;

import coverweight.report.HitCounts;
import coverweight.sourcefiles.ClassUnderTest;
import coverweight.weighting.WeightMap;

import java.io.File;
import java.util.Collection;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;

public class ModelSolutionComparisonCSV extends CSVMaker
{
    private ClassUnderTest modelCut;
    Map<ClassUnderTest, Collection<HitCounts>> cutToHitCountsMap;
    WeightMap weightMap;

    public ModelSolutionComparisonCSV(ClassUnderTest modelClassUnderTest,
                                      Map<ClassUnderTest, Collection<HitCounts>> cutToHitCountsCollectionMap,
                                      WeightMap weightMapInstance)
    {
        super("modelSolutionComparison", modelClassUnderTest.getRelativeDir() + File.separator + weightMapInstance.getName());
        modelCut = modelClassUnderTest;
        cutToHitCountsMap = cutToHitCountsCollectionMap;
        weightMap = weightMapInstance;

        setHeader();
        generateLines();
    }

    @Override
    protected void setHeader()
    {
        lines.add("ClassUnderTest" + ", " +
                "Variance (All)" + ", " +
                "Variance (Covered)" + ", " +
                "Generated Grade");
    }

    @Override
    public void generateLines()
    {
        Optional<HitCounts> optionalHitCounts = cutToHitCountsMap.get(modelCut).stream().filter(h -> h.getWeightMap().equals(weightMap)).findFirst();
        if(!optionalHitCounts.isPresent())
            return;

        HitCounts modelHitCounts = optionalHitCounts.get();


        lines.add("CUT, Variance (All), Variance (Covered), Grade");
        lines.add("Model-" + modelCut.getRelativeDir() + ", " +
                modelHitCounts.getVarianceAll() + ", " +
                modelHitCounts.getVarianceCoveredLines() + ", " +
                modelHitCounts.generateGrade());

        for (ClassUnderTest c : cutToHitCountsMap.keySet())
        {
            try
            {
                HitCounts hitCounts = cutToHitCountsMap.get(c).stream().filter(h -> h.getWeightMap().equals(weightMap)).findFirst().get();

                // TODO store stats for final calculation

                lines.add(c.getRelativeDir() + ", " +
                        hitCounts.getVarianceAll() + ", " +
                        hitCounts.getVarianceCoveredLines() + ", " +
                        hitCounts.generateGrade());
            }
            catch (NoSuchElementException e)
            {
                e.printStackTrace();
                lines.add(c.getRelativeDir() + ", " +
                        "Weightmap not found" + ", " +
                        "Weightmap not found" + ", " +
                        "Weightmap not found");
            }
        }

        // Now calculate correlations

    }
}
