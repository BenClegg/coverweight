package coverweight.output;

import coverweight.report.HitCounts;

import java.io.File;

public class HitCountsSummary extends OutputFile
{
    HitCounts hitCounts;

    public HitCountsSummary(HitCounts hitCountsInstance)
    {
        super("hitCountsSummary" + File.separator + hitCountsInstance.getClassUnderTest().getRelativeDir(),
                hitCountsInstance.getWeightMap().getName(),
                ".txt");
        hitCounts = hitCountsInstance;
    }

    @Override
    public void generateLines()
    {
        lines.add(hitCounts.toString());
    }
}
