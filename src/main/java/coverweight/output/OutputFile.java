package coverweight.output;

import coverweight.configuration.Global;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.Buffer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public abstract class OutputFile
{
    private File location;
    protected List<String> lines;
    private BufferedWriter bufferedWriter;

    public OutputFile(String outputDirName, String baseFileName, String extension)
    {
        location = new File(Global.getResultsDir() + File.separator +
                outputDirName + File.separator +
                baseFileName + extension);
        lines = new ArrayList<>();
        try
        {
            location.getParentFile().mkdirs();
            bufferedWriter = new BufferedWriter(new FileWriter(location, true));
        } catch (IOException ioEx) {
            ioEx.printStackTrace();
            System.err.println("Failed to initialise BufferedWriter at " + location);
        }
    }

    public void setLocation(String relativeLocation)
    {
        location = new File(Global.getResultsDir() + File.separator + relativeLocation);
    }

    public void addLine(String line)
    {
        lines.add(line);
    }

    public abstract void generateLines();

    public void write()
    {
        if(lines == null)
            return;
        if(lines.isEmpty())
            return;

        try
        {
            //Files.write(location.toPath(), lines, Charset.defaultCharset());
            for (String line : lines)
            {
                bufferedWriter.write(line);
                bufferedWriter.newLine();
            }
            bufferedWriter.flush();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void close()
    {
        try
        {
            bufferedWriter.close();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
