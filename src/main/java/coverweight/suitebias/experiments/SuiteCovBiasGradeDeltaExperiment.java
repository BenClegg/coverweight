package coverweight.suitebias.experiments;

import coverweight.CoverWeight;
import coverweight.experiments.Experiment;
import coverweight.experiments.output.CovVarGradeDeltaCorrelationAllPermutationsVisitor;
import coverweight.experiments.output.CoverageVarianceGradeDeltaCSVGenerator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SuiteCovBiasGradeDeltaExperiment extends Experiment
{
    // Config
    private static final boolean RUN_CORRELATION = false;
    private static final boolean RUN_STATISTIC_GENERATION = true;


    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private CoverWeight balanced;

    public SuiteCovBiasGradeDeltaExperiment(CoverWeight generatedCW, CoverWeight balancedCW)
    {
        super(generatedCW, "SuiteCoverageBiasGradeDeltaCorrelation");
        balanced = balancedCW;
    }

    @Override
    public void run()
    {
        if(RUN_CORRELATION)
        {
            logger.info("Running correlation analysis...");
            CovVarGradeDeltaCorrelationAllPermutationsVisitor visitor = new CovVarGradeDeltaCorrelationAllPermutationsVisitor(this.getIdentifier(), balanced);
            visitor.visit(getCoverWeight());
            visitor.write();
        }

        if(RUN_STATISTIC_GENERATION)
        {
            logger.info("Generating grades and coverage variances (plus deltas) for all test suites...");
            CoverageVarianceGradeDeltaCSVGenerator csvGenerator
                    = new CoverageVarianceGradeDeltaCSVGenerator(this.getIdentifier(), "allEnabledTests", getCoverWeight(), balanced);
            csvGenerator.write();
        }
    }
}
