package coverweight.suitebias.experiments.generators;

import coverweight.report.HitCounts;

import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public abstract class StatisticGenerator
{
    private String name;

    public StatisticGenerator(String name)
    {
        this.name = name;
    }

    public abstract Object generate(HitCounts hitCounts, HitCounts modelHC);

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StatisticGenerator that = (StatisticGenerator) o;
        return Objects.equals(getName(), that.getName());
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(getName());
    }

    public String getName()
    {
        return name;
    }
}
