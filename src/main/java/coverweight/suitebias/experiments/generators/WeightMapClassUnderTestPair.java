package coverweight.suitebias.experiments.generators;

import coverweight.sourcefiles.ClassUnderTest;
import coverweight.weighting.WeightMap;

public class WeightMapClassUnderTestPair
{
    private WeightMap weightMap;
    private ClassUnderTest classUnderTest;

    public WeightMapClassUnderTestPair(WeightMap weightMap, ClassUnderTest cut)
    {
        this.weightMap = weightMap;
        this.classUnderTest = cut;
    }

    public WeightMap getWeightMap()
    {
        return weightMap;
    }

    public ClassUnderTest getClassUnderTest()
    {
        return classUnderTest;
    }
}
