package coverweight.suitebias.experiments.generators;

import coverweight.report.HitCounts;

import java.util.concurrent.ConcurrentHashMap;

public class StatisticResult extends ConcurrentHashMap<HitCounts, Object>
{
    private String name;

    public StatisticResult(String name)
    {
        this.name = name;
    }

    public String getResultString(HitCounts hitCounts)
    {
        if(get(hitCounts) == null)
            throw new NullPointerException("No result for " + hitCounts + " of " + this.getName());
        return get(hitCounts).toString();
    }

    public String getName()
    {
        return name;
    }
}
