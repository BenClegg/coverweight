package coverweight.suitebias.experiments.output;

import coverweight.output.csv.BufferedCSVMaker;
import coverweight.output.csv.CSVMaker;
import coverweight.sourcefiles.UnitTest;

import java.util.Iterator;
import java.util.List;

public abstract class TestInfoCSV extends BufferedCSVMaker
{
    private List<UnitTest> tests;
    private String otherVariable;

    public TestInfoCSV(String outputDirName, String baseFileName, List<UnitTest> unitTests, String otherVariableName)
    {
        super(outputDirName, baseFileName);
        tests = unitTests;
        otherVariable = otherVariableName;

        setHeader();
        generateLines();
    }

    @Override
    protected void setHeader()
    {
        StringBuilder sb = new StringBuilder();
        sb.append(otherVariable);
        sb.append(",");

        Iterator<UnitTest> testIterator = tests.listIterator();
        while (testIterator.hasNext())
        {
            sb.append(testIterator.next().getBaseName());
            if(testIterator.hasNext())
                sb.append(",");
        }

        addLine(sb.toString());
    }

    public List<UnitTest> getTests()
    {
        return tests;
    }
}
