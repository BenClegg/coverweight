package coverweight.suitebias.experiments;

import coverweight.CoverWeight;
import coverweight.configuration.Global;
import coverweight.experiments.Experiment;
import coverweight.mistakesim.checkresults.TestResultsMap;
import coverweight.output.csv.BufferedCSVMaker;
import coverweight.report.HitCounts;
import coverweight.runner.TestResult;
import coverweight.sourcefiles.ClassUnderTest;
import coverweight.suitebias.DiagnosabilityMetrics;
import coverweight.suitebias.experiments.generators.StatisticGenerator;
import coverweight.suitebias.experiments.generators.StatisticResult;
import coverweight.weighting.WeightMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

public class StatisticAnalysisExperiment extends Experiment
{
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private ConcurrentMap<String, TestResultsMap> mutantResults;
    private final String baseSubDir;

    public StatisticAnalysisExperiment(CoverWeight coverWeight, Collection<TestResultsMap> mutantResults, int runNumber)
    {
        super(coverWeight, "StatisticAnalysisExperiment");
        this.baseSubDir = "SizeGeneration/" + String.format("%03d", runNumber);

        this.mutantResults = new ConcurrentHashMap<>();
        for (TestResultsMap trm : mutantResults)
            this.mutantResults.put(trm.getName(), trm);
    }

    @Override
    public void run()
    {
        Collection<ClassUnderTest> classUnderTestCollection = new ArrayList<>(getCoverWeight().getCuts());
        // Should ignore model solutions in this analysis
        classUnderTestCollection.removeAll(getCoverWeight().getModelCuts());


        // Students' solutions
        updateOutputDir("students");
        ConcurrentMap<StatisticGenerator, StatisticResult> studentPropertyMap = new ConcurrentHashMap<>();
        initStatisticGenerators(studentPropertyMap);
        StatisticAnalysisCSV studentCSV = new StatisticAnalysisCSV(studentPropertyMap);


        ClassUnderTest modelCUT = getCoverWeight().getModelCuts().get(0);

        for (WeightMap wm : getCoverWeight().getWeightMaps())
        {
            HitCounts modelHC = getCoverWeight().getHitCounts(modelCUT, wm);
            List<HitCounts> processedHitCounts =
                    classUnderTestCollection.parallelStream().map(cut -> {
                        HitCounts hitCounts = getCoverWeight().getHitCounts(cut, wm);
                        processHitCounts(hitCounts, modelHC, studentPropertyMap);
                        return hitCounts;
                    }).collect(Collectors.toList());
            for (HitCounts hc : processedHitCounts)
            {
                if (hc != null)
                    studentCSV.writeProcessedHitCounts(hc);
                else
                    System.out.println("WARNING NULL STUDENT HITCOUNTS");
            }
        }

        studentCSV.write();

        // Generate statistics for each every mutant set
        for (String toolId : mutantResults.keySet())
        {
            updateOutputDir(toolId);

            ConcurrentMap<StatisticGenerator, StatisticResult> mutantPropertyMap = new ConcurrentHashMap<>();
            initStatisticGenerators(mutantPropertyMap);
            StatisticAnalysisCSV mutantCSV = new StatisticAnalysisCSV(mutantPropertyMap);

            TestResultsMap mutantGradeerResults = mutantResults.get(toolId);

            getCoverWeight().getWeightMaps().forEach(wm -> {
                HitCounts modelHC = getCoverWeight().getHitCounts(modelCUT, wm);
                List<HitCounts> processedHitCounts =
                mutantGradeerResults.getAllGradeerSolutions().parallelStream()
                        .filter(mutantGradeerResults::solutionDetectedByAnyTests)
                        .map(m -> {
                            HitCounts hitCounts = new HitCounts(m, mutantGradeerResults, wm);
                            processHitCounts(hitCounts, modelHC, mutantPropertyMap);
                            return hitCounts;
                        }).collect(Collectors.toList());
                for(HitCounts hc : processedHitCounts)
                {
                    if(hc != null)
                        mutantCSV.writeProcessedHitCounts(hc);
                    else
                        System.out.println("WARNING NULL MUTANT HITCOUNTS (" + toolId + ")");
                }
            });

            mutantCSV.write();
        }

        // Reset subDir
        Global.setCurrentResultsSubDir(baseSubDir);
    }

    private void updateOutputDir(String datasetIdentifier)
    {
        Global.setCurrentResultsSubDir("results-" + datasetIdentifier + "/" + baseSubDir);
    }

    private void processHitCounts(HitCounts hitCounts, HitCounts modelHC, ConcurrentMap<StatisticGenerator, StatisticResult> propertyMap)
    {
        for (StatisticGenerator gen : propertyMap.keySet())
        {
            Object result = gen.generate(hitCounts, modelHC);
            if(result == null)
                result = null;
            propertyMap.get(gen).put(hitCounts, result);
        }
    }

    private void initStatisticGenerators(ConcurrentMap<StatisticGenerator, StatisticResult> propertyMap)
    {
        // TODO Only use model hitcounts for non-grade statistics?

        Set<StatisticGenerator> statisticGenerators = new HashSet<>();

        statisticGenerators.add(new StatisticGenerator("Grade")
        {
            @Override
            public Object generate(HitCounts hitCounts, HitCounts modelHC)
            {
                if(hitCounts.generateGrade() > 100)
                    logger.info("Grade > 100; " + hitCounts.toString());
                return hitCounts.generateGrade();
            }
        });
        statisticGenerators.add(new StatisticGenerator("Coverage Variance Covered")
        {
            @Override
            public Object generate(HitCounts hitCounts, HitCounts modelHC)
            {
                return modelHC.getVarianceCoveredLines();
            }
        });
        statisticGenerators.add(new StatisticGenerator("Coverage Variance All")
        {
            @Override
            public Object generate(HitCounts hitCounts, HitCounts modelHC)
            {
                return modelHC.getVarianceAll();
            }
        });
        statisticGenerators.add(new StatisticGenerator("Coverage Variance Covered Model")
        {
            @Override
            public Object generate(HitCounts hitCounts, HitCounts modelHC)
            {
                return modelHC.getVarianceCoveredLines();
            }
        });
        statisticGenerators.add(new StatisticGenerator("Coverage Variance All Model")
        {
            @Override
            public Object generate(HitCounts hitCounts, HitCounts modelHC)
            {
                return modelHC.getVarianceAll();
            }
        });
        statisticGenerators.add(new StatisticGenerator("Suite Size")
        {
            @Override
            public Object generate(HitCounts hitCounts, HitCounts modelHC)
            {
                return modelHC.getWeightMap().getTotalWeights();
            }
        });
        statisticGenerators.add(new StatisticGenerator("Covered Lines")
        {
            @Override
            public Object generate(HitCounts hitCounts, HitCounts modelHC)
            {
                return modelHC.keySet().stream().filter(l -> modelHC.get(l) > 0.001).count();
            }
        });
        statisticGenerators.add(new StatisticGenerator("Uncovered Lines")
        {
            @Override
            public Object generate(HitCounts hitCounts, HitCounts modelHC)
            {
                return modelHC.keySet().stream().filter(l -> modelHC.get(l) < 0.001).count();
            }
        });
        statisticGenerators.add(new StatisticGenerator("Calculated Covered Line Ratio")
        {
            @Override
            public Object generate(HitCounts hitCounts, HitCounts modelHC)
            {
                return modelHC.coveredLineRatio();
            }
        });
        statisticGenerators.add(new StatisticGenerator("Redundant Recoverage Sum")
        {
            @Override
            public Object generate(HitCounts hitCounts, HitCounts modelHC)
            {
                return modelHC.getSumRecoverage();
            }
        });
        statisticGenerators.add(new StatisticGenerator("Number Recovered Lines")
        {
            @Override
            public Object generate(HitCounts hitCounts, HitCounts modelHC)
            {
                return modelHC.values().stream().filter(d -> d > 1.0001).count();
            }
        });
        statisticGenerators.add(new StatisticGenerator("Total Passing Tests")
        {
            @Override
            public Object generate(HitCounts hitCounts, HitCounts modelHC)
            {
                return modelHC.getTestResults().stream().filter(TestResult::isPasses).count();
            }
        });
        statisticGenerators.add(new StatisticGenerator("Total Failing Tests")
        {
            @Override
            public Object generate(HitCounts hitCounts, HitCounts modelHC)
            {
                return modelHC.getTestResults().stream().filter(tr -> !tr.isPasses()).count();
            }
        });
        statisticGenerators.add(new StatisticGenerator("Passing Enabled Tests")
        {
            @Override
            public Object generate(HitCounts hitCounts, HitCounts modelHC)
            {
                return modelHC.getPassingEnabledTestsCount();
            }
        });
        statisticGenerators.add(new StatisticGenerator("Failing Enabled Tests")
        {
            @Override
            public Object generate(HitCounts hitCounts, HitCounts modelHC)
            {
                return modelHC.getFailingEnabledTestsCount();
            }
        });
        statisticGenerators.add(new StatisticGenerator("Other Solutions With Detected Faults")
        {
            @Override
            public Object generate(HitCounts hitCounts, HitCounts modelHC)
            {
                Collection<ClassUnderTest> cuts = new ArrayList<>(getCoverWeight().getCuts());
                cuts.removeAll(getCoverWeight().getModelCuts()); // No model solutions taken into account
                cuts.remove(hitCounts.getClassUnderTest()); // Remove the current CUT

                return (double) numberMutantsKilledBySuite(cuts, hitCounts.getWeightMap()) / cuts.size();
            }
        });
        for (String mutationTool : mutantResults.keySet())
        {
            statisticGenerators.add(new StatisticGenerator("Mutation Score " + mutationTool)
            {
                @Override
                public Object generate(HitCounts hitCounts, HitCounts modelHC)
                {
                    return mutantResults.get(mutationTool).faultDetectionRate(modelHC.getWeightMap(), true);
                }
            });
        }
        // Proportion of tests enabled
        statisticGenerators.add(new StatisticGenerator("Proportion Tests Enabled")
        {
            @Override
            public Object generate(HitCounts hitCounts, HitCounts modelHC)
            {
                return modelHC.getWeightMap().getProportionTestsEnabled();
            }
        });
        // Diagnosability metrics
        statisticGenerators.add(new StatisticGenerator("Density")
        {
            @Override
            public Object generate(HitCounts hitCounts, HitCounts modelHC)
            {
                return DiagnosabilityMetrics.density(modelHC);
            }
        });
        statisticGenerators.add(new StatisticGenerator("Unnormalised Density")
        {
            @Override
            public Object generate(HitCounts hitCounts, HitCounts modelHC)
            {
                return DiagnosabilityMetrics.densityUnnormalised(modelHC);
            }
        });
        statisticGenerators.add(new StatisticGenerator("Diversity")
        {
            @Override
            public Object generate(HitCounts hitCounts, HitCounts modelHC)
            {
                return DiagnosabilityMetrics.diversity(modelHC);
            }
        });
        statisticGenerators.add(new StatisticGenerator("Uniqueness")
        {
            @Override
            public Object generate(HitCounts hitCounts, HitCounts modelHC)
            {
                return DiagnosabilityMetrics.uniqueness(modelHC);
            }
        });
        statisticGenerators.add(new StatisticGenerator("DDU")
        {
            @Override
            public Object generate(HitCounts hitCounts, HitCounts modelHC)
            {
                return DiagnosabilityMetrics.ddu(modelHC);
            }
        });
        statisticGenerators.add(new StatisticGenerator("GrowthTarget")
        {
            @Override
            public Object generate(HitCounts hitCounts, HitCounts modelHC)
            {
                return modelHC.getWeightMap().getGrowthTargetDetails();
            }
        });

        // Make map
        for(StatisticGenerator g : statisticGenerators)
        {
            propertyMap.put(g, new StatisticResult(g.getName()));
        }
    }

    private long numberMutantsKilledBySuite(Collection<ClassUnderTest> cuts, WeightMap weightMap)
    {
        return cuts.stream().filter(cut -> {
            HitCounts hc = getCoverWeight().getHitCounts(cut, weightMap);
            return !hc.allEnabledTestsPass();
        }).count();
    }

}

class StatisticAnalysisCSV extends BufferedCSVMaker
{
    ConcurrentMap<StatisticGenerator, StatisticResult> propertyMap;

    public StatisticAnalysisCSV(ConcurrentMap<StatisticGenerator, StatisticResult> propertyMap)
    {
        super("StatisticAnalysisExperiment", "combinedStatistics");
        this.propertyMap = propertyMap;
        setHeader();
    }

    @Override
    protected void setHeader()
    {
        Iterator<StatisticResult> statResultIterator = propertyMap.values().iterator();

        StringBuilder sb = new StringBuilder();
        sb.append("WeightMap,");
        sb.append("ClassUnderTest,");

        while (statResultIterator.hasNext())
        {
            sb.append(statResultIterator.next().getName());
            if(statResultIterator.hasNext())
                sb.append(",");
        }

        addLine(sb.toString());
    }

    @Override
    public void generateLines()
    {
        setHeader();
    }

    public void writeProcessedHitCounts(HitCounts hitCounts)
    {
        Iterator<StatisticResult> statGenIterator = propertyMap.values().iterator();

        StringBuilder sb = new StringBuilder();
        sb.append(hitCounts.getWeightMap().getName());
        sb.append(",");
        sb.append(hitCounts.getClassUnderTest().getIdentifer());
        sb.append(",");
        while (statGenIterator.hasNext())
        {
            sb.append(statGenIterator.next().getResultString(hitCounts));
            if(statGenIterator.hasNext())
                sb.append(",");
        }

        addLine(sb.toString());

        for (StatisticResult statResult : propertyMap.values())
            statResult.remove(hitCounts);

    }
}
