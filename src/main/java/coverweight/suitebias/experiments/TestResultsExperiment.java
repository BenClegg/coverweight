package coverweight.suitebias.experiments;

import coverweight.CoverWeight;
import coverweight.experiments.Experiment;
import coverweight.runner.TestResult;
import coverweight.sourcefiles.ClassUnderTest;
import coverweight.sourcefiles.UnitTest;
import coverweight.suitebias.experiments.output.TestInfoCSV;
import coverweight.weighting.WeightMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;

public class TestResultsExperiment extends Experiment
{
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    public TestResultsExperiment(CoverWeight coverWeightInstance)
    {
        super(coverWeightInstance, "TestResults");
    }
    @Override
    public void run()
    {
        logger.info("Storing results of tests on mutants...");

        TestInfoCSV testsPassingCUTs = new TestInfoCSV(this.getIdentifier(), "TestsPassingCUTs", getCoverWeight().getUnitTests(), "ClassUnderTest")
        {
            @Override
            public void generateLines()
            {
                for (ClassUnderTest c : getCoverWeight().getCuts())
                {
                    StringBuilder line = new StringBuilder();
                    line.append(c.getIdentifer());
                    line.append(",");

                    Iterator<UnitTest> testIterator = this.getTests().listIterator();
                    while (testIterator.hasNext())
                    {
                        UnitTest t = testIterator.next();
                        TestResult tr = getCoverWeight().getTestResultForTestCUT(t, c);

                        if(tr.isPasses())
                            line.append("pass");
                        else
                            line.append("fail");

                        if(testIterator.hasNext())
                            line.append(",");
                    }
                    addLine(line.toString());
                }
            }
        };
        testsPassingCUTs.write();
        logger.info("Test results stored!");
    }
}
