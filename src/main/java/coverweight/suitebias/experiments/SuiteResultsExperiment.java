package coverweight.suitebias.experiments;

import coverweight.CoverWeight;
import coverweight.experiments.Experiment;
import coverweight.runner.TestResult;
import coverweight.runner.UnitTestCUTPair;
import coverweight.sourcefiles.ClassUnderTest;
import coverweight.sourcefiles.UnitTest;
import coverweight.suitebias.experiments.output.TestInfoCSV;
import coverweight.weighting.WeightMap;

import java.util.Iterator;

public class SuiteResultsExperiment extends Experiment
{

    public SuiteResultsExperiment(CoverWeight coverWeightInstance)
    {
        super(coverWeightInstance, "SuiteResultsExperiment");
    }

    @Override
    public void run()
    {
        TestInfoCSV suiteEnabledTestsCSV = new TestInfoCSV(this.getIdentifier(), "SuiteEnabledTests", getCoverWeight().getUnitTests(), "SuiteWeightMap")
        {
            @Override
            public void generateLines()
            {
                for (WeightMap wm : getCoverWeight().getWeightMaps())
                {
                    StringBuilder line = new StringBuilder();
                    line.append(wm.getName());
                    line.append(",");

                    Iterator<UnitTest> testIterator = this.getTests().listIterator();
                    while (testIterator.hasNext())
                    {
                        UnitTest t = testIterator.next();
                        line.append(wm.get(t));
                        if(testIterator.hasNext())
                            line.append(",");
                    }
                    addLine(line.toString());
                }
            }
        };
        suiteEnabledTestsCSV.write();
    }
}