package coverweight.suitebias;

import coverweight.CoverWeight;
import coverweight.sourcefiles.UnitTest;
import coverweight.weighting.WeightMap;

import java.math.BigInteger;
import java.util.*;

public class RandomWeightMapGenerator
{

    public static Set<WeightMap> generateRandomWeightMaps(List<UnitTest> tests, int limit, int randomSeed)
    {
        Random random = new Random(randomSeed);
        Set<WeightMap> generatedWeightMaps = new HashSet<>();

        for (int i = 0; i <= limit; i++)
        {
            int size = 1 + (int) Math.floor(random.nextDouble() * (tests.size() - 1));
            generatedWeightMaps.add(generateWeightMapForSize(tests, size, random));
        }

        return generatedWeightMaps;
    }

    /**
     * Generate WeightMaps for a given set of sizes
     * @param tests The tests to generate weights for
     * @param targetTotal How many tests to aim to generate in total
     * @param sizeIntervals the sizes of WeightMaps to generate
     * @param randomSeed seed for random generator
     * @return generated WeightMaps
     */
    public static Set<WeightMap> generateRandomWeightMapsBySizeIntervals(List<UnitTest> tests, int targetTotal, double[] sizeIntervals, int randomSeed)
    {
        Random random = new Random(randomSeed);
        Set<WeightMap> generated = new HashSet<>();

        int weightmapsPerSize = (int) Math.ceil((double)targetTotal / sizeIntervals.length);
        int searchBudget = weightmapsPerSize * 4;

        for (double proportionTests : sizeIntervals)
        {
            int size = (int) Math.round(proportionTests * tests.size());

            Set<WeightMap> groupWeightMaps = new HashSet<>();

            int consumedSearchBudget = 0;
            while (groupWeightMaps.size() < weightmapsPerSize)
            {
                consumedSearchBudget ++;
                if(consumedSearchBudget > searchBudget)
                    break;

                groupWeightMaps.add(generateWeightMapForSize(tests, size, random));
            }
            generated.addAll(groupWeightMaps);
        }
        return generated;
    }

    /**
     * Randomly generate a set of weightmaps for intervaled sizes up to the number of tests - 1
     * @param tests The tests to generate weights for
     * @param totalLimit How many tests to generate in total
     * @param percentageInterval the percentage intervals of number of tests to generate
     * @return generated WeightMaps
     */
    public static Set<WeightMap> generateRandomWeightMapsBySize(List<UnitTest> tests, int totalLimit, int percentageInterval, int randomSeed)
    {

        Random random = new Random(randomSeed);
        Set<WeightMap> generated = new HashSet<>();

        //int numberForEachSize = totalLimit / tests.size();

        // Regular spacing of size groups
        Set<Integer> sizeGroups = new TreeSet<>();
        for (int s = 100 - percentageInterval; s > 1; s -= percentageInterval)
        {
            sizeGroups.add((int) Math.round((s / 100.0) * tests.size()));
        }
        sizeGroups.remove(tests.size());


        int numberForEachSize = totalLimit / sizeGroups.size();
        int searchBudget = numberForEachSize * 3;

        for (int sizeGroup : sizeGroups)
        {
            Set<WeightMap> groupWeightMaps = new HashSet<>();

            int consumedSearchBudget = 0;
            while (groupWeightMaps.size() < numberForEachSize)
            {
                consumedSearchBudget ++;
                if(consumedSearchBudget > searchBudget)
                    break;

                groupWeightMaps.add(generateWeightMapForSize(tests, sizeGroup, random));
            }
            generated.addAll(groupWeightMaps);
        }
        return generated;
    }

    private static WeightMap generateWeightMapForSize(Collection<UnitTest> tests, int targetSize, Random random)
    {
        // Configure empty WeightMap
        WeightMap weightMap = new WeightMap(tests);

        // Copy of set of tests to select from
        List<UnitTest> pool = new ArrayList<>(tests);
        Set<UnitTest> chosen = new HashSet<>();

        while (chosen.size() < targetSize)
        {
            if(pool.isEmpty())
                break;

            // Choose test in pool, move it to chosen
            Collections.shuffle(pool, random);
            UnitTest t = pool.get(0);

            chosen.add(t);
            pool.remove(t);
        }

        weightMap.onlyEnableTests(chosen);
        return weightMap;
    }
}
