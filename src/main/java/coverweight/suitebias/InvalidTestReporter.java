package coverweight.suitebias;

import coverweight.CoverWeight;
import coverweight.configuration.CLIReader;
import coverweight.configuration.ExecFlag;
import coverweight.output.OutputFile;
import coverweight.runner.TestResult;
import coverweight.sourcefiles.ClassUnderTest;
import coverweight.sourcefiles.UnitTest;

import java.util.HashSet;
import java.util.Set;

public class InvalidTestReporter
{
    private Set<UnitTest> invalidTests;

    public static void main(String[] args)
    {
        CLIReader cli = new CLIReader(args);
        // ExecFlags to disable non-model CUTs
        Set<ExecFlag> execFlags = new HashSet<>();
        execFlags.add(ExecFlag.ONLY_MODEL_CUTS);

        try
        {
            // Don't load non-model CUTs
            CoverWeight cw = new CoverWeight(cli, execFlags);
            cw.run();

            // Report invalid tests
            InvalidTestReporter invalidTestReporter = new InvalidTestReporter(cw);
            invalidTestReporter.display();
            invalidTestReporter.write();
        }
        catch (NullPointerException npEx)
        {
            npEx.printStackTrace();
            OutputFile errorFile = new OutputFile("NullPointerExceptions", "NullPointerExceptions", ".txt")
            {
                @Override
                public void generateLines()
                {
                    addLine(npEx.getMessage());
                    StackTraceElement[] stackTraceElements = npEx.getStackTrace();
                    for (StackTraceElement e : stackTraceElements)
                    {
                        addLine(e.toString());
                    }
                }
            };
            errorFile.write();
        }

    }

    public InvalidTestReporter(CoverWeight coverWeight)
    {
        invalidTests = new HashSet<>();

        for (UnitTest t : coverWeight.getUnitTests())
        {
            for (ClassUnderTest c : coverWeight.getModelCuts())
            {
                TestResult result = coverWeight.getTestResultForTestCUT(t, c);
                if(!result.isPasses())
                    invalidTests.add(t);
            }
        }
    }

    public void display()
    {
        for (UnitTest t : invalidTests)
            System.out.println("[Invalid Test] " + t.getJavaFile());
    }

    public void write()
    {
        OutputFile outputFile = new OutputFile("InvalidTests", "invalidTests", ".txt")
        {
            @Override
            public void generateLines()
            {
                for (UnitTest t : invalidTests)
                {
                    addLine(t.getJavaFile().toString());
                }
            }
        };
        outputFile.generateLines();
        outputFile.write();
    }
}
