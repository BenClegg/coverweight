package coverweight.suitebias;

import coverweight.CoverWeight;
import coverweight.mistakesim.checkresults.TestResultsMap;
import coverweight.report.HitCounts;
import coverweight.sourcefiles.UnitTest;
import coverweight.weighting.GrowthTarget;
import coverweight.weighting.WeightMap;
import org.apache.commons.lang3.time.StopWatch;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

public class GrowingWeightmapGenerator
{

    private final Collection<UnitTest> testPool;
    private ConcurrentMap<String, TestResultsMap> mutantResults;
    private int iterationLimit = 10;
    private final Random random;
    private CoverWeight coverWeight;

    public GrowingWeightmapGenerator(CoverWeight modelCoverWeight, Collection<UnitTest> testPool, Collection<TestResultsMap> mutantResults, int randomSeed)
    {
        this(modelCoverWeight, testPool, randomSeed);

        for (TestResultsMap trm : mutantResults)
            this.mutantResults.put(trm.getName(), trm);
    }

    public GrowingWeightmapGenerator(CoverWeight modelCoverWeight, Collection<UnitTest> testPool, int randomSeed)
    {
        this.testPool = testPool;
        this.mutantResults = new ConcurrentHashMap<>();
        this.random = new Random(randomSeed * 907);
        this.coverWeight = modelCoverWeight;
    }

    public void setIterationLimit(int iterationLimit)
    {
        this.iterationLimit = iterationLimit;
    }

    public Map<GrowthTarget, Collection<WeightMap>> generateForAllGrowthTargets()
    {
        System.out.println("Generating suites with all growth targets...");

        ConcurrentMap<GrowthTarget, Collection<WeightMap>> map = new ConcurrentHashMap<>();

        Collection<GrowthTarget> growthTargets = new HashSet<>();

        //Random criterion
        growthTargets.add(new GrowthTarget(GrowthTarget.Criterion.RANDOM, GrowthTarget.PostSaturationMode.NONE, GrowthTarget.NO_MUTANTS));
        growthTargets.add(new GrowthTarget(GrowthTarget.Criterion.RANDOM_PRESET, GrowthTarget.PostSaturationMode.NONE, GrowthTarget.NO_MUTANTS));
        Collection<GrowthTarget.Criterion> nonRandomCriteria =
                Arrays.stream(GrowthTarget.Criterion.values())
                        .filter(c -> c != GrowthTarget.Criterion.RANDOM)
                        .filter(c -> c != GrowthTarget.Criterion.RANDOM_PRESET)
                        .collect(Collectors.toSet());

        // Other targets
        for (GrowthTarget.Criterion c : nonRandomCriteria)
        {
            for (GrowthTarget.PostSaturationMode m : GrowthTarget.PostSaturationMode.values())
            {
                if(c.equals(GrowthTarget.Criterion.MUTATION) || m.equals(GrowthTarget.PostSaturationMode.SWITCH_CRITERION))
                {
                    for (String mutationTool : mutantResults.keySet())
                    {
                        growthTargets.add(new GrowthTarget(c, m, mutationTool));
                    }
                }
                else
                {
                    growthTargets.add(new GrowthTarget(c, m, GrowthTarget.NO_MUTANTS));
                }
            }
        }

        // Can take ~1 min per growth target, multithreading should help
        growthTargets.stream().forEach(
                t -> map.put(t, generate(t))
        );

        return map;
    }


    public Collection<WeightMap> generate(GrowthTarget target)
    {
        StopWatch stopWatch = StopWatch.createStarted();

        // DISABLED
        // Special case for random generation
        // Use conventional random strategy; generating tests with, 20%, 40%, 60% and 80% of cases.
        // Target a number of suites equal to the number of tests

        if(target.getPrimaryCriterion().equals(GrowthTarget.Criterion.RANDOM_PRESET))
        {
            Set<WeightMap> randomlyGenerated = RandomWeightMapGenerator.generateRandomWeightMapsBySizeIntervals(
                    new ArrayList<>(testPool),
                    testPool.size(),
                    new double[]{0.2, 0.4, 0.6, 0.8},
                    random.nextInt(1000)
            );

            for (WeightMap g : randomlyGenerated)
                g.setGrowthTarget(target);

            return randomlyGenerated;
        }

        // Generate
        Collection<WeightMap> allRepetitions = new ArrayList<>();
        for (int i = 0; i < iterationLimit; i++)
        {
            WeightMap empty = new WeightMap(testPool);
            empty.disableAllTests();
            Collection<WeightMap> toAdd = generateRecursiveStep(target, empty);
            //String counts = weightMapSizes(toAdd);
            allRepetitions.addAll(toAdd);
            // Quit once target achieved
            if (target.growthComplete())
                break;
        }

        // Drop equivalent WeightMaps
        allRepetitions = removeDuplicates(allRepetitions);

        stopWatch.stop();

        System.out.println("Generated " + allRepetitions.size() + " suites for " + target.toString() + " in " + stopWatch.toString());

        return allRepetitions;
    }

    private Collection<WeightMap> removeDuplicates(Collection<WeightMap> weightMaps)
    {
        Collection<WeightMap> unique = new HashSet<>(weightMaps);
        return unique;
    }

    private String weightMapSizes(Collection<WeightMap> weightMaps)
    {
        Map<Integer, Integer> counts = new TreeMap<>();
        for (WeightMap wm : weightMaps)
        {
            Integer size = wm.getEnabledTests().size();
            if (!counts.containsKey(size))
                counts.put(size, 0);
            counts.put(size, counts.get(size) + 1);
        }

        StringBuilder sb = new StringBuilder();
        if (counts.values().stream().allMatch(v -> v == 1))
            sb.append("[One of each!] ");
        for (Integer size : counts.keySet())
        {
            sb.append("{");
            sb.append("Size: ");
            sb.append(size);
            sb.append(", ");
            sb.append("Suites: ");
            sb.append(counts.get(size));
            sb.append("}");
        }
        return sb.toString();
    }

    private Collection<WeightMap> generateRecursiveStep(GrowthTarget target, WeightMap alreadySelected)
    {
        Collection<UnitTest> improvingTests = findTestsThatImproveTarget(alreadySelected, target);
        // No improving tests remain;
        if(improvingTests.isEmpty())
            return Collections.emptySet();

        // Make improved suite
        WeightMap nextStep = new WeightMap(alreadySelected);
        nextStep.enableTest(randomlySelectTest(improvingTests));
        nextStep.setGrowthTarget(target);

        // Add (copy of) suite to GrowthTarget if adequacy for current Criterion is found & reset current generation
        if(suiteIsAdequate(nextStep, target))
        {
            target.addAdequateSuite(new WeightMap(nextStep));
            // Reset current generation; will need to allow for potential continued generation
            nextStep.disableAllTests();
        }

        Collection<WeightMap> improvedSuites = new HashSet<>();
        if(target.getAdequateSuites().isEmpty())
        {
            improvedSuites.add(new WeightMap(nextStep));
        }
        else
        {
            WeightMap nextStepWithAdequateSuites = new WeightMap(nextStep);
            // Add each enabled test of adequate suites to new next step
            target.getAdequateSuites().stream()
                    .map(WeightMap::getEnabledTests) // enabled tests of each adequate suite
                    .forEach(ts -> ts.forEach(nextStepWithAdequateSuites::enableTest));
            improvedSuites.add(nextStepWithAdequateSuites);
        }

        // Generation should stop (i.e. recursion should end) - last step
        if(improvingTests.size() <= 1 || target.growthComplete())
            return improvedSuites;

        // Generation continues
        improvedSuites.addAll(generateRecursiveStep(target, nextStep));
        return improvedSuites;
    }

    private boolean suiteIsAdequate(WeightMap toCheck, GrowthTarget target)
    {
        if(target.getCurrentCriterion().equals(GrowthTarget.Criterion.RANDOM) ||
            target.getCurrentCriterion().equals(GrowthTarget.Criterion.RANDOM_PRESET)
        )
            return false;

        if(target.getCurrentCriterion().equals(GrowthTarget.Criterion.COVERAGE))
        {
            CoverWeight cw = generateCoverWeightForWeightMap(toCheck);
            HitCounts hc = getHitCountsForCoverWeight(cw, toCheck);

            if(hc.coveredLineRatio() >= 1.0)
                return true;
            return false;
        }

        // Mutation
        if(mutantResults.containsKey(target.getMutationTool()))
        {
            TestResultsMap matched = mutantResults.get(target.getMutationTool());
            if(matched.faultDetectionRate(toCheck, true) >= 1.0)
                return true;
            return false;
        }
        throw new NoSuchElementException("No matching mutation tool found!");
    }


    private UnitTest randomlySelectTest(Collection<UnitTest> choices)
    {
        return choices.stream().skip(random.nextInt(choices.size())).findFirst().get();
    }

    private Collection<UnitTest> findTestsThatImproveTarget(WeightMap alreadySelected, GrowthTarget target)
    {
        CoverWeight alreadySelectedCW = generateCoverWeightForWeightMap(alreadySelected);
        HitCounts alreadySelectedHC = getHitCountsForCoverWeight(alreadySelectedCW, alreadySelected);

        return testPool.stream()
                // Filter out tests that have already been selected
                .filter(t -> !alreadySelected.getEnabledTests().contains(t))
                // Also remove tests that are in already adequate sub-suites (for stacking / criterion switch)
                .filter(t -> target.getAdequateSuites().stream().noneMatch(
                        as -> as.getEnabledTests().contains(t)
                ))
                // Only keep tests that improve the target
                .filter(t -> testImprovesTarget(t, alreadySelectedHC, target.getCurrentCriterion(), target.getMutationTool()))
                .collect(Collectors.toSet());
    }

    private boolean testImprovesTarget(UnitTest toCheck, HitCounts alreadySelected, GrowthTarget.Criterion criterion, String mutationTool)
            throws IllegalArgumentException
    {
        // Random will just select any test
        if(criterion.equals(GrowthTarget.Criterion.RANDOM) ||
                criterion.equals(GrowthTarget.Criterion.RANDOM_PRESET)
        )
            return true;

        WeightMap weightMapToCheck = new WeightMap(alreadySelected.getWeightMap());
        weightMapToCheck.put(toCheck, 1.0);
        weightMapToCheck.update();

        CoverWeight coverWeightToCheck = generateCoverWeightForWeightMap(weightMapToCheck);
        HitCounts toCheckHC = getHitCountsForCoverWeight(coverWeightToCheck, weightMapToCheck);

        if (criterion.equals(GrowthTarget.Criterion.COVERAGE))
        {

            if(toCheckHC.coveredLineRatio() > alreadySelected.coveredLineRatio())
                return true;
            return false;
        }

        if (criterion.equals(GrowthTarget.Criterion.MUTATION))
        {
            if(mutantResults != null && !mutantResults.isEmpty())
            {
                if(mutantResults.containsKey(mutationTool))
                {
                    double alreadySelectedMutationScore = mutantResults.get(mutationTool).faultDetectionRate(alreadySelected.getWeightMap(), true);
                    double toCheckMutationScore = mutantResults.get(mutationTool).faultDetectionRate(toCheckHC.getWeightMap(), true);
                    if(toCheckMutationScore > alreadySelectedMutationScore)
                        return true;
                    return false;
                }
            }
            throw new IllegalArgumentException("Tried to get mutation results when no mutants exist.");
        }

        throw new IllegalArgumentException("GrowthTarget not permitted for testImprovesTarget; must be either MUTATION or COVERAGE.");
    }

    private CoverWeight generateCoverWeightForWeightMap(WeightMap toUse)
    {
        CoverWeight c = coverWeight.clone();
        c.setWeightMaps(Collections.singletonList(toUse));
        c.run();
        return c;
    }

    private HitCounts getHitCountsForCoverWeight(CoverWeight cw, WeightMap w)
    {
        return cw.getHitCounts(cw.getModelCuts().get(0), w);
    }


}
