package coverweight.suitebias;

import coverweight.CoverWeight;
import coverweight.configuration.CLIReader;
import coverweight.configuration.Global;
import coverweight.configuration.OptionNames;
import coverweight.mistakesim.checkresults.GradeerResultsParser;
import coverweight.mistakesim.checkresults.TestResultsMap;
import coverweight.output.OutputFile;
import coverweight.sourcefiles.ClassUnderTest;
import coverweight.sourcefiles.UnitTest;
import coverweight.suitebias.experiments.StatisticAnalysisExperiment;
import coverweight.suitebias.experiments.SuiteResultsExperiment;
import coverweight.suitebias.experiments.TestResultsExperiment;
import coverweight.weighting.GrowthTarget;
import coverweight.weighting.WeightMap;
import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;

public class SuiteBiasStudy
{
    private static final int DEFAULT_RANDOM_SUITE_COUNT = 50;
    private static final int DEFAULT_REPEATED_RUNS = 100;
    private int repeatedRuns = DEFAULT_REPEATED_RUNS;

    private static final int QUICK_NUMBER_CUTS = 5;
    private static final int QUICK_NUMBER_TESTS = 120;

    private static final boolean ENABLE_SIZE_GENERATION = true;
    private static final boolean ENABLE_SIMPLE_RANDOM_GENERATION = false;

    private static final int BASE_RANDOM_SEED = 52474897;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private int randomSuiteCount = DEFAULT_RANDOM_SUITE_COUNT;

    private static final boolean INCLUDE_ALL_TESTS_ENABLED = true;

    private final double[] SUITE_SIZE_INTERVALS = new double[] {0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8};

    public static void main(String[] args)
    {

        // Execute study with CLI arguments
        CLIReader cli = new CLIReader(args);
        try
        {
            // TODO may need to load in mutants for mutation score results
            // TODO (support different kinds of mutants?)
            SuiteBiasStudy suiteBiasStudy = new SuiteBiasStudy(cli);
        }
        catch (NullPointerException npEx)
        {
            npEx.printStackTrace();
            OutputFile errorFile = new OutputFile("NullPointerExceptions", "NullPointerExceptions", ".txt")
            {
                @Override
                public void generateLines()
                {
                    addLine(npEx.getMessage());
                    StackTraceElement[] stackTraceElements = npEx.getStackTrace();
                    for (StackTraceElement e : stackTraceElements)
                    {
                        addLine(e.toString());
                    }
                }
            };
            errorFile.write();
        }
    }

    /**
     * Run test suite bias study for a defined CUT
     */
    public SuiteBiasStudy(CLIReader cli) throws NullPointerException
    {
        StopWatch totalStopWatch = StopWatch.createStarted();

        CoverWeight coverWeight = initialCoverWeight(cli);

        // Make a copy with only naive weights to use as baseline
        //CoverWeight balanced = coverWeight.clone();
        //balanced.run();

        // Load mutant results from Gradeer
        GradeerResultsParser gradeerResultsParser = new GradeerResultsParser(coverWeight.getUnitTests());
        Collection<TestResultsMap> mutantResults = new ArrayList<>();
        try
        {
            mutantResults = gradeerResultsParser.parse(
                    cli.getInputValue(OptionNames.GRADEER_CSV_MUTANTS));
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        assert (!mutantResults.isEmpty());
        if(mutantResults.isEmpty())
            System.exit(1);
        if(mutantResults.iterator().next().isEmpty())
            System.exit(2);

        for (TestResultsMap tr : mutantResults)
        {
            tr.removeUndetectable();
            tr.mergeIdentical();
            assert (!tr.getAllGradeerSolutions().isEmpty());
        }


        // Run tests
        coverWeight.run();
        reportInvalidTests(coverWeight);

        // Store results of tests
        TestResultsExperiment testResultsExperiment = new TestResultsExperiment(coverWeight);
        testResultsExperiment.run();

        // Growth generation repetitions
        for (int run = 1; run <= repeatedRuns; run++)
        {
            StopWatch sizeGenStopWatch = StopWatch.createStarted();
            subStudy(coverWeight, run, mutantResults);
            sizeGenStopWatch.stop();
            logger.info("SizeGen time: " + sizeGenStopWatch.toString());
        }


        totalStopWatch.stop();
        logger.info("Finished whole data generation in " + totalStopWatch.toString());
    }

    private void applyQuickRunMode(CoverWeight coverWeight, CLIReader cli)
    {
        boolean quickRunMode = false;
        if(cli.optionIsSet(OptionNames.QUICK_RUN_MODE))
            quickRunMode = true;

        if(quickRunMode)
        {
            logger.info("Quick-Run mode enabled!");
            repeatedRuns = 2;

            Set<ClassUnderTest> enabledCuts = new HashSet<>(coverWeight.getModelCuts());
            Set<UnitTest> enabledTests = new HashSet<>();

            for (int i = 0; i < QUICK_NUMBER_CUTS; i++)
            {
                if(coverWeight.getCuts().isEmpty())
                    break;
                Collections.shuffle(coverWeight.getCuts());
                ClassUnderTest selected = coverWeight.getCuts().get(0);
                enabledCuts.add(selected);
                coverWeight.getCuts().remove(selected);
            }

            for (int i = 0; i < QUICK_NUMBER_TESTS; i++)
            {
                if(coverWeight.getUnitTests().isEmpty())
                    break;
                Collections.shuffle(coverWeight.getUnitTests());
                UnitTest selected = coverWeight.getUnitTests().get(0);
                enabledTests.add(selected);
                coverWeight.getUnitTests().remove(selected);
            }

            coverWeight.setCuts(new ArrayList<>(enabledCuts));
            coverWeight.setUnitTests(new ArrayList<>(enabledTests));
        }
    }

    private void subStudy(CoverWeight coverWeight, int runNumber, Collection<TestResultsMap> mutantResults)
    {
        Global.setCurrentResultsSubDir("SizeGeneration/" + String.format("%03d", runNumber));
        System.out.println();
        logger.info("Running analysis for size-based suite generation (" + runNumber + ")...");

        // Generate combinations of WeightMaps to represent different test suites
        // Only simple weights of 1 (enabled) and 0 (disabled)
        logger.info("Generating size-based WeightMaps");
        GrowingWeightmapGenerator growingWeightmapGenerator = new GrowingWeightmapGenerator(
                coverWeight,
                coverWeight.getUnitTests(),
                mutantResults,
                runNumber
        );
        //growingWeightmapGenerator.setIterationLimit(1);
        Map<GrowthTarget, Collection<WeightMap>> generatedWeightMaps = growingWeightmapGenerator.generateForAllGrowthTargets();
        final List<WeightMap> allGenerated = mergeAllWeightMaps(generatedWeightMaps.values());

        CoverWeight cw = coverWeight.clone();
        updateWeightMaps(cw, allGenerated, runNumber);
        runExperiments(cw, mutantResults, runNumber);

    }

    private List<WeightMap> mergeAllWeightMaps(Collection<Collection<WeightMap>> weightMapCollections)
    {
        List<WeightMap> merged = new ArrayList<>();

        for (Collection<WeightMap> collection : weightMapCollections)
        {
            merged.addAll(collection);
        }

        return merged;
    }


    private CoverWeight initialCoverWeight(CLIReader cli)
    {
        // Load experiment-specific args
        if(cli.optionIsSet(OptionNames.SUITE_BIAS_RANDOM_TARGET_COUNT))
            randomSuiteCount = Integer.parseInt(cli.getInputValue(OptionNames.SUITE_BIAS_RANDOM_TARGET_COUNT));

        // Load a CoverWeight instance with standard args
        CoverWeight coverWeight = new CoverWeight(cli, new HashSet<>());

        // If in quick-run mode, reduce number of tests and CUTs now.
        applyQuickRunMode(coverWeight, cli);

        // Make default WeightMap not normalized
        WeightMap wm = new WeightMap(coverWeight.getUnitTests());
        List<WeightMap> wmList = new ArrayList<>();
        wmList.add(wm);
        coverWeight.setWeightMaps(wmList);

        return coverWeight;
    }

    private void reportInvalidTests(CoverWeight coverWeight)
    {
        // Report invalid tests
        InvalidTestReporter invalidTestReporter = new InvalidTestReporter(coverWeight);
        invalidTestReporter.display();
        invalidTestReporter.write();
    }

    private void updateWeightMaps(CoverWeight cw, List<WeightMap> newWeightMaps, int runIteration)
    {

        // Include run iteration in name
        int i = 1;
        for (WeightMap wm : newWeightMaps)
        {
            wm.setName("Run" + String.format("%02d", runIteration) + "_" + String.format("%05d", i));
            i ++;
        }

        if (INCLUDE_ALL_TESTS_ENABLED)
        {
            WeightMap allTestsWeightMap = new WeightMap(cw.getUnitTests());
            allTestsWeightMap.setName("AllTestsEnabled");
            allTestsWeightMap.getTotalWeights(); // Set total weights
            newWeightMaps.add(allTestsWeightMap);
        }

        cw.setWeightMaps(Collections.unmodifiableList(newWeightMaps));
        cw.run();
    }

    private void runExperiments(CoverWeight coverWeight, Collection<TestResultsMap> mutantResults, int runNumber)
    {
        for (WeightMap weightMap : coverWeight.getWeightMaps())
            if(weightMap.getTotalWeights() < 1)
                logger.info(weightMap.toString());

        logger.info("Running multi-suite analysis...");
        StatisticAnalysisExperiment statisticAnalysisExperiment = new StatisticAnalysisExperiment(coverWeight, mutantResults, runNumber);
        statisticAnalysisExperiment.run();

        logger.info("Storing test execution results...");
        SuiteResultsExperiment suiteResultsExperiment = new SuiteResultsExperiment(coverWeight);
        suiteResultsExperiment.run();
    }

}
