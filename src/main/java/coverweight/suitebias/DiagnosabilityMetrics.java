package coverweight.suitebias;

import coverweight.report.HitCounts;
import coverweight.runner.TestResult;
import coverweight.runner.jacoco.CoverageResult;
import coverweight.sourcefiles.UnitTest;
import coverweight.weighting.WeightMap;

import java.util.*;
import java.util.stream.Collectors;

public class DiagnosabilityMetrics
{
    public static double densityUnnormalised(HitCounts hitCounts)
    {
        List<TestResult> enabledTestResults = getEnabledTestResults(hitCounts);
        if (enabledTestResults.isEmpty())
            return Double.NaN;
        // Numerator
        int sum = 0;
        for (TestResult tr : enabledTestResults)
            sum += tr.getCoverage().getCoveredLines().size();

        // Denominator
        int denominator = enabledTestResults.size() *
                getAllComponentLines(enabledTestResults.get(0)).size();

        return (double) sum / (double) denominator;
    }

    public static double density(HitCounts hitCounts)
    {
        return 1 - Math.abs(1 - (2 * densityUnnormalised(hitCounts)));
    }

    public static double diversity(HitCounts hitCounts)
    {
        int numerator = 0;
        for (Set<TestResult> ts : activityGroupedTests(hitCounts))
            numerator += ts.size() * (ts.size() - 1);

        int bigN = hitCounts.getWeightMap().getEnabledTests().size();
        int denominator = bigN * (bigN - 1);

        return 1 - ((double) numerator / (double) denominator);
    }

    public static double uniqueness(HitCounts hitCounts)
    {
        WeightMap w = hitCounts.getWeightMap();
        if(w.getEnabledTests().isEmpty())
            System.err.println("WeightMap has no enabled tests for " + hitCounts.toString());
        int bigM = getAllComponentLines(getEnabledTestResults(hitCounts).get(0)).size();
        return (double) activityGroupedLines(hitCounts).size() / (double) bigM;

    }

    public static double ddu(HitCounts hitCounts)
    {
        return density(hitCounts) * diversity(hitCounts) * uniqueness(hitCounts);
    }

    private static Collection<Set<TestResult>> activityGroupedTests(HitCounts hitCounts)
    {
        Collection<Set<TestResult>> mainSet = new HashSet<>();
        Queue<TestResult> toProcess = new ArrayDeque<>(getEnabledTestResults(hitCounts));
        // Process every individual
        while (!toProcess.isEmpty())
        {
            TestResult tr = toProcess.remove();
            // Try to find existing set with same activity as tr
            boolean matched = false;
            for(Set<TestResult> setToCheck : mainSet)
            {
                if(sharesActivity(tr, setToCheck))
                {
                    setToCheck.add(tr);
                    matched = true;
                    break;
                }
            }
            // No exist set contains tr
            if (!matched)
            {
                Set<TestResult> newSet = new HashSet<>();
                newSet.add(tr);
                mainSet.add(newSet);
            }
        }

        return mainSet;
    }

    private static Collection<Set<Integer>> activityGroupedLines(HitCounts hitCounts)
    {
        Collection<Set<Integer>> mainSet = new HashSet<>();
        List<TestResult> enabledTestResults = getEnabledTestResults(hitCounts);
        // TODO implement

        Queue<Integer> toProcess = new ArrayDeque<>(
                getAllComponentLines(enabledTestResults.get(0))
        );
        // Process every individual
        while (!toProcess.isEmpty())
        {
            Integer line = toProcess.remove();
            // Try to find existing set with same activity as tr
            boolean matched = false;
            for(Set<Integer> setToCheck : mainSet)
            {
                if(sharesActivity(line, setToCheck, enabledTestResults))
                {
                    setToCheck.add(line);
                    matched = true;
                    break;
                }
            }
            // No exist set contains tr
            if (!matched)
            {
                Set<Integer> newSet = new HashSet<>();
                newSet.add(line);
                mainSet.add(newSet);
            }
        }

        return mainSet;
    }

    private static boolean sharesActivity(TestResult testResultToMatch, Set<TestResult> setToCheck)
    {
        CoverageResult toMatch = testResultToMatch.getCoverage();
        CoverageResult toCheck = setToCheck.stream().findFirst().get().getCoverage();

        if(toMatch.getCoveredLines().equals(toCheck.getCoveredLines()) &&
                toMatch.getUncoveredLines().equals(toCheck.getUncoveredLines()))
        {
            return true;
        }
        return false;
    }

    private static boolean sharesActivity(Integer toMatch,
                                          Set<Integer> setToCheck,
                                          Collection<TestResult> testResults)
    {
        Integer toCheck = setToCheck.stream().findFirst().get();

        // Check both lines are covered the same for every test; return false once a mismatch found
        for (TestResult tr : testResults)
        {
            CoverageResult cr = tr.getCoverage();

            if(cr.getCoveredLines().contains(toMatch) != cr.getCoveredLines().contains(toCheck))
                return false;
            if(cr.getUncoveredLines().contains(toMatch) != cr.getUncoveredLines().contains(toCheck))
                return false;
        }
        // No mismatch detected, the lines share activity
        return true;
    }

    private static List<TestResult> getEnabledTestResults(HitCounts hitCounts)
    {
        Collection<UnitTest> enabledTests = hitCounts.getWeightMap().getEnabledTests();
        return hitCounts.getTestResults().stream()
                .filter(tr -> enabledTests.contains(tr.getTest()))
                .collect(Collectors.toList());
    }

    private static Collection<Integer> getAllComponentLines(TestResult enabledTestResult)
    {
        Collection<Integer> lines = new HashSet<>();
        lines.addAll(enabledTestResult.getCoverage().getCoveredLines());
        lines.addAll(enabledTestResult.getCoverage().getUncoveredLines());
        return lines;
    }

}
