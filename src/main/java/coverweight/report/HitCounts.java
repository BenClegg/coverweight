package coverweight.report;

import coverweight.mistakesim.checkresults.GradeerSolution;
import coverweight.mistakesim.checkresults.TestResultsMap;
import coverweight.runner.TestResult;
import coverweight.runner.jacoco.CoverageResult;
import coverweight.runner.jacoco.JacocoAnalyser;
import coverweight.sourcefiles.ClassUnderTest;
import coverweight.sourcefiles.UnitTest;
import coverweight.weighting.WeightMap;
import org.apache.commons.math3.stat.descriptive.moment.Variance;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.stream.Collectors;

public class HitCounts extends ConcurrentHashMap<Integer, Double>
{
    private ClassUnderTest classUnderTest;
    private WeightMap weightMap;

    private Set<Integer> coveredLines;
    private Set<Integer> uncoveredLines;

    Collection<TestResult> testResults;

    public HitCounts(ClassUnderTest cut, WeightMap weights, Collection<TestResult> testResultList)
    {
        classUnderTest = cut;
        weightMap = weights;
        coveredLines = new ConcurrentSkipListSet<>();
        uncoveredLines = new ConcurrentSkipListSet<>();

        testResults = testResultList;
        testResults.forEach(tr -> {
            if(tr.getClassUnderTest().equals(classUnderTest))
            {
                addCovered(tr);
                addUncovered(tr);

            }
        });
    }

    /**
     * Pseudo-HitCounts, used only to derive grades, and access a ClassUnderTest
     * @param gradeerSolution
     * @param testResultsMap
     * @param weightMap
     */
    public HitCounts(GradeerSolution gradeerSolution, TestResultsMap testResultsMap, WeightMap weightMap)
    {
        this.classUnderTest = new ClassUnderTest(gradeerSolution);
        this.testResults = new HashSet<>();
        this.weightMap = weightMap;

        for (UnitTest t : weightMap.getEnabledTests())
        {
            testResults.add(new TestResult(classUnderTest, t, testResultsMap.testDetectsSolution(t, gradeerSolution)));
        }

        this.coveredLines = null;
        this.uncoveredLines = null;

    }

    private void addCovered(TestResult testResult)
    {
        for (int lineNumber : testResult.getCoverage().getCoveredLines())
        {
            coveredLines.add(lineNumber);
            uncoveredLines.remove(lineNumber);

            double weight = weightMap.get(testResult.getTest());
            if(containsKey(lineNumber))
            {
                double existing = get(lineNumber);
                put(lineNumber, existing + weight);
            }
            else
            {
                put(lineNumber, weight);
            }
        }
    }

    private void addUncovered(TestResult testResult)
    {
        for (int lineNumber : testResult.getCoverage().getUncoveredLines())
        {
            if(!coveredLines.contains(lineNumber))
                uncoveredLines.add(lineNumber);
            // Add uncovered line if it doesn't exist, but it will have a hit count of 0
            if(!containsKey(lineNumber))
                put(lineNumber, 0.0);
            // Do nothing otherwise, no need to modify if it's already known
        }
    }

    public ClassUnderTest getClassUnderTest()
    {
        return classUnderTest;
    }

    @Override
    public String toString()
    {
        return "HitCounts{" +
                "classUnderTest=" + classUnderTest +
                ", VarianceOfAllHitCounts=" + getVarianceAll() +
                ", VarianceOfCoveredHitCounts=" + getVarianceCoveredLines() +
                ", weightMap=" + weightMap +
                '}';
    }

    public List<String> weightedCoverageSummary()
    {


        List<String> lineCoverage = new ArrayList<>();

        for (int l : keySet())
        {
            lineCoverage.add("Line " + l + ": " + get(l));
        }
        return lineCoverage;
    }


    public double getVarianceAll()
    {
        if(values().isEmpty())
            return Double.NaN;
        double[] coverageArray = values().stream().mapToDouble(Double::doubleValue).toArray();

        Variance variance = new Variance();
        return variance.evaluate(coverageArray);
    }

    /**
     * Calculate variance of only lines with positive weighted hit counts. These are generally lines that have been covered.
     * @return
     */
    public double getVarianceCoveredLines()
    {
        if (coveredLines == null)
            return Double.NaN;
        if(coveredLines.isEmpty())
            return Double.NaN;
        double[] coverageArray = coveredLines.stream().mapToDouble(this::get).toArray();

        Variance variance = new Variance();
        return variance.evaluate(coverageArray);
    }

    public String simpleStatistics()
    {
        StringBuilder sb = new StringBuilder();

        sb.append(String.format("%-65s", weightMap.getName()));
        sb.append("| ");

        sb.append("Variance (All): ");
        sb.append(String.format("%-20s", getVarianceAll()));
        sb.append("| ");

        sb.append("Variance (Covered): ");
        sb.append(String.format("%-20s", getVarianceCoveredLines()));
        sb.append("| ");

        sb.append("All Weights >0?: ");
        sb.append(weightMap.allWeightsGreaterThanZero());
        sb.append("| ");

        sb.append("Grade: ");
        sb.append(generateGrade());

        return sb.toString();
    }

    public WeightMap getWeightMap()
    {
        return weightMap;
    }

    public double generateGrade()
    {
        if(weightMap == null)
        {
            System.out.println("WeightMap is null for HitCounts " + this.toString());
        }
        double grade = 0.0;
        for (TestResult tr : testResults)
        {
            if(tr.isPasses() && tr.getClassUnderTest().equals(classUnderTest))
                grade += weightMap.get(tr.getTest());
        }
        return (grade / weightMap.getTotalWeights()) * 100;
    }

    public Set<Integer> getCoveredEnabledLines()
    {
        Set<Integer> enabledCovered = new HashSet<>();
        for (Integer l : this.keySet())
        {
            if(this.get(l) > 0.0)
                enabledCovered.add(l);
        }
        return enabledCovered;
    }

    public double getSumRecoverage()
    {
        double redundantCoverage = 0;

        for (int lineNumber : this.keySet())
        {
            if (this.get(lineNumber) > 1)
                redundantCoverage += (this.get(lineNumber) - 1);
        }

        return redundantCoverage;
    }

    public double coveredLineRatio()
    {
        double totalLines = this.keySet().size();
        return this.keySet().stream().filter(l -> this.get(l) > 0).count() / totalLines;
    }

    public Collection<TestResult> getTestResults()
    {
        return testResults;
    }

    public boolean allEnabledTestsPass()
    {
        return activeTestResults().stream().allMatch(TestResult::isPasses);
    }
    public long getPassingEnabledTestsCount()
    {
        return activeTestResults().stream().filter(TestResult::isPasses).count();
    }
    public long getFailingEnabledTestsCount()
    {
        return activeTestResults().stream().filter(tr -> !tr.isPasses()).count();
    }

    private Collection<TestResult> activeTestResults()
    {
        return testResults.stream().filter(tr -> {
            UnitTest t = tr.getTest();
            return (weightMap.get(t) > 0);
        }).collect(Collectors.toList());
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        HitCounts hitCounts = (HitCounts) o;
        return Objects.equals(classUnderTest, hitCounts.classUnderTest) &&
                Objects.equals(weightMap, hitCounts.weightMap) &&
                Objects.equals(coveredLines, hitCounts.coveredLines) &&
                Objects.equals(uncoveredLines, hitCounts.uncoveredLines) &&
                Objects.equals(testResults, hitCounts.testResults);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(super.hashCode(), classUnderTest, weightMap, coveredLines, uncoveredLines, testResults);
    }
}
