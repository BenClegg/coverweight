package coverweight.configuration;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class PathLoader
{
    public static List<Path> loadPaths(String allPaths)
    {
        List<Path> paths = new ArrayList<>();

        String[] pathStrings = allPaths.split(",");
        for (String p : pathStrings)
        {
            paths.add(Paths.get(p));
        }
        return paths;
    }

    public static Path loadFirstPath(String allPaths)
    {
        return loadPaths(allPaths).get(0);
    }
}
