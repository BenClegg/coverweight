package coverweight.configuration;

import java.io.File;

public class Global
{
    public static String HOME_DIR = "/var/tmp/CoverWeight";
    public static String LIB_DIR = HOME_DIR + File.separator + "lib";

    private static String currentResultsSubDir = "";

    public static String ANT_HOME = "/usr/share/ant";
    public static String MVN_REPOSITORY_DIR = "/usr/share/maven-repo";

    public static int PER_TEST_TIMEOUT = 5;

    public static void init()
    {
        loadEnvVars();

        new File(HOME_DIR).mkdirs();
        new File(LIB_DIR).mkdirs();
    }

    public static void setHomeDir(File dir)
    {
        HOME_DIR = dir.getPath();
        if(HOME_DIR.endsWith(File.separator))
            HOME_DIR = HOME_DIR.substring(0, HOME_DIR.length() - 1);
        LIB_DIR = HOME_DIR + File.separator + "lib";

        init();
    }

    public static void setAntHome(File dir)
    {
        ANT_HOME = dir.getPath();
        if(ANT_HOME.endsWith(File.separator))
            ANT_HOME = ANT_HOME.substring(0, ANT_HOME.length() - 1);
    }

    public static void setMvnRepositoryDir(File dir)
    {
        MVN_REPOSITORY_DIR = dir.getPath();
        if(MVN_REPOSITORY_DIR.endsWith(File.separator))
            MVN_REPOSITORY_DIR = MVN_REPOSITORY_DIR.substring(0, MVN_REPOSITORY_DIR.length() - 1);
    }


    private static void loadEnvVars()
    {
        if (!getEnvVar("ANT_HOME").isEmpty())
            ANT_HOME = getEnvVar("ANT_HOME");
        if (!getEnvVar("MVN_REPOSITORY_DIR").isEmpty())
            MVN_REPOSITORY_DIR = getEnvVar("MVN_REPOSITORY_DIR");
    }

    private static String getEnvVar(String envIdentifier)
    {
        ProcessBuilder pb = new ProcessBuilder();
        String result = pb.environment().get(envIdentifier);
        if (result != null)
        {
            if (!result.isEmpty())
            {
                return result;
            }
        }
        return "";
    }

    public static String getResultsDir()
    {
        if(currentResultsSubDir.isEmpty())
            return HOME_DIR + File.separator + "results";
        else
            return HOME_DIR + File.separator + "results" + File.separator + currentResultsSubDir;
    }

    public static void setCurrentResultsSubDir(String subDir)
    {
        currentResultsSubDir = subDir;
    }
}
