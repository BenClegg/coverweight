package coverweight.configuration;

import org.apache.commons.cli.*;

import java.io.File;

public class CLIReader
{
    private CommandLine cli;
    private Options opts;

    private void initOptions()
    {
        opts = new Options();

        opts.addOption(Option.builder(OptionNames.CUT_SHORT)
                .longOpt(OptionNames.CUT)
                .hasArg()
                .desc("Paths to subjects under test.")
                .build());

        opts.addOption(Option.builder(OptionNames.MODEL_CUT_DIR)
                .hasArg()
                .desc("Paths to directories containing model/golden subjects under test.")
                .build());

        opts.addOption(Option.builder(OptionNames.MODEL_CUT_RELATIVE_FILE)
                .hasArg()
                .desc("Relative local path to target sourcefile of model solution")
                .build());

        opts.addOption(Option.builder(OptionNames.MUTANT_CUT_DIR)
                .hasArg()
                .desc("Paths to directories containing mutant subjects under test.")
                .build());

        opts.addOption(Option.builder(OptionNames.TESTS_SHORT)
                .longOpt(OptionNames.TESTS)
                .hasArg()
                .desc("Path to tests.")
                .build());

        opts.addOption(Option.builder(OptionNames.BUILT_CLASSES)
                .hasArg()
                .desc("Path to .class dir of classes.")
                .build());

        opts.addOption(Option.builder(OptionNames.BUILT_TESTS)
                .hasArg()
                .desc("Path to .class dir of tests.")
                .build());

        opts.addOption(Option.builder(OptionNames.WEIGHT_CSVS)
                .hasArg()
                .desc("Paths of directories containing CSVs that define test weights.")
                .build());

        opts.addOption(Option.builder(OptionNames.NESTED_CUTS)
                .desc("Defined path contains directories that each contain one CUT")
                .build());

        opts.addOption(Option.builder(OptionNames.CUT_FILENAME)
                .hasArg()
                .desc("Name of .java file that CUT is identified as.")
                .build());

        /*
        opts.addOption(Option.builder(OptionNames.SUITE_BIAS_ALL_COMBINATIONS)
                .desc("Generate all possible suites to use in suite bias experiment. Warning: very slow, untested.")
                .build());
                */
        opts.addOption(Option.builder(OptionNames.SUITE_BIAS_RANDOM_TARGET_COUNT)
                .hasArg()
                .desc("Target number of suites to randomly generate in suite bias experiment.")
                .build());

        opts.addOption(Option.builder(OptionNames.SUITE_BIAS_SUITE_SIZES)
                .hasArg()
                .desc("The sizes of test suites, separated by commas.")
                .build());

        opts.addOption(Option.builder(OptionNames.QUICK_RUN_MODE)
                .desc("Run with a reduced set of tests and CUTs and reduced repetition.")
                .build());

        opts.addOption(Option.builder(OptionNames.CUSTOM_WORKING_DIR)
                .hasArg()
                .desc("Manually define a working dir other than /var/tmp/CoverWeight.")
                .build());

        opts.addOption(Option.builder(OptionNames.CUSTOM_ANT_HOME)
                .hasArg()
                .desc("Manually define an Ant dir other than /usr/share/ant.")
                .build());

        opts.addOption(Option.builder(OptionNames.CUSTOM_MAVEN_REPO)
                .hasArg()
                .desc("Manually define a Maven repo dir other than /usr/share/maven-repo.")
                .build());

        // Gradeer CSV inputs
        opts.addOption(Option.builder(OptionNames.GRADEER_CSV_STUDENT)
                .hasArg()
                .desc("Location of Gradeer output CSV for students' solutions.")
                .build());
        opts.addOption(Option.builder(OptionNames.GRADEER_CSV_MUTANTS)
                .hasArg()
                .desc("Locations of Gradeer output CSVs for mutants' solutions.")
                .build());
    }

    public CLIReader(String[] args)
    {
        initOptions();

        CommandLineParser commandLineParser = new DefaultParser();

        try
        {
            cli = commandLineParser.parse(opts, args);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
            System.err.println("Unexpected parsing error, check configuration.");
            System.exit(1);
        }

        if(optionIsSet(OptionNames.CUSTOM_WORKING_DIR))
            Global.setHomeDir(new File(getInputValue(OptionNames.CUSTOM_WORKING_DIR)));
        if(optionIsSet(OptionNames.CUSTOM_ANT_HOME))
            Global.setAntHome(new File(getInputValue(OptionNames.CUSTOM_ANT_HOME)));
        if(optionIsSet(OptionNames.CUSTOM_MAVEN_REPO))
            Global.setMvnRepositoryDir(new File(getInputValue(OptionNames.CUSTOM_MAVEN_REPO)));
    }

    public String getInputValue(String optStr) throws IllegalArgumentException
    {
        if(cli.hasOption(optStr))
        {
            return cli.getOptionValue(optStr);
        }
        Option o = opts.getOption(optStr);
        StringBuilder msg = new StringBuilder();
        msg.append("Option not set: (-" + o.getOpt());

        if(o.hasLongOpt())
            msg.append(" / --" + o.getLongOpt());
        msg.append(") - " + o.getDescription());

        throw new IllegalArgumentException(msg.toString());
    }

    public boolean optionIsSet(String optStr)
    {
        if(cli.hasOption(optStr))
            return true;
        return false;
    }
}
