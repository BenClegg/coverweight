package coverweight.configuration;

public class OptionNames
{
    public static final String CUT = "sourceUnderTest";
    public static final String CUT_SHORT = "s";

    public static final String MODEL_CUT_DIR = "modelSourceDir";
    public static final String MODEL_CUT_RELATIVE_FILE = "modelSourceRelativeFile";

    public static final String MUTANT_CUT_DIR = "mutantSourceDir";


    public static final String CUT_FILENAME = "sourceFilename";
    public static final String TESTS = "tests";
    public static final String TESTS_SHORT = "t";

    public static final String BUILT_TESTS = "builtTests";
    public static final String BUILT_CLASSES = "builtClasses";

    public static final String WEIGHT_CSVS = "weightCsvs";

    public static final String NESTED_CUTS = "nestedCuts";

    public static final String SUITE_BIAS_ALL_COMBINATIONS = "makeAllCombinations";
    public static final String SUITE_BIAS_RANDOM_TARGET_COUNT = "randomSuiteTargetCount";
    public static final String SUITE_BIAS_SUITE_SIZES = "suiteSizes";

    public static final String QUICK_RUN_MODE = "quickRun";

    public static final String CUSTOM_WORKING_DIR = "workingDir";
    public static final String CUSTOM_ANT_HOME = "antHome";
    public static final String CUSTOM_MAVEN_REPO = "mavenRepo";

    public static final String GRADEER_CSV_STUDENT = "studentGradeerResults";
    public static final String GRADEER_CSV_MUTANTS = "mutantGradeerResults";

}
