#!/bin/bash

readonly DUMMY_RUN=false

runCommand() {
    local command=$1

    if [ "$DUMMY_RUN" = false ] ; then
        echo "Running Command: ${command}"
        eval "${command}"
    else
        echo "Dummy Command: ${command}"
    fi
}

## Constants
readonly START_TIME=$(date +"%Y-%m-%d-%T")
readonly WORKING_DIR=$(eval "pwd")
readonly ALL_RESULTS_DIR="${WORKING_DIR}/results-${START_TIME}"
readonly COVERAGE_TOOL_LOCATION="${WORKING_DIR}/coverweight-1.0-SNAPSHOT-jar-with-dependencies.jar"
readonly JAR_DEPENDENCIES="/var/tmp/jarDependencies/*"
echo $ALL_RESULTS_DIR
echo "${WORKING_DIR}"

# Make main results dir
runCommand "mkdir ${ALL_RESULTS_DIR}"

##
returnToOriginalWorkDir() {
    runCommand "cd ${WORKING_DIR}"
}

moveResults() {
    local subject=$1true

    returnToOriginalWorkDir
    local subjectCWEnv="${WORKING_DIR}/subjects/${subject}/cwEnv"

    runCommand "mv ${subjectCWEnv}/results ${ALL_RESULTS_DIR}/${subject}"
}

switchToSubjectWorkDir() {
    local subject=$1

    runCommand "cd ${WORKING_DIR}/subjects/${subject}"
}

runSubject() {
    local subject=$1
    local modelSourceRelativeFile=$2
    local sourceFilename=$3

    switchToSubjectWorkDir "${subject}"
    local subjectDir=$(eval "pwd")

    local args="-nestedCuts"
    args="${args} -s ${subjectDir}/solutions-student"
    args="${args} -modelSourceDir ${subjectDir}/modelSolutions/model_a"
    args="${args} -modelSourceRelativeFile ${modelSourceRelativeFile}"
    args="${args} -sourceFilename ${sourceFilename}"
    args="${args} -t ${subjectDir}/testSuites"
    args="${args} -builtClasses ${subjectDir}/cwEnv/built-classes"
    args="${args} -builtTests ${subjectDir}/cwEnv/built-tests"
    args="${args} -workingDir ${subjectDir}/cwEnv"
    args="${args} -mutantGradeerResults gradeerOutput/major/allCheckResults.csv,gradeerOutput/pit/allCheckResults.csv"

    local command="java ${MEMORY_LIMIT} -cp ${COVERAGE_TOOL_LOCATION}:${JAR_DEPENDENCIES} coverweight.suitebias.SuiteBiasStudy ${args}"
    runCommand "${command}"

    moveResults $subject
}

printInvalidTests() {
    runCommand "cat ${ALL_RESULTS_DIR}/*/InvalidTests/invalidTests.txt"
}

removeUnnecessaryResultDirs() {
    runCommand "rm ${ALL_RESULTS_DIR}/*/InvalidTests -r"
    runCommand "rm ${ALL_RESULTS_DIR}/*/SizeGeneration -r"
    runCommand "rm ${ALL_RESULTS_DIR}/*/TestResults -r"
}

## Run
runSubject "fa-DataLoader" "assignment2020/dataloading/DataLoader.java" "DataLoader.java"
runSubject "fa-FQs" "assignment2020/handoutquestions/FitnessQuestions.java" "FitnessQuestions.java"
runSubject "chess-Queen" "assignment2018/Queen.java" "Queen.java"
runSubject "chess-Board" "assignment2018/Board.java" "Board.java"
runSubject "wine-Cellar" "assignment2019/WineSampleCellar.java" "WineSampleCellar.java"
printInvalidTests
removeUnnecessaryResultDirs